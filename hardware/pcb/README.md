## HuniServe Computer Aided Design  (CAD)

Into this folder, we add all Computer Aided Design (CAD) design 
files for the project. This includes all 3D models and drawings.

### Sending CAD files to the PCB manufacturer

Here is a video tutorial on how to generate the Gerber files in KiCad:

[![How to generate the Gerber files in KiCad](https://img.youtube.com/vi/1AXwjZoyNno/0.jpg)](https://www.youtube.com/watch?v=1AXwjZoyNno)
A video tutorial on how to generate the Gerber files in KiCad can be found

The PCB manufacturer needs the following files, with the following extensions:

- `.brd` - The PCB layout file, created by KiCad.
- `.drl` - The drill file, created by KiCad.
- `.gbr` - The Gerber file, created by KiCad.
- `.gbl` - The Gerber file, created by KiCad.
- `.gbo` - The Gerber file, created by KiCad.
- `.gbp` - The Gerber file, created by KiCad.
- `.gbs` - The Gerber file, created by KiCad.
- `.gko` - The Gerber file, created by KiCad.
- `.gml` - The Gerber file, created by KiCad.
- `.gto` - The Gerber file, created by KiCad.
- `.gtl` - The Gerber file, created by KiCad.
- `.gts` - The Gerber file, created by KiCad.
- `.txt` - The Gerber file, created by KiCad.

#### How to generate the Gerber files in KiCad

1. Open the PCB layout file in KiCad.
2. Click on `File` -> `Plot...`.
3. In the Include section, select the following options:
    - `F.Cu` - Front copper layer.
    - `B.Cu` - Back copper layer.
    - `F.Paste` - Front paste layer.
    - `B.Paste` - Back paste layer.
    - `F.Silkscreen` - Front silk layer.
    - `B.Silkscreen` - Back silk layer.
    - `F.Mask` - Front solder mask layer.
    - `B.Mask` - Back solder mask layer.
    - `Edge.Cuts` - Edge cuts layer.
4. In the General Options section, select the following options:
    - `Plot footprint values` 
    - `Plot reference designators` 
    - `Check zone fills before plotting` 
5. In the Gerber Options section, select the following options:
    - `Generate Gerber Job File`
    - `Use extended X2 format`
    - `Include netlist attributes`
    - Coordinate format: `4.6`
6. Click on `Plot` to generate the Gerber files.
    - The Gerber files are generated in the manufacturing directory adjacent to the PCB layout file.
7. Click on `Generate Drill Files...`.
    - Have the Output folder set to:
        - `manufacturing/`
    - In the Drill File Format section, select the following options:
        - `Gerber X2`
    - In the Map File Format section, select the following options:
        - `Gerber X2`
    - In the Drill Origin section, select the following options:
        - `Absolute origin`
8. Click on `Generate Drill File` to generate the drill files.
    - The drill files are generated in the manufacturing directory adjacent to the PCB layout file.
9. Zip the files in the manufacturing directory.
10. Send the zip file to the PCB manufacturer.
    1. Sign in on page [https://jlcpcb.com/](https://jlcpcb.com/) 
    2. Hover over `My file` and click  `Upload Gerber Or 3D Files`
    3. Click `Add gerber file` button and select the zip file you created in step 9.

