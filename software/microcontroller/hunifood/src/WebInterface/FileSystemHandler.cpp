#include "FileSystemHandler.h"

FileSystemHandler::FileSystemHandler()
{

}

FileSystemHandler::~FileSystemHandler()
{

}

void FileSystemHandler::serveFileFromSD( AsyncWebServerRequest *request, const char* filename, const char* contentType, int code )
{
    if( SD.exists( filename ) )
    {
        File file = SD.open( filename );
        String content;
        while( file.available() )
        {
            content += String( ( char ) file.read() );
        }
        file.close();
        request->send( 200, contentType, content );
    }
    else
    {
        //check if /upload/fileNotFound.html exists on SD card
        if( SD.exists( "/upload/fileNotFound.html" ) )
        {
            serveFileFromSD( request, "/upload/fileNotFound.html", "text/html", 404 );
        }
        else
        {
            request->send( 404, "text/plain", "File not found" );
        }

        request->send( 404, "text/plain", "File not found" );
    }
}

String FileSystemHandler::buildDirectoryStructure( File dir, int numTabs, bool includeCardSize, bool includeFiles )
{
    String result = "";
    if( numTabs == 0 && includeCardSize ) // If it's the root directory and includeCardSize is true
    {
        // Always indent SD Card size line one level.
        for( int i = 0; i < numTabs + 1; i++ )
        {
            result += "|   ";
        }

        // Get card size in GB
        float sizeInGB = static_cast< double >( SD.cardSize() ) / ( 1024 * 1024 * 1024 );

        result += "SD Card Size: ";
        result += String( sizeInGB );
        result += " GB\n"; // GB is the unit for size
    }

    while( true )
    {
        File entry = dir.openNextFile();
        if( !entry )
        {
            break;
        }

        if( entry.isDirectory() || includeFiles )
        {
            for( int i = 0; i < numTabs; i++ )
            {
                result += "|   ";
            }

            if( entry.isDirectory() )
            {
                result += "+---";
                result += entry.name(); // Append directory name
                result += "\n";
                result += buildDirectoryStructure( entry, numTabs + 1, includeFiles, includeCardSize );
            }
            else // if it's a file
            {
                result += "|   ";
                result += entry.name(); // Append file name
                result += "\t\t" + String( entry.size() );
                result += "\n";
            }
        }

        entry.close();
    }

    return result;
}

void FileSystemHandler::onIndexRequest( AsyncWebServerRequest *request )
{
    serveFileFromSD( request, "/index.html", "text/html" );
}

void FileSystemHandler::onDirectoryStructureRequest( AsyncWebServerRequest *request )
{
    String result = "Directory structure:\n";
    String path = request->url();

    path.remove( 0, 5 ); // Remove "/dir/" from the URL
    path = "/" + path; // Add leading slash

    result += buildDirectoryStructure( SD.open( path.c_str() ), 0, true );
    request->send( 200, "text/plain", result );
}

void FileSystemHandler::onUpload( AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final )
{

    if( !index )
    {
        //Open a new file on the SD card
        if( !SD.begin() )
        {
            return request->send( 500, "text/plain", "An Error Occurred While Opening the SD Card" );
        }
        if( !SD.exists( "/upload" ) )
        {
            SD.mkdir( "/upload" );
        }
        uploadFile = SD.open( "/upload/" + filename, FILE_WRITE );
    }
    for( size_t i=0; i < len; i++ )
    {
        uploadFile.write( data[ i ] );
    }
    if( final )
    {
        uploadFile.close();
    }
}

// Create or Update File
void FileSystemHandler::onCreateOrUpdateFile( AsyncWebServerRequest *request )
{
    String message;
    if( request->hasParam( "message", true ) ) //If received POST request to /file
    {
        message = request->getParam( "message", true )->value();//Get the value of the request
    }
    File file = SD.open( "/example.txt", FILE_WRITE );
    if( file )
    {
        file.println( message );
        file.close();
        request->send( 200, "text/plain", "File written successfully" );
    }
    else
    {
        request->send( 500, "text/plain", "File writing failed" );
    }
}



// Delete File
void FileSystemHandler::onDeleteFile( AsyncWebServerRequest *request )
{
    if( SD.exists( "/example.txt" ) )
    {
        SD.remove( "/example.txt" );
        request->send( 200, "text/plain", "File deleted successfully" );
    }
    else
    {
        request->send( 404, "text/plain", "File not found" );
    }
}

void FileSystemHandler::serveAnyFileFromSD( AsyncWebServerRequest *request, const char* filename, const char* )
{
    // Implementation of serving files from SD

    String path = filename;

    Serial.printf( "xRequested path: %s\n", path.c_str() );

    // Specify the appropriate content type based on the file extension
    if( path.endsWith( ".html" ) || path.endsWith( ".htm" ) )
    {
        request->send( SD, path, "text/html" );
    }
    else if( path.endsWith( ".css" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".js" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".png" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".gif" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".jpg" ) || path.endsWith( ".jpeg" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".ico" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".xml" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".pdf" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".zip" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".gz" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".json" ) )
    {
        request->send( SD, path );
    }
    else if( path.endsWith( ".txt" ) || path.endsWith( ".md" ) )
    {
        request->send( SD, path );
    }
    else
    {
        // Serve the file as download
        serveFileFromSD( request, path.c_str(), "application/octet-stream" );
    }
}
void FileSystemHandler::notFound( AsyncWebServerRequest *request )
{
    throw "FileSystemHandler::notFound is not implemented yet.";
}
void FileSystemHandler::onGetFile( AsyncWebServerRequest *request )
{
    String path = request->url();

    // Remove "/files/" from the URL
    path.remove( 0, 7 );

    // Add leading slash
    path = "/" + path;

    Serial.printf( "aRequested path: %s\n", path.c_str() );

    // Serve the file
    serveAnyFileFromSD( request, path.c_str(), nullptr );
}