#include "WebServerHandler.h"
#include "FileSystemHandler.h"

FileSystemHandler fileSystemHandler = FileSystemHandler();

WebServerHandler::WebServerHandler() : server( 80 )
{
    // Constructor logic here if needed
}

void WebServerHandler::begin()
{
    server.on( "/", HTTP_GET, []( AsyncWebServerRequest *request ){
        fileSystemHandler.onIndexRequest( request );
    } );

    server.on( "/dir/", HTTP_GET, []( AsyncWebServerRequest *request ){
        fileSystemHandler.onDirectoryStructureRequest( request );
    } );

    server.on( "/files", HTTP_POST, []( AsyncWebServerRequest *request ){
        fileSystemHandler.onCreateOrUpdateFile( request );
    } );

    server.on( "/files", HTTP_DELETE, []( AsyncWebServerRequest *request ){
        fileSystemHandler.onDeleteFile( request );
    } );

    server.on( "/files/*", HTTP_GET, [ & ]( AsyncWebServerRequest *request )
    {
        fileSystemHandler.onGetFile( request );
    } );

    server.on( "/upload", HTTP_POST,
               []( AsyncWebServerRequest * request ) {
    },
               []( AsyncWebServerRequest * request, const String & filename, size_t index, uint8_t *data, size_t len, bool final ) {
        fileSystemHandler.onUpload( request, filename, index, data, len, final );
    } );

    // You can add other routes here.

    server.begin();
}