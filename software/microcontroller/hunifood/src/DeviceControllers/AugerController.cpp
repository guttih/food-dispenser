#include "AugerController.h"

AugerController::AugerController( short motorPinDirection, short motorPinStep, short motorPinEnable )
{
    const short stepsPerRevolution = 200;
    const float RPM = 120;
    _stepper = new A4988( stepsPerRevolution, motorPinDirection, motorPinStep, motorPinEnable );
    _stepper->begin( RPM );
    _stepper->setEnableActiveState( LOW );

}

AugerController::~AugerController()
{

}

/**
 * @brief Moves the auger a given amount of degrees.
 *
 * @param deg
 */
void AugerController::rotateStepper( long deg )
{
    _stepper->rotate( deg );
    Serial.println( String( "Stepper started rotating process towards " ) + String( deg ) + "°" );
}
void AugerController::enableStepper( bool enable )
{
    if( enable )
        _stepper->enable();
    else
        _stepper->disable();


    Serial.println( String( "Stepper " ) + ( enable ? "enabled" : "disabled" ) );
}