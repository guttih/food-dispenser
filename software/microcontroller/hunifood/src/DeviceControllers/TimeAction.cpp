#include "TimeAction.h"

bool TimeAction::isValid()
{
    return _triggerMillis != 0;
}

uint64_t TimeAction::getTriggerMillis() const
{
    return _triggerMillis;
}

void TimeAction::executeAction() const
{
    switch( _actionType )
    {
        case 0:     // no parameters
            _action_basic();
            break;
        case 1:     // one parameter
            _action_one_int( _paramOne );
            break;
        case 2:     // two parameters
            _action_two_ints( _paramOne, _paramTwo );
            break;
        case 3:     // one float parameter
            _action_one_float( _paramOne );
            break;
        case 4:     // one String and one unsigned long parameter
            _action_String_ulong( _paramStringOne, _paramULongOne );
            break;
        default:
            Serial.println( "Invalid action type." );
            break;
    }
}

