#include "TimeManager.h"
#include <Arduino.h>
#include <stdexcept>
#include <TimeHelper.h>


TimeManager::TimeManager()
{

}

TimeManager::~TimeManager()
{
    while( !_actionQueue.empty() )
    {
        delete _actionQueue.top();
        _actionQueue.pop();
    }

}
void TimeManager::initRtc( TwoWire* wire, byte address )
{
    _wire = wire;
    _address = address;
}
bool TimeManager::isDeviceConnected()
{
    _wire->beginTransmission( _address );
    byte error = _wire->endTransmission();
    return error == 0; // If no error, then a device was found at this address
}
bool TimeManager::syncWithRtc()
{
    try
    {
        tmElements_t tm = getRtcTime();
        time_t rtcTime = makeTime( tm );

        if( rtcTime <= 0 )
        {
            // This is probably an error case, handle it accordingly
            // log "Invalid time from RTC"
            Serial.println( "Invalid time from RTC" );
            return false;
        }

        setCurrentTime( rtcTime );
    }
    catch ( const std::exception & e ) // More general exception catch
    {
        Serial.println( "Exception caught in syncWithRtc: " + String( e.what() ) );
        return false; // Synchronization failed
    }
    return true; // Synchronization succeeded
}
void TimeManager::checkResyncWithRtc( uint64_t intervalMillis )
{
    static uint64_t lastSyncTime = 0;
    if( ( millis() - lastSyncTime ) > intervalMillis )
    {
        Serial.println( "Syncing with RTC" );
        lastSyncTime = millis();
        if( !syncWithRtc() )
        {
            throw std::runtime_error( "Failed to sync with RTC from checkResyncWithRtc" );
        }
    }
}

String TimeManager::nowString()
{
    time_t t = now();
    return formatTimeString( t );
}

time_t TimeManager::toTimeT( tmElements_t tm )
{
    //is there a copy constructor for tmElements_t?
    tmElements_t tm2 = tm;
    return makeTime( tm );
}

tmElements_t TimeManager::toTmElement( time_t t )
{
    tmElements_t tm;
    breakTime( t, tm );
    return tm;
}

uint64_t TimeManager::nowMillis()
{
    uint64_t currentMillis = millis();
    uint64_t elapsedMillis = ( currentMillis >= _lastSyncMillis ) ? ( currentMillis - _lastSyncMillis ) : ( currentMillis + ( 4294967295 - _lastSyncMillis ) );

    uint64_t elapsedSecs = elapsedMillis / 1000;
    time_t currentTime = now();
    currentTime += elapsedSecs; // Update current time with elapsed seconds
    _lastSyncMillis = currentMillis - ( elapsedMillis % 1000 ); // Maintain the exact time of the last sync

    return currentTime * 1000 + elapsedMillis % 1000; // Return the current time in milliseconds
}

bool TimeManager::setRtcTime( tmElements_t tm )
{
    // To eliminate any potential race conditions,
    // stop the clock before writing the values,
    // then restart it after.
    Serial.printf( "Setting RTC time to: %04d-%02d-%02d %02d:%02d:%02d\n",  tm.Year, tm.Month, tm.Day, tm.Hour, tm.Minute, tm.Second );
    _wire->beginTransmission( _address );
    _wire->write( ( uint8_t ) 0x00 ); // reset register pointer
    _wire->write( ( uint8_t ) 0x80 ); // Stop the clock. The seconds will be written last
    _wire->write( dec2bcd( tm.Minute ) );
    _wire->write( dec2bcd( tm.Hour ) ); // sets 24 hour format
    _wire->write( dec2bcd( tm.Wday ) );
    _wire->write( dec2bcd( tm.Day ) );
    _wire->write( dec2bcd( tm.Month ) );

    uint8_t year = tmYearToY2k( tm.Year );
    _wire->write( dec2bcd( year ) );
    if( _wire->endTransmission() != 0 )
    {
        return false;
    }

    // Now go back and set the seconds, starting the clock back up as a side effect
    _wire->beginTransmission( _address );
    _wire->write( ( uint8_t ) 0x00 ); // reset register pointer
    _wire->write( dec2bcd( tm.Second ) ); // write the seconds, with the stop bit clear to restart
    if( _wire->endTransmission() != 0 )
    {
        return false;
    }

    setCurrentTime( makeTime( tm ) ); //update the current time in this object
    return true;
}

tmElements_t TimeManager::getRtcTime()
{
    tmElements_t tm;
    uint8_t sec;

    _wire->beginTransmission( _address );
    _wire->write( ( uint8_t ) 0x00 );
    if( _wire->endTransmission() != 0 )
    {
        throw std::runtime_error( "Failed to end transmission to RTC" );
    }
    // request the 7 data fields (secs, min, hr, dow, date, mth, yr)
    _wire->requestFrom( ( uint8_t ) _address, ( uint8_t ) tmNbrFields );
    if( _wire->available() < tmNbrFields )
    {
        throw std::runtime_error( "Insufficient data from RTC" );
    }

    sec = _wire->read();
    tm.Second = bcd2dec( sec & 0x7f );
    tm.Minute = bcd2dec( _wire->read() );
    tm.Hour = bcd2dec( _wire->read() & 0x3f );// mask assumes 24hr clock
    tm.Wday = bcd2dec( _wire->read() );
    tm.Day = bcd2dec( _wire->read() );
    tm.Month = bcd2dec( _wire->read() );
    uint8_t year = bcd2dec( _wire->read() );
    tm.Year = y2kYearToTm( year );
    if( sec & 0x80 )
    {
        throw std::runtime_error( "RTC clock is halted" );
    }

    return tm;
}

tmElements_t TimeManager::makeTimeElements( int year, byte month, byte dayOfMonth, byte hour, byte minute, byte second )
{
    tmElements_t tm;
    tm.Year   =  CalendarYrToTm( ( byte ) year );
    tm.Month  = month;
    tm.Day    = dayOfMonth;
    tm.Hour   = hour;
    tm.Minute = minute;
    tm.Second = second;
    tm.Wday   = dayOfWeek( makeTime( tm ) ); // Calculate day of week
    return tm;
}

void TimeManager::setCurrentTime( time_t t )
{
    setTime( t ); // Set the system time
    _lastSyncMillis = millis(); // Reset millisecond counter to now
}

uint8_t TimeManager::bcd2dec( uint8_t num )
{
    return ( ( num / 16 * 10 ) + ( num % 16 ) );
}

uint8_t TimeManager::dec2bcd( uint8_t num )
{
    return ( ( num / 10 * 16 ) + ( num % 10 ) );
}

void TimeManager::updateLastTriggerAction( TimeAction* newAction )
{
    // If _lastTriggerAction is null or the new action's trigger time is later, update _lastTriggerAction
    if( _lastTriggerAction == nullptr || newAction->getTriggerMillis() > _lastTriggerAction->getTriggerMillis() )
    {
        _lastTriggerAction = newAction;
    }
}

void TimeManager::addTimeAction( tmElements_t tm, action_t action )
{
    time_t t = makeTime( tm );
    uint64_t triggerMillis = ( ( uint64_t ) t ) * 1000; // Convert time_t to milliseconds
    addTimeAction( triggerMillis, action ); // Call the other addTimeAction method
}

void TimeManager::pushToQueue( TimeAction* newAction )
{
    _actionQueue.push( newAction );
    updateLastTriggerAction( newAction );
}
void TimeManager::addTimeAction( uint64_t triggerMillis, action_t action )
{
    // The new operator is used to allocate memory on the heap for a new TimeAction object.
    // By using new, you ensure that the TimeAction object will persist until it is explicitly deleted.
    TimeAction* timeAction = new TimeAction( triggerMillis, action );

    if( timeAction == nullptr )
    {
        throw std::runtime_error( "Failed to allocate memory for TimeManager::addTimeAction( action_t..." );
        return;
    }

    // Adds the new TimeAction to the queue of actions to be executed and updates _lastTriggerAction if necessary
    pushToQueue( timeAction );
}

void TimeManager::addTimeAction( uint64_t triggerMillis, action_with_String_ulong action, String description, unsigned long paramUlong )
{
    // The new operator is used to allocate memory on the heap for a new TimeAction object.
    // By using new, you ensure that the TimeAction object will persist until it is explicitly deleted.
    Serial.println( "Adding time action with description: " + description + ", triggerMillis: " + String( triggerMillis ) );
    TimeAction* timeAction = new TimeAction( triggerMillis, action, description, paramUlong );

    if( timeAction == nullptr )
    {
        throw std::runtime_error( "Failed to allocate memory for TimeManager::addTimeAction( action_with_String_ulong..." );
        return;
    }

    // Adds the new TimeAction to the queue of actions to be executed and updates _lastTriggerAction if necessary
    pushToQueue( timeAction );
}

void TimeManager::addTimeAction( uint64_t triggerMillis, action_with_one_int action, int param1 )
{
    // The new operator is used to allocate memory on the heap for a new TimeAction object.
    // By using new, you ensure that the TimeAction object will persist until it is explicitly deleted.
    TimeAction* timeAction = new TimeAction( triggerMillis, action, param1 );

    if( timeAction == nullptr )
    {
        throw std::runtime_error( "Failed to allocate memory for TimeManager::addTimeAction( action_with_one_int..." );
        return;
    }

    // Adds the new TimeAction to the queue of actions to be executed and updates _lastTriggerAction if necessary
    pushToQueue( timeAction );
}

void TimeManager::addTimeAction( uint64_t triggerMillis, action_with_two_ints action, int param1, int param2 )
{
    // The new operator is used to allocate memory on the heap for a new TimeAction object.
    // By using new, you ensure that the TimeAction object will persist until it is explicitly deleted.
    TimeAction* timeAction = new TimeAction( triggerMillis, action, param1, param2 );

    if( timeAction == nullptr )
    {
        throw std::runtime_error( "Failed to allocate memory for TimeManager::addTimeAction( action_with_two_ints..." );
        return;
    }

    // Adds the new TimeAction to the queue of actions to be executed and updates _lastTriggerAction if necessary
    pushToQueue( timeAction );
}

void TimeManager::addTimeAction( uint64_t triggerMillis, action_with_one_float action, float param1 )
{
    TimeAction* timeAction = new TimeAction( triggerMillis, action, param1 );
    if( timeAction == nullptr )
    {
        throw std::runtime_error( "Failed to allocate memory for TimeManager::addTimeAction( action_with_two_ints..." );
        return;
    }

    // Adds the new TimeAction to the queue of actions to be executed and updates _lastTriggerAction if necessary
    pushToQueue( timeAction );
}

/**
 * @brief Converts unsigned long value like returned from millis() to tmElements_t and bases the return value on current time and current millis.
 *
 * @param timeInMillis
 * @return tmElements_t
 */

tmElements_t TimeManager::millisToTimeElements( uint64_t timeInMillis )
{
    // Get current time in milliseconds.
    uint64_t currentMillis = millis();

    // Get the current time.
    time_t currentTime = now();

    // Determine the difference between the current and provided timestamps.
    int64_t millisDiff = timeInMillis > currentMillis ? static_cast< int64_t >( timeInMillis - currentMillis ) : 0;

    // Calculate proportion of seconds.
    double secondsToAdd = millisDiff / 1000.0;

    // Adjust currentTime according to the calculated proportion of seconds.
    time_t calculatedTime = currentTime + static_cast< time_t >( round( secondsToAdd ) );

    tmElements_t tm;
    breakTime( calculatedTime, tm );

    return tm;

    // get current mills
    // get current mills
    // uint64_t currentMillis = millis();
    // // get current time
    // time_t currentTime = now();


    // //Ok we need the relationship between the currentMillis and convertMills and use currentTime to create a new time_t
    // time_t newTime = currentTime + ( ( convertMills - currentMillis ) / 1000 );


    // tmElements_t tm;
    // breakTime( newTime, tm );

    // return tm;
}

int TimeManager::getActionCount() const
{
    return _actionQueue.size();
}

bool TimeManager::handleTimeActions()
{
    // Check if actions need to be executed
    bool actionsDue = false;
    while( !_actionQueue.empty() && ( _actionQueue.top() )->getTriggerMillis() <= millis() )
    {
        Serial.printf( "Triggering getTriggerMillis: %llu =< millis: %lu\n", ( _actionQueue.top() )->getTriggerMillis(), millis() );

        if( _lastTriggerAction == _actionQueue.top() )
        {
            _lastTriggerAction = nullptr; // Set _lastTriggerAction to null if it's the action being executed
        }
        _actionQueue.top()->executeAction();  // execute action
        delete _actionQueue.top();  // delete action after execution to reclaim memory.
        _actionQueue.pop();  // remove action from queue
        actionsDue = true;
    }
    return actionsDue;
}

uint64_t TimeManager::getNextActionTriggerMillis() const
{
    if( _actionQueue.empty() )
    {
        throw std::runtime_error( "No actions in queue, check if action is empty before calling this function" );
    }
    else
    {
        return _actionQueue.top()->getTriggerMillis();
    }
}

TimeAction *TimeManager::getLastTriggerAction() const
{
    return _lastTriggerAction;
}

uint64_t TimeManager::getLastActionTriggerMillis() const
{
    if( _lastTriggerAction == nullptr )
    {
        throw std::runtime_error( "No last action exists, check if action is empty before calling this function" );
    }
    else
    {
        return getLastTriggerAction()->getTriggerMillis();
    }
}


uint64_t TimeManager::getFutureMillis( const tmElements_t tm )
{
    // Convert tmElements_t to a Unix timestamp
    time_t t = makeTime( tm );

    // Convert Unix timestamp to milliseconds
    uint64_t timestampMillis = ( ( uint64_t ) t ) * 1000;

    // Get the current Unix timestamp in milliseconds
    uint64_t nowMillisUnix = nowMillis();

    // Compute the difference between current Unix timestamp and timestampMillis
    int64_t diffMillis = timestampMillis - nowMillisUnix;

    if( diffMillis > 0 )
    {
        // Return millis timestamp in the future
        return millis() + ( unsigned long ) diffMillis;
    }
    else
    {
        // Return the current millis timestamp if the date is in the past
        return millis();
    }
}
String TimeManager::formatTimeString( time_t t )
{
    return TimeHelper::formatTimeString( t );
}
String TimeManager::formatTimeString( const tmElements_t tm )
{
    return TimeHelper::formatTimeString( tm );
}
