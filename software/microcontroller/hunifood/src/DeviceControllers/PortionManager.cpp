#include "PortionManager.h"


PortionManager::PortionManager( AugerController *auger, DispensingController *dispenser, Weigher *storageWeigher, Weigher *portionWeigher )
{
    _foodTransporter = auger;
    _storageScale = storageWeigher;
    _portionScale = portionWeigher;
    _foodDispenser = dispenser;
}

void PortionManager::begin()
{

    _portionScale->begin();
    _storageScale->begin();
}

void PortionManager::rotateStepper( long deg )
{
    _foodTransporter->rotateStepper( deg );
}

void PortionManager::enableStepper( bool enable )
{
    _foodTransporter->enableStepper( enable );
}

PortionManager::~PortionManager()
{

}
float PortionManager::readPortionWeightInGrams( bool waitForScaleToBeReady, uint8_t sampleCount )
{
    return _portionScale->readWeightInGrams( waitForScaleToBeReady, sampleCount );
}
float PortionManager::readStorageWeightInGrams( bool waitForScaleToBeReady, uint8_t sampleCount )
{
    return _storageScale->readWeightInGrams( waitForScaleToBeReady, sampleCount );
}
void PortionManager::openDispensingDoor()
{
    enableServo( true );
    setServoAngle( 180 );
}

void PortionManager::closeDispensingDoor()
{
    enableServo( true );
    setServoAngle( 0 );
}
void PortionManager::enableServo( bool enable )
{
    _foodDispenser->enable( enable );
}

void PortionManager::setServoAngle( uint16_t angle )
{
    _foodDispenser->setServoAngle( angle );
}

