
// #include <Arduino.h>
// #include <Weigher.h>
// #include "AugerController.h"
// #include "PortionManager.h"
// #include "display.h"
// #include "constants.h"
// #include "secret.h"
// #include "Event/FeedingEvent.h"
// #include "Event/EventManager.h"
// #include "Event/ScheduledEvent.h"
// #include "Event/DailyRecurrence.h"

// #include <WiFi.h>
// #include "WebServerHandler.h"

// #include <Wire.h>

// #include <TimeLib.h>
// #include "TimeManager.h"
// #include <DS1307RTC.h>


// TimeManager timeManager;

// #define CIRCLE_COUNT 6


// const int servoY = 172;


// WebServerHandler webServer;

// float weight =-1;
// float lastWeight = -1;
// static uint32_t stepperTime;
// bool enableStuff = true;
// tmElements_t tm;


// void feedingEvent( String description, unsigned long portionSize )
// {
//     Serial.println( "Action Feeding event executed: " + description + ", portionSize: " + String( portionSize ) );
// }

// void printAction0()
// {
//     Serial.println( "Action 0 Executed" );
// }
// void printAction1( int a )
// {
//     Serial.println( "Action 1 Executed a: " + String( a ) );
// }

// void printAction2( int a, int b )
// {
//     Serial.println( "Action 2 Executed, a: " + String( a ) + ", b: " + String( b ) );
// }


// bool isDeviceConnectedAt( byte address )
// {
//     Wire.beginTransmission( address );
//     byte error = Wire.endTransmission();
//     return error == 0; // If no error, then a device was found at this address
// }

// void setup()
// {
//     Serial.begin( 115200 );
//     FeedingEvent event1( "Description1", 100 );
//     Serial.println( "Event1: " + event1.serialize() );

//     FeedingEvent event2( "Description2", 200 );
//     Serial.println( "Event2: " + event2.serialize() );
//     Serial.println( "Event1 == Event2: " + String( event1 == event2 ) );
//     Serial.println( "Deserializing event1 into event2" );
//     event2.deserialize( event1.serialize() );
//     Serial.println( "Event2: " + event2.serialize() );
//     Serial.println( "Event1 == Event2: " + String( event1 == event2 ) );
//     event2.setDescription( "Description4" );
//     Serial.println( "Event1 2= Event2: " + String( event1 == event2 ) );
//     event2.setDescription( "Description1" );
//     Serial.println( "Event1 3= Event2: " + String( event1 == event2 ) );

//     Wire.begin( SDA_PIN, SCL_PIN );
//     timeManager.initRtc( &Wire, ADDRESS_RTC );

//     // Checking if a device is connected at address 0x68
//     if( timeManager.isDeviceConnected() )
//     {
//         Serial.printf( "Syncing with RTC %s\n", timeManager.syncWithRtc() ? "was a success." : "failed!" );
//     }
//     else
//         Serial.printf( "No device is connected at address 0x%02X\n", ADDRESS_RTC );


//     ScheduledEvent scheduledEvent1( "feeding1", new FeedingEvent( "I am a daily scheduled feeding event", 12 ), new DailyRecurrence( now() + 100 ) );
//     Serial.println( "scheduledEvent1: " + scheduledEvent1.serialize() );

//     time_t t = now() + 9;
//     Serial.println( "now()+200: " + timeManager.formatTimeString( timeManager.millisToTimeElements( t ) ) );
//     ScheduledEvent scheduledEvent2( "feeding2", new FeedingEvent( "I am a daily scheduled feeding event 2", 13 ), new DailyRecurrence( t ) );
//     Serial.println( "millis : " + String( millis() ) + " At: " + timeManager.formatTimeString( timeManager.millisToTimeElements( millis() ) ) );
//     unsigned long ul = scheduledEvent2.getNextTriggerMillis( now() );
//     Serial.println( "trigger: " + String( ul ) + " At: " + timeManager.formatTimeString( timeManager.millisToTimeElements( ul ) ) );

//     timeManager.addTimeAction( ( uint64_t ) ul, feedingEvent, scheduledEvent2.getDescription(),
//                                ( ( FeedingEvent* ) scheduledEvent2.getEvent() )->getPortionSize() );



//     Serial.println( "1 Starting loop action  " + timeManager.nowString() );
//     timeManager.addTimeAction( millis() + 14000, printAction0 );
//     Serial.println( "2 Starting loop action  " + timeManager.nowString() );
//     timeManager.addTimeAction( timeManager.getFutureMillis( timeManager.toTmElement( now() ) ) + 12000, printAction1, 11 );
//     ul = 30000;
//     Serial.println( "millis: " + String( 1000 ) + " At: " + timeManager.formatTimeString( timeManager.millisToTimeElements( 1000 ) ) );
//     Serial.println( "millis: " + String( ul ) + " At: " + timeManager.formatTimeString( timeManager.millisToTimeElements( ul ) ) );
//     timeManager.addTimeAction( 20000, printAction0 );
//     timeManager.addTimeAction( ul + 1712, printAction2, 11, 23 );
//     timeManager.addTimeAction( ul + 1710, printAction0 );
//     timeManager.addTimeAction( ul + 1711, printAction1, 100 );
//     Serial.println( "3 Starting loop action  " + timeManager.nowString() );

//     Serial.println( "Next action to re executed: " + String( timeManager.getNextActionTriggerMillis() ) + " At: " +
//                     timeManager.formatTimeString( timeManager.millisToTimeElements( timeManager.getNextActionTriggerMillis() ) ) );
//     Serial.println( "Last action to re executed: " + String( timeManager.getLastActionTriggerMillis() ) + " At: " +
//                     timeManager.formatTimeString( timeManager.millisToTimeElements( timeManager.getLastActionTriggerMillis() ) ) );

// }

// int actionCount = 0;
// void loop()
// {
//     if( actionCount != timeManager.getActionCount() )
//     {
//         actionCount = timeManager.getActionCount();
//         if( actionCount > 0 )
//         {
//             Serial.print( "Action count: " + String( actionCount ) );
//             Serial.print( " Next action trigger millis: " + String( timeManager.getNextActionTriggerMillis() ) );
//             Serial.println( " Next action trigger time  : " +
//                             timeManager.formatTimeString( timeManager.millisToTimeElements( timeManager.getNextActionTriggerMillis() ) ) );
//         }
//     }

//     Serial.println( "4 Starting loop action  " + timeManager.nowString() );
//     if( timeManager.handleTimeActions() )
//     {
//         Serial.println( "Action executed" );
//     }
//     delay( 1000 );
//     timeManager.checkResyncWithRtc();


// }