
#include <Arduino.h>
#include <Weigher.h>
#include "AugerController.h"
#include "PortionManager.h"
#include "display.h"
#include "constants.h"
#include "secret.h"
#include <TimeHelper.h>
#include <FeedingEvent.h>
#include <ScheduledEvent.h>
#include <DailyRecurrence.h>
#include <WeeklyRecurrence.h>
#include <Scheduler.h>
#include <SDCardStorage.h>


#include <WiFi.h>
#include "WebServerHandler.h"

#include <Wire.h>

#include <TimeLib.h>
#include "TimeManager.h"
#include <DS1307RTC.h>

#define CIRCLE_COUNT 6


const int servoY = 172;


WebServerHandler webServer;

// Weigher storageWeight( STORAGE_SCALE_DT_PIN, STORAGE_SCALE_SCK_PIN, 105.151169 );
Weigher storageWeight( STORAGE_SCALE_DT_PIN, STORAGE_SCALE_SCK_PIN,  39215, 108.188255 );
Weigher portionWeight( PORTION_SCALE_DT_PIN, PORTION_SCALE_SCK_PIN, 272758, 402.025513 );
DispensingController dispenser( SERVO_PIN, SERVO_CHANNEL );
AugerController auger( MOTOR_PIN_DIRECTION, MOTOR_PIN_STEP, MOTOR_PIN_ENABLE );
PortionManager portioner( &auger, &dispenser,  &storageWeight, &portionWeight );
TimeManager timeManager;

float weight =-1;
float lastWeight = -1;
static uint32_t stepperTime;
bool enableStuff = true;
tmElements_t tm;

Scheduler scheduler( std::make_shared< SDCardStorage >( SD_CS ) );;


void feedingEvent( String description, unsigned long portionSize )
{
    Serial.println( "Action Feeding event executed: " + description + ", portionSize: " + String( portionSize ) );
}

void printAction0()
{
    Serial.println( "Action 0 Executed" );
}
void printAction1( int a )
{
    Serial.println( "Action 1 Executed a: " + String( a ) );
}

void printAction2( int a, int b )
{
    Serial.println( "Action 2 Executed, a: " + String( a ) + ", b: " + String( b ) );
}


bool isDeviceConnectedAt( byte address )
{
    Wire.beginTransmission( address );
    byte error = Wire.endTransmission();
    return error == 0; // If no error, then a device was found at this address
}

void testSDSaveLoad()
{
    Serial.println( "testSDSaveLoad()" );
    time_t NOW = makeTime( TimeHelper::createTmElement( 2023, 12, 25, 0, 0, 0 ) );
    scheduler.add( std::make_shared< ScheduledEvent >( "", std::make_shared< Event >( "Event 1" ),
                                                       std::make_shared< DailyRecurrence >( NOW + 100 ) ) );

    Serial.println( "1Second until next trigger" + String( scheduler.getSecondsUntilTriggered() ) );
    scheduler.add( std::make_shared< ScheduledEvent >( "", std::make_shared< Event >( "Event 2" ),
                                                       std::make_shared< DailyRecurrence >( NOW + 22 ) ) );
    Serial.println( "2Second until next trigger" + String( scheduler.getSecondsUntilTriggered() ) );
    scheduler.add( std::make_shared< ScheduledEvent >( "", std::make_shared< FeedingEvent >( "FeedingEvent 11", 111 ),
                                                       std::make_shared< DailyRecurrence >( NOW + 1111 ) ) );
    Serial.println( "3Second until next trigger" + String( scheduler.getSecondsUntilTriggered() ) );
    scheduler.add( std::make_shared< ScheduledEvent >( "", std::make_shared< FeedingEvent >( "FeedingEvent 22", 222 ),
                                                       std::make_shared< DailyRecurrence >( NOW + 2222 ) ) );

    Serial.println( "4Second until next trigger" + String( scheduler.getSecondsUntilTriggered() ) );

    String msg;

    float sizeInGB = static_cast< double >( SD.cardSize() ) / ( 1024 * 1024 * 1024 );
    float freeSizeInGB = static_cast< double >( SD.totalBytes() ) / ( 1024 * 1024 * 1024 );
    // drawCentered( "SD Card size", 100 );
    // tft.setFreeFont( FF17 );
    msg = "Free " + String( freeSizeInGB ) + " GB  " + String( sizeInGB ) + " GB  ";
    // drawCentered( msg, 120 );


    scheduler.save();
    Serial.println( "Number of events after save: " + String( scheduler.count() ) );
    scheduler.removeAllItems();
    Serial.println( "Number of events after removeAllItems: " + String( scheduler.count() ) );
    Serial.println( "5Second until next trigger" + String( scheduler.getSecondsUntilTriggered() ) );
    scheduler.load();
    Serial.println( "Number of events after load: " + String( scheduler.count() ) );
    Serial.println( "------------- ScheduledEvents listed By Added order   -----------------\n\n" + scheduler.serializedList() + "\n\n" );
    Serial.println( "6Second until next trigger" + String( scheduler.getSecondsUntilTriggered() ) );

}

void initDisplay()
{
    Serial.println( "initDisplay()" );
    delay( 500 );
    tft.begin();
    tft.invertDisplay( false );
    tft.setRotation( 3 );
    tft.setSwapBytes( true );
    tft.fillScreen( TFT_BLACK );
    tft.setFreeFont( FF18 );
    setBrightnessPin( LED_PIN, BRIGHTNESS_CHANNEL );
    setBrightness( 66, BRIGHTNESS_CHANNEL );
    Serial.println( "initDisplay() done" );
}

bool toggle = true;
constexpr uint8_t IO_A_DIRECTION_REG = 0x00;
constexpr uint8_t IO_B_DIRECTION_REG = 0x01;
constexpr uint8_t IO_A_REGISTER = 0x12;
constexpr uint8_t IO_B_REGISTER = 0x13;

void initAllMCP23017Pins( bool asOutput = true )
{
    String actionDescription = "Setting all pins as " + String( asOutput ? "OUTPUT" : "INPUT" ) + "  pins";

    // Command to set all pins on Port A AND B to input (0 for output, 1 for input)
    int action = asOutput ? 0x00 : 0xFF;
    Serial.println( actionDescription );
    Wire.beginTransmission( ADDRESS_MCP23017 );
    Wire.write( IO_A_DIRECTION_REG );// Select Port A direction register
    Wire.write( action );// Set all pins on Port A to output (0 for output, 1 for input)
    Wire.endTransmission();

    Wire.beginTransmission( ADDRESS_MCP23017 );
    Wire.write( IO_B_DIRECTION_REG ); // Select Port B direction register
    Wire.write( action ); // Set all pins on Port B to output
    Wire.endTransmission();
}

void enableAllMCP23017Pins( bool enable )
{
    String actionDescription = "Powering " + String( enable ? "on" : "off" ) + " all MCP23017 pins";
    int action = enable ? 0xFF : 0x00;

    Serial.println( actionDescription );
    Wire.beginTransmission( ADDRESS_MCP23017 );
    Wire.write( IO_A_REGISTER );// Select Port A register
    Wire.write( action );// Set all pins on Port A to output (0 for output, 1 for input)
    Wire.endTransmission();

    Wire.beginTransmission( ADDRESS_MCP23017 );
    Wire.write( IO_B_REGISTER );
    Wire.write( action );
    Wire.endTransmission();
}

String msg, storageWeightMsg, portionWeightMsg, storageWeightMsgLast, portionWeightMsgLast;
void setup()
{
    Serial.begin( 115200 );
    Serial.println( "Starting setup" );

    delay( 500 );
    if( !scheduler.beginSD() )
        msg = "Unable to open sd card!";
    else
        msg = "SD card opened!";
    unsigned long ul;

    Serial.println( msg );
    time_t startTime = 1702364835;
    long increment;
    time_t currentTime = startTime + 3;
    setTime( currentTime );
    Serial.println( "Starting setup" );
    // delay( 2000 );
    Wire.begin( SDA_PIN, SCL_PIN );
    timeManager.initRtc( &Wire, ADDRESS_RTC );

    // Checking if a device is connected at address 0x68
    if( timeManager.isDeviceConnected() )
    {
        Serial.printf( "Syncing with RTC %s\n", timeManager.syncWithRtc() ? "was a success." : "failed!" );
    }
    else
        Serial.printf( "No device is connected at address 0x%02X\n", ADDRESS_RTC );

    initAllMCP23017Pins();
    enableAllMCP23017Pins( toggle );
    portioner.begin();

    Serial.println( "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" );
    time_t NOW = makeTime( TimeHelper::createTmElement( 2023, 12, 25, 10, 10, 0 ) );
    time_t TEST = NOW;
    Serial.println( "Second until next trigger" + String( scheduler.getSecondsUntilTriggered() ) );
    scheduler.add( std::make_shared< ScheduledEvent >( "feeding1",
                                                       std::make_shared< FeedingEvent >( "I am a daily scheduled feeding event 1", 8 ),
                                                       std::make_shared< DailyRecurrence >( NOW ) ) );
    Serial.println( "Second until next trigger" + String( scheduler.getSecondsUntilTriggered() ) );

    scheduler.add( std::make_shared< ScheduledEvent >( "feeding2",
                                                       std::make_shared< FeedingEvent >( "I am a daily scheduled feeding event 2", 13 ),
                                                       std::make_shared< DailyRecurrence >( NOW ) ) );

    scheduler.add( std::make_shared< ScheduledEvent >( "feeding3",
                                                       std::make_shared< FeedingEvent >( "I am a daily scheduled feeding event 3", 33 ),
                                                       std::make_shared< WeeklyRecurrence >( NOW, MAX_LONG,
                                                                                             CombineWeekdays( WEEKLY_SUNDAY, WEEKLY_WEDNESDAY,
                                                                                                              WEEKLY_FRIDAY ) ) ) );



    scheduler.addDailyEvent( "I am a daily scheduled event 4", NOW + 60 + 6 );
    scheduler.addDailyFeedingEvent( "I am a daily scheduled feeding event 4", 16, NOW + 60 );
    scheduler.addWeeklyFeedingEvent( "I am a weekly scheduled feeding event 6", 26, NOW + 60 + 3, CombineWeekdays( WEEKLY_SUNDAY, WEEKLY_WEEKEND ) );

    // tft.drawString( msg,  80, 100 );
    // initDisplay();
    testSDSaveLoad();
    DailyRecurrence dr1( NOW, MAX_LONG );
    DailyRecurrence dr2 = dr1;
    Serial.println( "dr1: " + dr1.serialize() );
    Serial.println( "dr2: " + dr2.serialize() );
    DailyRecurrence dr3( 0, 0 );
    dr3 = dr1;
    Serial.println( "dr3: " + dr3.serialize() );

    WeeklyRecurrence wr1( NOW, MAX_LONG, CombineWeekdays( WEEKLY_SUNDAY, WEEKLY_WEDNESDAY, WEEKLY_FRIDAY ) );
    WeeklyRecurrence wr2 = wr1;
    Serial.println( "wr1: " + wr1.serialize() );
    Serial.println( "wr2: " + wr2.serialize() );
    WeeklyRecurrence wr3( 0, 0 );
    wr3 = wr1;
    Serial.println( "wr3: " + wr3.serialize() );

    Serial.println( " All DailyRecurrence and WeeklyRecurrence objects are equal: " + String( dr1.equals( dr2 ) && dr1.equals( dr3 ) &&
                                                                                              wr1.equals( wr2 ) && wr1.equals( wr3 ) ) );

    // Count the number of events
    Serial.println( "Number of events: " + String( scheduler.count() ) );

    Serial.println( "------------- ScheduledEvents listed By Added order   -----------------\n\n" + scheduler.serializedList() + "\n\n" );

    scheduler.sortById( true );
    Serial.println( "------------- ScheduledEvents listed By Id in reverse order     -----------------\n\n" + scheduler.serializedList() + "\n\n" );
    scheduler.sortById();
    Serial.println( "------------- ScheduledEvents listed By Id in ascending order   -----------------\n\n" + scheduler.serializedList() + "\n\n" );

    scheduler.sortByNextTrigger( currentTime, true );
    Serial.println( "------------- ScheduledEvents listed By Next Trigger in reverse order   ----------\n\n" + scheduler.serializedList() + "\n\n" );

    scheduler.sortByNextTrigger( currentTime );
    Serial.println( "------------- ScheduledEvents listed By Next Trigger in ascending order ----------\n\n" + scheduler.serializedList() + "\n\n" );



    Serial.println( "now()              At : " + timeManager.formatTimeString( now() ) );
    Serial.println( "millis             At: " + timeManager.formatTimeString( timeManager.millisToTimeElements( millis() ) ) +
                    " :" + String( millis() ) );


    Serial.println( "----------------  feeding3  ------------------------" );

    Serial.println( "----------------------------------------" );



    Serial.println( "1 Starting loop action  " + timeManager.nowString() );
    timeManager.addTimeAction( millis() + 14000, printAction0 );
    Serial.println( "2 Starting loop action  " + timeManager.nowString() );
    timeManager.addTimeAction( timeManager.getFutureMillis( timeManager.toTmElement( now() ) ) + 12000, printAction1, 11 );
    ul = 30000;
    Serial.println( "millis: " + String( 1000 ) + " At: " + timeManager.formatTimeString( timeManager.millisToTimeElements( 1000 ) ) );
    Serial.println( "millis: " + String( ul ) + " At: " + timeManager.formatTimeString( timeManager.millisToTimeElements( ul ) ) );
    timeManager.addTimeAction( 20000, printAction0 );
    timeManager.addTimeAction( ul + 1712, printAction2, 11, 23 );
    timeManager.addTimeAction( ul + 1710, printAction0 );
    timeManager.addTimeAction( ul + 3711, printAction1, 100 );
    Serial.println( "3 Starting loop action  " + timeManager.nowString() );

    Serial.println( "Next action to re executed: " + String( timeManager.getNextActionTriggerMillis() ) + " At: " +
                    timeManager.formatTimeString( timeManager.millisToTimeElements( timeManager.getNextActionTriggerMillis() ) ) );
    Serial.println( "Last action to re executed: " + String( timeManager.getLastActionTriggerMillis() ) + " At: " +
                    timeManager.formatTimeString( timeManager.millisToTimeElements( timeManager.getLastActionTriggerMillis() ) ) );

    initDisplay();
    float sizeInGB = static_cast< double >( SD.cardSize() ) / ( 1024 * 1024 * 1024 );
    float freeSizeInGB = static_cast< double >( SD.totalBytes() ) / ( 1024 * 1024 * 1024 );
    drawCentered( "SD Card size", 100 );
    tft.setFreeFont( FF17 );
    msg = "Free " + String( freeSizeInGB ) + " GB  " + String( sizeInGB ) + " GB  ";
    drawCentered( msg, 120 );
    testSDSaveLoad();
    portioner.enableServo( true );
    portioner.readPortionWeightInGrams( true );
    tft.drawString( "Portion",  140, 180 );
    tft.drawString( "Storage", 140, 220 );
}

const int xWeight= 240;

int actionCount = 0;
int counter = 0;
void loop()
{

    //Erase last message

    portionWeightMsgLast = portionWeightMsg;
    storageWeightMsgLast = storageWeightMsg;
    portionWeightMsg = String( portioner.readPortionWeightInGrams( true, 4 ) ) + String( " gr." );
    storageWeightMsg = String( portioner.readStorageWeightInGrams( true, 4 ) ) + String( " gr." );

    tft.setTextColor( TFT_BLACK );
    tft.drawString( portionWeightMsgLast, xWeight, 180 );
    tft.drawString( storageWeightMsgLast, xWeight, 220 );

    //Draw new message
    tft.setTextColor( TFT_WHITE );
    tft.drawString( portionWeightMsg, xWeight, 180 );
    tft.drawString( storageWeightMsg, xWeight, 220 );

    //only toggle every 5th loop
    if( counter++ % 5 == 0 )
    {
        toggle = !toggle;
        portioner.enableStepper( toggle );
        portioner.enableStepper( false );


        if( toggle )
            portioner.openDispensingDoor();
        else
            portioner.closeDispensingDoor();
    }
    else
    {
        portioner.enableServo( false );
    }
    enableAllMCP23017Pins( toggle );
    if( toggle )
    {
        portioner.rotateStepper( -360 / 5 );
    }

    if( actionCount != timeManager.getActionCount() )
    {
        actionCount = timeManager.getActionCount();
        if( actionCount > 0 )
        {
            Serial.print( "Action count: " + String( actionCount ) );
            Serial.print( " Next action trigger millis: " + String( timeManager.getNextActionTriggerMillis() ) );
            Serial.println( " Next action trigger time  : " +
                            timeManager.formatTimeString( timeManager.millisToTimeElements( timeManager.getNextActionTriggerMillis() ) ) );
            if( timeManager.getActionCount() == 1 )
            {
                Serial.println( "----------This is the last action to execute ----------" );
            }
        }
    }

    Serial.println( "4 Starting loop action  " + timeManager.nowString() );
    if( timeManager.handleTimeActions() )
    {
        Serial.println( "Action executed" );
    }
    delay( 1000 );
    timeManager.checkResyncWithRtc();


}