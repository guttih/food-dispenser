
// #include <Arduino.h>
// #include <Weigher.h>
// #include "AugerController.h"
// #include "PortionManager.h"
// #include "display.h"
// #include "constants.h"
// #include "secret.h"
// #include "DispensingController.h"

// #include <WiFi.h>
// #include "WebServerHandler.h"

// #include <Wire.h>

// #include <TimeLib.h>
// #include "TimeManager.h"
// #include <DS1307RTC.h>


// Weigher weigher( PORTION_SCALE_DT_PIN, PORTION_SCALE_SCK_PIN, 105.151169 );
// DispensingController dispenser( SERVO_PIN, SERVO_CHANNEL );
// AugerController auger( MOTOR_PIN_DIRECTION, MOTOR_PIN_STEP, MOTOR_PIN_ENABLE );
// PortionManager portioner( &auger, &dispenser,  &weigher );
// TimeManager timeManager;

// #define CIRCLE_COUNT 6


// const int servoY = 172;


// WebServerHandler webServer;

// void printAction0()
// {
//     Serial.println( "Action 0 Executed" );
// }
// void printAction1( int a )
// {
//     Serial.println( "Action 1 Executed a: " + String( a ) );
// }

// void printAction2( int a, int b )
// {
//     Serial.println( "Action 2 Executed, a: " + String( a ) + ", b: " + String( b ) );
// }


// bool isDeviceConnectedAt( byte address )
// {
//     Wire.beginTransmission( address );
//     byte error = Wire.endTransmission();
//     return error == 0; // If no error, then a device was found at this address
// }



// constexpr uint8_t IO_A_DIRECTION_REG = 0x00;
// constexpr uint8_t IO_B_DIRECTION_REG = 0x01;
// constexpr uint8_t IO_A_REGISTER = 0x12;
// constexpr uint8_t IO_B_REGISTER = 0x13;

// void initAllMCP23017Pins( bool asOutput = true )
// {
//     String actionDescription = "Setting all pins as " + String( asOutput ? "OUTPUT" : "INPUT" ) + "  pins";

//     // Command to set all pins on Port A AND B to input (0 for output, 1 for input)
//     int action = asOutput ? 0x00 : 0xFF;
//     Serial.println( actionDescription );
//     Wire.beginTransmission( ADDRESS_MCP23017 );
//     Wire.write( IO_A_DIRECTION_REG );// Select Port A direction register
//     Wire.write( action );// Set all pins on Port A to output (0 for output, 1 for input)
//     Wire.endTransmission();

//     Wire.beginTransmission( ADDRESS_MCP23017 );
//     Wire.write( IO_B_DIRECTION_REG ); // Select Port B direction register
//     Wire.write( action ); // Set all pins on Port B to output
//     Wire.endTransmission();
// }

// void enableAllMCP23017Pins( bool enable )
// {
//     String actionDescription = "Powering " + String( enable ? "on" : "off" ) + " all MCP23017 pins";
//     int action = enable ? 0xFF : 0x00;

//     Serial.println( actionDescription );
//     Wire.beginTransmission( ADDRESS_MCP23017 );
//     Wire.write( IO_A_REGISTER );// Select Port A register
//     Wire.write( action );// Set all pins on Port A to output (0 for output, 1 for input)
//     Wire.endTransmission();

//     Wire.beginTransmission( ADDRESS_MCP23017 );
//     Wire.write( IO_B_REGISTER );
//     Wire.write( action );
//     Wire.endTransmission();
// }
// void setup()
// {
//     Serial.begin( 115200 );
//     Serial.println( "Starting" );

//     Wire.begin( SDA_PIN, SCL_PIN );
//     timeManager.initRtc( &Wire, ADDRESS_RTC );
//     if( isDeviceConnectedAt( ADDRESS_MCP23017 ) )
//     {
//         Serial.printf( "A device is connected at address 0x%02X.\n", ADDRESS_MCP23017 );
//     }
//     else
//         Serial.printf( "No device is connected at address 0x%02X\n", ADDRESS_MCP23017 );

//     // Checking if a device is connected at address 0x68
//     if( timeManager.isDeviceConnected() )
//     {
//         Serial.printf( "Syncing with RTC %s\n", timeManager.syncWithRtc() ? "was a success." : "failed!" );
//     }
//     else
//         Serial.printf( "No device is connected at address 0x%02X\n", ADDRESS_RTC );

//     tmElements_t tm; time_t t;




//     initAllMCP23017Pins();
//     enableAllMCP23017Pins( true );
//     portioner.begin();
//     displaySetup();
//     WiFi.begin( WIFI_SSID, WIFI_PASSWORD );

//     // // Check the Wi-Fi is connected to wifi network
//     while( WiFi.status() != WL_CONNECTED )
//     {
//         delay( 1000 );
//         Serial.println( "WiFi Connecting" );
//     }
//     Serial.println( WiFi.localIP() );

//     webServer.begin();
// }

// float weight =-1;
// float lastWeight = -1;
// static uint32_t stepperTime;
// bool enableStuff = true;
// tmElements_t tm;

// void loop()
// {
//     Serial.println( "4 Starting loop action  " + timeManager.nowString() );
//     if( timeManager.handleTimeActions() )
//     {
//         Serial.println( "Action left: " + String( timeManager.getActionCount() ) );
//         // Serial.println( "Next action trigger millis: " +
//         //                 String( timeManager.getNextActionTriggerTime() ) +
//         //                 "Next action trigger time  : " +
//         //                 timeManager.formatTimeString( timeManager.millisToTimeElements( timeManager.getNextActionTriggerTime() ) ) );
//     }
//     delay( 1000 );
//     Serial.println( "6 Starting loop action  " + timeManager.nowString() );
//     timeManager.checkResyncWithRtc();
//     Serial.println( "7 Starting loop action  " + timeManager.nowString() );
//     enableStuff = !enableStuff;
//     Serial.println( timeManager.nowString() );
//     // if( enableStuff )
//     //     portioner.rotateStepper( 360 );

//     delay( 500 );
//     // else
//     //     portioner.closeDispensingDoor();

//     // weight = portioner.readWeightInGrams( true );
//     // if( lastWeight != weight )
//     // {

//     //     lastWeight = weight;
//     // }

// }