#ifndef WEIGHER_H
#define WEIGHER_H

#include "HX711.h"

/**
 * @brief This class is used to read the weight from the load cell.
 *
 * Possibly this class could be put in the lib folder, but let's check
 * when more functionality has been added.
 *
 */

class Weigher
{
public:
    Weigher( short pinData, short pinClock, float scaleFactor );
    Weigher( short pinData, short pinClock, long scaleOffset, float scaleFactor );
    void begin( long delayTime = 500, uint8_t sampleCountForTare = 20 );
    float readWeightInGrams( bool waitForScaleToBeReady, uint8_t sampleCount = 1 );
    ~Weigher();

private:
    HX711 *_scale;
    short _pinData = 0;
    short _pinClock = 0;;
    float _scaleFactor = 0;
    long _scaleOffset = 0;

};

#endif