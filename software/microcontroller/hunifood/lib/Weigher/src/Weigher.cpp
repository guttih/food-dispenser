#include "Weigher.h"

Weigher::Weigher( short pinData, short pinClock, float scaleFactor )
{
    _pinData = pinData;
    _pinClock = pinClock;
    _scaleFactor = scaleFactor;
    _scale = new HX711();
}

Weigher::Weigher( short pinData, short pinClock, long scaleOffset, float scaleFactor )
{
    _pinData = pinData;
    _pinClock = pinClock;
    _scaleOffset = scaleOffset;
    _scaleFactor = scaleFactor;
    _scale = new HX711();
}

Weigher::~Weigher()
{

}

/**
 * @brief Tare the scale
 *
 * @param delayTime - Time to wait in milliseconds, before starting tare.
 *                    It's best to wait a bit for the circuit to
 *                    settle after powering up the whole system.

 * @param sampleCountForTare - Number of samples to take for tare.
 *                             The more samples, the more accurate
 *                             the tare, but the longer it takes.
 */
void Weigher::begin( long delayTime, uint8_t sampleCountForTare )
{
    _scale->begin( _pinData, _pinClock );
    _scale->set_scale( _scaleFactor );
    _scale->tare( sampleCountForTare );
}

float Weigher::readWeightInGrams( bool waitForScaleToBeReady, uint8_t sampleCount )
{
    while( waitForScaleToBeReady && !_scale->is_ready() )
    {
        delay( 10 );
    }
    return _scale->get_units( sampleCount );
}