#ifndef DAILY_RECURRENCE_H
#define DAILY_RECURRENCE_H

#include "RecurrenceType.h"
#include "Recurrence.h"
#include <TimeLib.h>

class DailyRecurrence : public Recurrence
{
public:

    DailyRecurrence( time_t startTime, time_t stopTime = MAX_LONG ) : Recurrence( startTime, stopTime )
    {

    }
    ~DailyRecurrence()
    {
    }

    RecurrenceType getType() const override
    {
        return RecurrenceType::DAILY;
    }

    /**
     * @brief Assignment operator
     *
     *
     * @param other - the other DailyRecurrence to assign to this DailyRecurrence
     * @example Usage example:
     *    DailyRecurrence dr1(now(), now()*SECS_PER_DAY);
     *    DailyRecurrence dr2 = dr1;
     *
     * @return DailyRecurrence&
     */
    DailyRecurrence& operator=( const DailyRecurrence& other )
    {
        if( this != &other && this->getType() == other.getType() )
        {
            this->_startTime = other._startTime;
            this->_stopTime = other._stopTime;
        }
        return *this;
    }


    bool equals( const Recurrence& other ) const
    {
        if( other.getType() == RecurrenceType::DAILY )
        {
            const DailyRecurrence* derivedOther = static_cast< const DailyRecurrence* >( &other );
            return derivedOther && this->getStartTime() == other.getStartTime() && this->getStopTime() == other.getStopTime();
        }

        return false;

    }

    time_t getNextTrigger( time_t currentTime ) const override
    {
        if( _stopTime < currentTime )
        {
            // Serial.println( "DailyRecurrence::getNextTrigger: _stopTime < currentTime" );
            return TimeHelper::calculateNextTriggerTime( _startTime, _stopTime, _stopTime );
        }
        return TimeHelper::calculateNextTriggerTime( _startTime, _stopTime, currentTime );
    }

    /**
     * @brief Compare this recurrence with another recurrence
     *
     * @param currentTime - the current time to calculate the next trigger time
     * @param other - the other recurrence to compare with
     * @return int
     *  a negative number if this recurrence is less than the other recurrence
     * 0 if this recurrence is equal to the other recurrence
     * a positive number if this recurrence is greater than the other recurrence
     */
    int compareNextTriggers( time_t currentTime, const Recurrence& other ) const override
    {
        return this->getNextTrigger( currentTime ) - other.getNextTrigger( currentTime );
    }

    String serialize() const override
    {
        DynamicJsonDocument doc( 1024 );
        doc[ ATTR_TYPE ] = getTypeString();
        doc[ ATTR_STARTTIME ] = _startTime;
        doc[ ATTR_STOPTIME ] = _stopTime;


        return doc.as< String >();
    }

    bool deserialize( const String& data ) override
    {
        DynamicJsonDocument doc( 1024 );
        deserializeJson( doc, data );
        if( doc.isNull() || doc.size() == 0 || !doc.containsKey( ATTR_TYPE ) || doc[ ATTR_TYPE ].as< String >() != getTypeString() ||
            !doc.containsKey( ATTR_STARTTIME ) || !doc.containsKey( ATTR_STOPTIME ) )
        {
            Serial.println( "--------Fail: deserialize Invalid JSON DailyRecurrence type ----------" );
            return false;
        }
        _startTime = doc[ ATTR_STARTTIME ].as< time_t >();
        _stopTime = doc[ ATTR_STOPTIME ].as< long >();
        return true;
    }

};

#endif