#ifndef EVENT_HELPER_H
#define EVENT_HELPER_H

#include <Arduino.h>

enum class EventType {
    BASIC,
    FEEDING,
    UNKNOWN,
};

class EventTypeHelper
{
public:
    static String toString( EventType type )
    {
        switch( type )
        {
            case EventType::BASIC:
                return "BASIC";
            case EventType::FEEDING:
                return "FEEDING";
            default:
                return "UNKNOWN";
        }
    }

    static EventType fromString( String type )
    {
        if( type == toString( EventType::BASIC ) )
        {
            return EventType::BASIC;
        }
        else if( type == toString( EventType::FEEDING ) )
        {
            return EventType::FEEDING;
        }
        else
        {
            return EventType::UNKNOWN;
        }
    }

    static bool isValid( EventType type )
    {
        return type != EventType::UNKNOWN;
    }

    static bool isValid( String type )
    {
        return isValid( fromString( type ) );
    }
};

#endif