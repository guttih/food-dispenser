
#ifndef WEEKLY_RECURRENCE_H
#define WEEKLY_RECURRENCE_H

#include "RecurrenceType.h"
#include "DailyRecurrence.h"
#include "TimeHelper.h"
#include <TimeLib.h>

#define ATTR_WEEKDAYS "weekdays"

class WeeklyRecurrence : public DailyRecurrence
{
public:
    /**
     * @brief Constructs a instance of WeeklyRecurrence
     *
     * This constructor initializes a new instance of WeeklyRecurrence class with the specified start time,
     * repeat count, and weekdays.
     *
     * @param startTime Represents the start time of the recurrence.
     * @param repeatCount The number of times the recurrence is repeated. If not specified, it defaults to MAX_LONG,
     * meaning the recurrence will repeat indefinitely.
     * @param weekdays The days in a week on which the recurrence takes place. If not specified, it defaults to WEEKLY_ALL,
     * meaning the recurrence takes place on all days of the week.
     *
     * @example Usage example:
     *
     *      // Create a recurring event that starts from a specific time,
     *      // repeats every day and stops repeating after 10 days
     *      WeeklyRecurrence wr1(now(), now()*SECS_PER_DAY);
     *
     *
     *      //repeats indefinitely only on weekends
     *      time_t start = makeTime( TimeHelper::createTmElement( 2023, 12, 16, 10, 10, 0 ) );
     *      WeeklyRecurrence wr2(start, MAX_LONG, WEEKLY_WEEKEND);
     *
     *       // This creates a 'WeeklyRecurrence' instance named wr3.
     *       // It represents an event recurring weekly, that is set to start at the current time (as provided by now()).
     *       // The event is scheduled to repeat every 30 days from its start time.
     *       // Furthermore, within each week, the event will only occur on Mondays, Wednesdays, and Fridays.
     *       // The 'CombineWeekdays' function is utilized to specify these three weekdays for the recurrence.
     *      WeeklyRecurrence wr3( now(), ( now() * SECS_PER_DAY * 30 ), CombineWeekdays( WEEKLY_MONDAY, WEEKLY_WEDNESDAY, WEEKLY_FRIDAY ) );
     */
    WeeklyRecurrence( time_t startTime, long repeatCount = MAX_LONG, Weekdays weekdays = WEEKLY_ALL )
        : DailyRecurrence( startTime, repeatCount )
    {
        setWeekdays( weekdays );
    }

    void setWeekdays( Weekdays weekdays )
    {
        _weekdays = weekdays;
    }

    Weekdays getWeekdays() const
    {
        return _weekdays;
    }

    String getWeekdaysAsString() const override
    {
        return TimeHelper::WeekdaysToString( _weekdays );
    }


    RecurrenceType getType() const override
    {
        return RecurrenceType::WEEKLY;
    }

    String formatTimeStringWithWeekday( time_t time ) const
    {
        return TimeHelper::formatTimeString( time ) + " " + TimeHelper::getWeekdayString( time );
    }

    time_t getNextTrigger( time_t currentTime ) const override
    {
        // Implement the logic to calculate the next trigger for weekly recurrence,
        // considering the Weekdays stored in _Weekdays.
        // If _Weekdays is set to WEEKLY_ALL, trigger on all days.

        time_t firstTrigger = DailyRecurrence::getNextTrigger( currentTime );
        time_t test = firstTrigger;

        if( _weekdays == WEEKLY_ALL )
        {
            return test;
        }
        int count =1;
        while( !TimeHelper::isTimeDayOfWeekInWeekdays( test, _weekdays ) && ( count < 8 ) && ( test < _stopTime ) )
        {
            count++;
            test+=SECS_PER_DAY;
            test = getNextTrigger( test );
        }

        if( test > _stopTime || count > 7 || !TimeHelper::isTimeDayOfWeekInWeekdays( test, _weekdays ) )
        {
            return firstTrigger;
        }

        if( test > _stopTime || count > 7 || !TimeHelper::isTimeDayOfWeekInWeekdays( test, _weekdays ) )
        {
            return firstTrigger;
        }
        else
        {
            return test;
        }

    }
    WeeklyRecurrence& operator=( const WeeklyRecurrence& other )
    {
        if( this != &other && this->getType() == other.getType() )
        {
            // Manually copy member variables of the base DailyRecurrence class
            this->_startTime = other._startTime;
            this->_stopTime = other._stopTime;

            // Copying the additional member variable of WeeklyRecurrence
            this->_weekdays = other._weekdays;
        }
        return *this;
    }
    bool equals( const Recurrence& other ) const
    {
        if( other.getType() == RecurrenceType::WEEKLY )
        {
            const WeeklyRecurrence* derivedOther = static_cast< const WeeklyRecurrence* >( &other );
            return derivedOther
                   && this->getStartTime() == other.getStartTime()
                   && this->getStopTime() == other.getStopTime()
                   && this->getWeekdays() == derivedOther->getWeekdays();
        }

        return false;

    }

    String serialize() const override
    {
        DynamicJsonDocument doc( 1024 );
        doc[ ATTR_TYPE ] = getTypeString();
        doc[ ATTR_STARTTIME ] = _startTime;
        doc[ ATTR_STOPTIME ] = _stopTime;
        doc[ ATTR_WEEKDAYS ] = _weekdays;
        return doc.as< String >();
    }

    bool deserialize( const String& data ) override
    {
        DynamicJsonDocument doc( 1024 );
        deserializeJson( doc, data );
        if( doc.isNull() || doc.size() == 0 || !doc.containsKey( ATTR_TYPE ) || doc[ ATTR_TYPE ].as< String >() != getTypeString() ||
            !doc.containsKey( ATTR_STARTTIME ) || !doc.containsKey( ATTR_STOPTIME ) || !doc.containsKey( ATTR_WEEKDAYS ) )
        {
            Serial.println( "--------Fail: deserialize Invalid JSON WeeklyRecurrence type ----------" );
            return false;
        }
        _startTime = doc[ ATTR_STARTTIME ].as< time_t >();
        _stopTime = doc[ ATTR_STOPTIME ].as< long >();
        _weekdays = static_cast< Weekdays >( doc[ ATTR_WEEKDAYS ].as< int >() );
        return true;
    }

private:

    Weekdays _weekdays;

};

#endif // WEEKLY_RECURRENCE_H
