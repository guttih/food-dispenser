#ifndef SD_CARD_STORAGE_H
#define SD_CARD_STORAGE_H

#include "IStorage.h"
#include "Event.h"
#include "DailyRecurrence.h"
#include "ScheduledEvent.h"

#include <SD.h>
#include <SPI.h>

// Implement the 'storage' interface for SD card.
class SDCardStorage : public IStorage
{

private:
    // SD card chip select pin
    uint8_t _csPin;

public:

    bool begin()
    {
        if( !SD.begin( _csPin ) )
        {
            Serial.println( "SDCardStorage::initSD() error initializing SD card on pin " + String( _csPin ) );
            return false;
        }
        return true;
    }
    SDCardStorage( uint8_t csPin = SD_CS )
    {
        _csPin = csPin;
    }

    bool load( std::vector< std::shared_ptr< ScheduledEvent > >& eventList ) override
    {
        Serial.println( "SDCardStorage::load()" );
        File dataFile = SD.open( getFullFileName() ); // Open the events.dat file directly.

        if( dataFile )
        {
            String contents;
            while( dataFile.available() )
            {
                char ch = ( char ) dataFile.read();
                if( ch != '\n' )
                {
                    contents += ch;
                }
                else
                {
                    if( contents.length() > 0 )
                    {
                        std::shared_ptr< ScheduledEvent > se = std::make_shared< ScheduledEvent >( "",
                                                                                                   std::make_shared< Event >( "" ),
                                                                                                   std::make_shared< DailyRecurrence >( 0 ) );
                        se->deserialize( contents ); // Deserialize the contents.
                        Serial.println( "Loading: " + contents ); // Prints "Loading: <serialized event>
                        eventList.push_back( se ); // Add the event to the event list.
                        contents = "";         // Clear the string for the next line.
                    }
                }
            }

            // To handle the last line if it does not end with '\n'
            if( contents.length() > 0 )
            {
                std::shared_ptr< ScheduledEvent > se = std::make_shared< ScheduledEvent >( "",
                                                                                           std::make_shared< Event >( "" ),
                                                                                           std::make_shared< DailyRecurrence >( 0 ) );
                se->deserialize( contents ); // Deserialize the contents.
                eventList.push_back( se ); // Add the event to the event list.
            }

            dataFile.close(); // Close the file.
            return true;
        }
        else
        {
            Serial.println( "SDCardStorage::load() error opening /events.dat" ); // Error message if file opening fails.
            return false;
        }
    }

    bool save( const std::vector< std::shared_ptr< ScheduledEvent > >& eventList ) override
    {
        Serial.println( "SDCardStorage::save()" );
        File dataFile = SD.open( getFullFileName(), FILE_WRITE ); // Open the events.dat file for writing.

        if( dataFile )
        {
            for( const auto& se : eventList ) // for each ScheduledEvent in the list
            {
                String contents = se->serialize(); // Serializes the scheduled event.
                Serial.println( "Saving: " + contents ); // Prints "Saving: <serialized event>
                dataFile.println( contents );   // Write the serialized event to the file.
            }

            dataFile.close(); // Close the file.
            return true;
        }
        else
        {
            Serial.println( "SDCardStorage::save() error opening /events.dat for writing" ); // Error message if file opening fails.
            return false;
        }
    }

    String getFullFileName()
    {
        //ends with slash or not
        if( dir.endsWith( "/" ) )
            return dir + _fileName;
        else
            return dir + "/" + _fileName;
    }
    String createId( unsigned long epochTime = 0 ) override
    {
        if( !seedInitialized_ )
        {
            randomSeed( analogRead( 0 ) );
            seedInitialized_ = true;
        }

        // If epochTime is 0 (default value), get the current time using TimeLib's now() function
        if( epochTime == 0 )
        {
            epochTime = now();
        }

        unsigned long currentTimeMillis = millis() % 1000;
        int randomValue = random( 0, 9999 );

        char randomStr[ 5 ]; // Buffer to hold the zero-padded string of randomValue
        sprintf( randomStr, "%04d", randomValue ); // Format randomValue with zero-padding

        String id = String( epochTime ) + String( currentTimeMillis ) + String( randomStr ); // Append the formatted randomValue string

        return id;
    }

private:
    String dir = "/";
    String _fileName =  "events.dat";
    bool seedInitialized_ = false;

};


#endif