#ifndef EVENT_H
#define EVENT_H

#include <ArduinoJson.h>
#include "EventType.h"
#include "TimeLib.h"

 #define  ATTR_TYPE "type"
 #define  ATTR_DESCRIPTION "description"

// Base class for all event types This class can be instantiated and can be used to represent a basic event that do not need any values

class Event
{
public:
    Event( String _description ) :  _description( _description )
    {
    }
    virtual ~Event()
    {
    }

    virtual EventType getType() const
    {
        return EventType::BASIC;
    }

    String getTypeString() const
    {
        return EventTypeHelper::toString( getType() );
    }


    void setDescription( const String& desc )
    {
        _description = desc;
    }
    const String &getDescription() const
    {
        return _description;
    }

    virtual String serialize() const
    {
        DynamicJsonDocument doc( 512 );
        doc[ ATTR_TYPE ] = getTypeString();
        doc[ ATTR_DESCRIPTION ] = _description;
        return doc.as< String >();
    }

    virtual bool deserialize( const String& input )
    {
        DynamicJsonDocument doc( 512 );
        deserializeJson( doc, input );
        if( doc.isNull() || doc.size() == 0 || !doc.containsKey( ATTR_TYPE ) || doc[ ATTR_TYPE ].as< String >() != getTypeString() )
        {
            Serial.println( "--------Fail: deserializing, Throwing Invalid JSON event type ----------" );
            return false;
        }
        _description = doc[ ATTR_DESCRIPTION ].as< String >();
        return true;
    }


    Event& operator=( const Event& other )
    {
        // self-assignment guard
        if( this == &other || this->getType() != other.getType() )
            return *this;

        // copy data from other's fields to this object's fields
        _description = other._description;

        // return the existing object so we can chain this operator
        return *this;
    }

    virtual bool equals( const Event& other ) const
    {
        return this->getType() == other.getType() && this->getDescription() == other.getDescription();
    }

    bool operator==( const Event& other ) const
    {
        return equals( other );
    }

    bool operator!=( const Event& other ) const
    {
        return !equals( other );
    }


protected:
    String _description;
};

#endif