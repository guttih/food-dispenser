#ifndef RECURRENCE_H
#define RECURRENCE_H

#include <ArduinoJson.h>
#include <TimeLib.h>
#include "RecurrenceType.h"
#include "TimeHelper.h"

#define ATTR_TYPE "type"
#define ATTR_STARTTIME "startTime"
#define ATTR_STOPTIME "stopTime"

const long MAX_LONG = 2147483647L;

// Base class for all recurrence types This class is abstract and should not be instantiated

class Recurrence
{
public:
    Recurrence( time_t startTime, time_t stopTime = MAX_LONG ) : _startTime( startTime )
    {
        setStopTime( stopTime );
    }
    virtual ~Recurrence()
    {
    }

    virtual RecurrenceType getType() const
    {
        return RecurrenceType::BASIC;
    }

    time_t getStopTime() const
    {
        return _stopTime;
    }

    virtual void setStopTime( time_t stopTime )
    {
        if( stopTime <= _startTime )
        {
            Serial.println( "Recurrence::setStopTime: stopTime cannot be less than startTime, setting stopTime=startTime" );
            _stopTime = _startTime;
        }
        else
        {
            // stopTime > startTime
            time_t datePartStart = TimeHelper::extractDate( _startTime );
            time_t datePartStop = TimeHelper::extractDate( stopTime );
            if( datePartStart == datePartStop )
            {
                // Same day
                time_t timePartStart = TimeHelper::extractTime( _startTime );
                time_t timePartStop = TimeHelper::extractTime( stopTime );
                if( timePartStop < timePartStart )
                {
                    // stop time < start time
                    Serial.println( "Recurrence::setStopTime: time part of stop time is less than start time, using start time part - one day" );
                    return setStopTime( TimeHelper::joinExtractedDateAndTime( datePartStart, timePartStart - SECS_PER_DAY ) );
                }
                //Time part stop time >= Time part start time, that's ok
            }
            _stopTime = stopTime;
        }
    }

    String getTypeString() const
    {
        return RecurrenceTypeHelper::toString( getType() );
    }


    virtual time_t getNextTrigger( time_t currentTime ) const = 0;
    virtual int compareNextTriggers( time_t currentTime, const Recurrence& other ) const = 0;

    /**
     * @brief Returns the number of seconds until the next trigger
     * If the recurrence is not active, returns last triggered time
     * @param currentTime
     * @return long
     */
    virtual long getSecondsUntilTriggered( time_t currentTime ) const
    {
        Serial.println( "Recurrence::getSecondsUntilTriggered next: " +
                        getNextTriggerAsString( currentTime ) + "(" +
                        String( getNextTrigger( currentTime ) ) +
                        ") current: " +
                        "(" +
                        String( currentTime ) +
                        ")" +
                        TimeHelper::formatTimeString( currentTime ) );
        return getNextTrigger( currentTime ) - currentTime;
    }


    void setStartTime( time_t startTime )
    {
        _startTime = startTime;
        setStopTime( _stopTime );
    }

    time_t getStartTime() const
    {
        return _startTime;
    }

    String getStartTimeAsString() const
    {
        return TimeHelper::formatTimeString( _startTime );
    }

    String getNextTriggerAsString( time_t currentTime ) const
    {
        return TimeHelper::formatTimeString( getNextTrigger( currentTime ) );
    }

    String formatTimeString( time_t t )
    {
        return TimeHelper::formatTimeString( t );
    }
    virtual String getWeekdaysAsString() const
    {
        return "";
    }
    String formatTimeStringWithWeekday( time_t t )
    {
        return TimeHelper::formatTimeString( t ) + " " + TimeHelper::weekdayNumberToString( weekday( t ) );
    }
    String formatTimeString( const tmElements_t tm )
    {
        return TimeHelper::formatTimeString( tm );
    }



    virtual bool equals( const Recurrence& other ) const
    {
        return this->getType() == other.getType() && this->getStartTime() == other.getStartTime() && this->getStopTime() == other.getStopTime();
    }

    bool operator==( const Recurrence& other ) const
    {
        return this->equals( other );
    }

    bool operator!=( const Recurrence& other ) const
    {
        return !this->equals( other );
    }

    // Implement serialization / deserialization methods
    virtual String serialize() const = 0;
    virtual bool deserialize( const String& data ) = 0;

protected:
    time_t _startTime;
    time_t _stopTime;

};

#endif