#ifndef SCHEDULED_EVENT_H
#define SCHEDULED_EVENT_H


#include "Event.h"
#include "Recurrence.h"
#include "DailyRecurrence.h"
#include "WeeklyRecurrence.h"
#include "FeedingEvent.h"
#include "EventType.h"
#include <memory>

#define ATTR_ID "id"
#define ATTR_EVENT "event"
#define ATTR_RECURRENCE "recurrence"
#define ATTR_PERSISTENT_STORAGE "persistentStorage"

// Enumerate the types of Recurrence (Daily, Recurring)




class ScheduledEvent
{
public:
    /**
     * @brief Constructor for the ScheduledEvent class.
     *
     * The ScheduledEvent object comprises of an event and recurrence. Both event and recurrence are
     * represented by shared pointers to generic objects of the related types. The shared pointers allow
     * the ScheduledEvent to easily manage the lifetimes of the encapsulated objects and prevent potential
     * memory leaks.
     *
     * @param id A unique string identifier for the event
     * @param event Shared pointer to an Event object
     * @param recurrence Shared pointer to a Recurrence object
     *
     * @note Both event and recurrence must be instantiated using std::make_shared before passing to
     * the constructor.
     *
     * ### Example
     *
     * ```cpp
     * auto event = std::make_shared<FeedingEvent>("I am a daily scheduled feeding event", 12);
     * auto recurrence = std::make_shared<DailyRecurrence>(now() + 60);
     * ScheduledEvent sd1("feeding1", event, recurrence);
     * ```
     *
     * Or, using save pointers:
     * ```cpp
     * std::shared_ptr< ScheduledEvent > se = std::make_shared< ScheduledEvent >( "", std::make_shared< Event >( "" ),
     *                                                                                std::make_shared< DailyRecurrence >( 0 ) );
    * ```
     * Or, using auto:
     * ```cpp
     *  auto sd2 = std::make_shared< ScheduledEvent >( "feeding2", std::make_shared< FeedingEvent >( "I am a daily scheduled feeding event", 13 ),
     *                                                             std::make_shared< DailyRecurrence >( now() + 60 ) );
     * ```
     */
    ScheduledEvent( const String& id, std::shared_ptr< Event > event, std::shared_ptr< Recurrence > recurrence, bool persistentStorage = true )
        : _id( id ), _event( std::move( event ) ), _recurrence( std::move( recurrence ) ), _persistentStorage( persistentStorage )
    {
    }

    ~ScheduledEvent() = default;

    void setId( const String& id )
    {
        this->_id = id;
    }

    const String& getId() const
    {
        return _id;
    }

    const Event * getEvent() const
    {
        return _event.get();
    }
    const Recurrence * getRecurrence() const
    {
        return _recurrence.get();
    }


    /**
     * @brief Check if the ScheduledEvent is a persistent storage event.
     * @return True if the ScheduledEvent should be stored in persistent storage, false otherwise.
     *
     * @note The default value is true.  This value cannot be changed after the ScheduledEvent is created.
     *
     */
    bool isPersistentStorage() const
    {
        return _persistentStorage;
    }


    time_t getNextTrigger( time_t currentTime ) const
    {
        return _recurrence->getNextTrigger( currentTime );
    }

    long getSecondsUntilTriggered( time_t currentTime ) const
    {
        return _recurrence->getSecondsUntilTriggered( currentTime );
    }

    ScheduledEvent& operator=( const ScheduledEvent& other )
    {
        if( this != &other && this->_event->getType() == other._event->getType() && this->_recurrence->getType() == other._recurrence->getType() )
        {
            _id = other._id;
            _persistentStorage = other._persistentStorage;
            _event = other._event; // shared_ptr handles copying
            _recurrence = other._recurrence; // shared_ptr handles copying
        }
        return *this;
    }

    bool equals( const ScheduledEvent& other ) const
    {
        return this->getId() == other.getId() &&
               this->isPersistentStorage() == other.isPersistentStorage() &&
               this->getEvent()->equals( *other.getEvent() ) &&
               this->getRecurrence()->equals( *other.getRecurrence() );
    }

    bool operator==( const ScheduledEvent& other ) const
    {
        return this->equals( other );
    }

    bool operator!=( const ScheduledEvent& other ) const
    {
        return !this->equals( other );
    }


    String getDescription() const
    {
        Serial.println( "---------------------------ScheduledEvent::getDescription() " + _event->getDescription() );
        return _event->getDescription();
    }

    String serialize() const
    {
        DynamicJsonDocument doc( 1024 );
        // JsonObject obj = doc.to< JsonObject >();
        doc[ ATTR_ID ] = _id;
        doc[ ATTR_EVENT ] = _event->serialize();
        doc[ ATTR_RECURRENCE ] = _recurrence->serialize();
        doc[ ATTR_PERSISTENT_STORAGE ] = _persistentStorage;

        return doc.as< String >();
    }



    void deserialize( const String& data )
    {
        DynamicJsonDocument doc( 1024 );
        deserializeJson( doc, data );
        _id = doc[ ATTR_ID ].as< String >();
        _persistentStorage = doc[ ATTR_PERSISTENT_STORAGE ].as< bool >();

        String eventJsonStr = doc[ ATTR_EVENT ].as< String >();
        String recurrenceJsonStr = doc[ ATTR_RECURRENCE ].as< String >();

        //Get the sub JSON objects
        DynamicJsonDocument eventDoc( 1024 );
        DynamicJsonDocument recurrenceDoc( 1024 );

        deserializeJson( eventDoc, eventJsonStr );
        deserializeJson( recurrenceDoc, recurrenceJsonStr );

        //Get the type of the event and recurrence
        String eTypeStr = eventDoc[ ATTR_TYPE ].as< String >();
        String rTypeStr = recurrenceDoc[ ATTR_TYPE ].as< String >();

        RecurrenceType rType = RecurrenceTypeHelper::fromString( rTypeStr );
        EventType eType = EventTypeHelper::fromString( eTypeStr );

        //Check if the sub object types are valid
        if( !RecurrenceTypeHelper::isValid( rType ) )
        {
            Serial.println( "ScheduledEvent::Throwing Invalid JSON recurrence type" );
            return;
            // throw std::runtime_error( "Invalid JSON recurrence type" );
        }
        if( !EventTypeHelper::isValid( eType ) )
        {
            Serial.println( "ScheduledEvent::Throwing Invalid JSON event type" );
            return;
            // throw std::runtime_error( "Invalid JSON event type" );
        }

        //Check if we need to delete the old objects and create new WEEKLY if the types are different

        //Recurrence objects
        if( rTypeStr != _recurrence->getTypeString() ) //if the recurrence type is different, delete the old recurrence object and create a new one
        {

            if( rType == RecurrenceType::DAILY )
            {
                _recurrence = std::make_shared< DailyRecurrence >( 0 );
            }
            else if( rType == RecurrenceType::WEEKLY )
            {
                _recurrence = std::make_shared< WeeklyRecurrence >( 0 );
            }

        }



        if( eTypeStr != _event->getTypeString() ) //if the event type is different, delete the old event object and create a new one
        {

            if( eType == EventType::FEEDING )
            {
                _event = std::make_shared< FeedingEvent >( "", 0 );
            }
            else if( eType == EventType::BASIC )
            {
                _event = std::make_shared< Event >( "" );
            }
        }

        _event->deserialize( eventJsonStr );
        _recurrence->deserialize( recurrenceJsonStr );
    }

    String getNextTriggerAsString( time_t currentTime )
    {
        return _recurrence->getNextTriggerAsString( currentTime );
    }
    String getNextTriggerWithWeekdayAsString( time_t currentTime )
    {
        time_t trigger = _recurrence->getNextTrigger( currentTime );
        return _recurrence->formatTimeStringWithWeekday( trigger );
    }

    time_t getStartTime() const
    {
        return _recurrence->getStartTime();
    }

    int compareNextTriggers( time_t currentTime, const ScheduledEvent& other ) const
    {
        return _recurrence->compareNextTriggers( currentTime, *other._recurrence );
    }


private:
    String _id;
    bool _persistentStorage = false;
    std::shared_ptr< Event > _event;
    std::shared_ptr< Recurrence > _recurrence;
};

#endif

