#ifndef STORAGE_H
#define STORAGE_H

#include "ScheduledEvent.h"
#include <vector>

// Define the 'storage' interface.
class IStorage
{
public:
    virtual ~IStorage() = default;
    virtual bool begin() = 0;
    virtual bool load( std::vector< std::shared_ptr< ScheduledEvent > >& eventList ) = 0;
    virtual bool save( const std::vector< std::shared_ptr< ScheduledEvent > >& eventList ) = 0;
    virtual String createId( unsigned long epochTime ) = 0;
};


#endif // STORAGE_H