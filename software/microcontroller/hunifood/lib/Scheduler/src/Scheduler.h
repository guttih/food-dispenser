#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <vector>
#include <algorithm>
#include "ScheduledEvent.h"
#include "IStorage.h"


enum class SortBy
{
    ID,
    ID_REVERSE,
    NEXT_TRIGGER,
    NEXT_TRIGGER_REVERSE
};

class Scheduler
{
public:
    Scheduler( std::shared_ptr< IStorage > storage ) : _storage( storage ), _sortOrder( SortBy::NEXT_TRIGGER )
    {
    }
    ~Scheduler() = default;

    /**
     * @brief Creates a new unique id
     *
     * @return String
     */
    String createId()
    {
        return _storage->createId( now() );
    }

    bool beginSD()
    {
        return _storage->begin();
    }

    /**
     * @brief Adds a new Event to the list in memory, you should rather use the addDailyEvent, addWeeklyEvent,
     * addDailyFeedingEvent or addWeeklyFeedingEvent methods for convenience and to avoid errors.
     *
     * @param event
     *
     * @returns a shared pointer to the ScheduledEvent that was added to the list
     *
     * @note The ScheduledEvent is not saved to persistent storage, you need to call save() to do that
     */
    std::shared_ptr< ScheduledEvent > add( std::shared_ptr< ScheduledEvent > event )
    {
        _eventList.push_back( event );
        sortList();
        return event;
    }

    /**
     * @brief Adds a new Event which is triggered every day at the specified time.
     *
     * @param description The description of the event
     * @param triggerTime Date when the first event should be triggered, the time part is then used for the daily trigger time every day after that
     * @param persistentStorage If true, the event should be stored in persistent storage(saved to sd card), false otherwise
     */
    std::shared_ptr< ScheduledEvent > addDailyEvent( const String& description, time_t triggerTime, bool persistentStorage = true )
    {
        std::shared_ptr< ScheduledEvent > sd1 =
            std::make_shared< ScheduledEvent >( createId(), std::make_shared< Event >( description ),
                                                std::make_shared< DailyRecurrence >( triggerTime ), persistentStorage );
        return add( sd1 );
    }

    /**
     * @brief Adds a new Event which is triggered every week at the specified time.
     *
     * @param description The description of the event
     * @param triggerTime Date when the first event should be triggered, the time part is then used for the weekly trigger time every week after that
     * @param daysOfWeek The days of the week when the event should be triggered
     * @param persistentStorage If true, the event should be stored in persistent storage(saved to sd card), false otherwise
     */

    std::shared_ptr< ScheduledEvent > addWeeklyEvent( const String& description, time_t triggerTime, Weekdays daysOfWeek,
                                                      bool persistentStorage = true )
    {
        std::shared_ptr< ScheduledEvent > sd1 =
            std::make_shared< ScheduledEvent >( createId(), std::make_shared< Event >( description ),
                                                std::make_shared< WeeklyRecurrence >( triggerTime, MAX_LONG, daysOfWeek ), persistentStorage );
        return add( sd1 );
    }

    /**
     * @brief Adds a new FeedingEvent which is triggered every day at the specified time.
     *
        * @param description The description of the event
        * @param portionSize The portion size of the feeding event
        * @param triggerTime Date when the first event should be triggered, the time part is then used for the daily trigger time every day after that
        * @param persistentStorage If true, the event should be stored in persistent storage(saved to sd card), false otherwise
    */
    std::shared_ptr< ScheduledEvent > addDailyFeedingEvent( const String& description, unsigned long portionSize, time_t triggerTime,
                                                            bool persistentStorage = true )
    {
        std::shared_ptr< ScheduledEvent > sd1 =
            std::make_shared< ScheduledEvent >( createId(), std::make_shared< FeedingEvent >( description, portionSize ),
                                                std::make_shared< DailyRecurrence >( triggerTime ), persistentStorage );

        return add( sd1 );
    }

    /**
     * @brief Adds a new FeedingEvent which is triggered every week at the specified time.
     *
        * @param description The description of the event
        * @param portionSize The portion size of the feeding event
        * @param triggerTime Date when the first event should be triggered, the time part is then used for the weekly trigger time every week after that
        * @param daysOfWeek The days of the week when the event should be triggered
        * @param persistentStorage If true, the event should be stored in persistent storage(saved to sd card), false otherwise

    */
    std::shared_ptr< ScheduledEvent > addWeeklyFeedingEvent( const String& description, unsigned long portionSize, time_t triggerTime, Weekdays daysOfWeek,
                                                             bool persistentStorage = true )
    {
        std::shared_ptr< ScheduledEvent > sd1 =
            std::make_shared< ScheduledEvent >( createId(), std::make_shared< FeedingEvent >( description, portionSize ),
                                                std::make_shared< WeeklyRecurrence >( triggerTime, MAX_LONG, daysOfWeek ), persistentStorage );

        return add( sd1 );
    }



    size_t count() const
    {
        return _eventList.size();
    }

    /**
     * @brief Removes all ScheduledEvents from the list in memory
     * If you save the list to a file after this, the file will be empty
     */
    void removeAllItems()
    {
        _eventList.clear();
    }

    String serializedList() const
    {
        String result;
        for( auto &event : _eventList )
        {
            result += event->serialize() + "\n";
        }

        return result;
    }

    /**
     * @brief Gets when the next event should be triggered
     *
     * @return time_t - (Epoch time) when the next event will happen. If there are no events in the list, -1 is returned.
     *
     * @note Please keep in mind that there could be events that are to be triggered in the past because the should only be triggered once.
     * You should:
     *   - check if the returned time is in the past and if so, trigger the event and remove it from the list.
     *   - check if there are any events in the list before calling this method.
     *   - consider using the getSecondsUntilTriggered() method for ease of use instead.
     */
    time_t getNextTriggerTime() const
    {
        if( _eventList.size() > 0 )
        {
            return _eventList[ 0 ]->getNextTrigger( now() );
        }
        else
        {
            return -1;
        }
    }

    /**
     * @brief Gets the number of seconds until the next event should be triggered
     *
     * @return long - the number of seconds until the next event should be triggered. If there are no events in the list, -1 is returned.
     *
     * @note You should check if there are any events in the list before calling this method.
     */
    long getSecondsUntilTriggered() const
    {
        return getSecondsUntilTriggered( now() );
    }

    /**
     * @brief Gets the number of seconds until the next event should be triggered compared to time passed in
     *
     * @param currentTime - the current time do relative calculations from
     *
     * @return long - the number of seconds until the next event should be triggered. If there are no events in the list, -1 is returned.
     *
     * @note You should check if there are any events in the list before calling this method.
     */

    long getSecondsUntilTriggered( time_t currentTime ) const
    {
        if( _eventList.size() > 0 )
        {
            return _eventList[ 0 ]->getSecondsUntilTriggered( currentTime );
        }
        else
        {
            return -1;
        }
    }



    /**
     * @brief Get the Top Item object
     *
     * @return std::shared_ptr< ScheduledEvent >
     */

    std::shared_ptr< ScheduledEvent > getTopItem() const
    {
        if( _eventList.size() > 0 )
        {
            return _eventList[ 0 ];
        }
        else
        {
            return nullptr;
        }
    }


    std::shared_ptr< ScheduledEvent > getItem( size_t position ) const
    {
        if( position >= 0 && position < _eventList.size() )
        {
            return _eventList[ position ];
        }
        else
        {
            return nullptr;
        }
    }

    /**
     * @brief Loads the list of ScheduledEvents from persistent storage and sorts it.
     *
     */
    bool load()
    {
        bool ret = _storage->load( _eventList );;
        sortList();
        return ret;
    }

    bool save() const
    {
        return _storage->save( _eventList );
    }

    void sortById( bool reverse = false )
    {
        setSortOrder( reverse ? SortBy::ID_REVERSE : SortBy::ID );
        sortList();
    }

    void sortByNextTrigger( time_t currentTime, bool reverse = false )
    {
        setSortOrder( reverse ? SortBy::NEXT_TRIGGER_REVERSE : SortBy::NEXT_TRIGGER );
        sortList( currentTime );
    }

private:

    void setSortOrder( SortBy sortOrder )
    {
        _sortOrder = sortOrder;
    }

    void sortList( time_t currentTime=0 )
    {
        if( currentTime == 0 )
        {
            currentTime = now();
        }
        switch( _sortOrder )
        {
            case SortBy::ID:
                executeSortById();
                break;
            case SortBy::ID_REVERSE:
                executeSortById( true );
                break;
            case SortBy::NEXT_TRIGGER:
                executeSortByNextTrigger( currentTime );
                break;
            case SortBy::NEXT_TRIGGER_REVERSE:
                executeSortByNextTrigger( currentTime, true );
                break;
            default:

                break;
        }
    }

    void executeSortById( bool reverse = false )
    {
        if( reverse )
        {
            std::sort( _eventList.begin(), _eventList.end(),
                       []( const std::shared_ptr< ScheduledEvent >& a, const std::shared_ptr< ScheduledEvent >& b ) {
                return a->getId() > b->getId();
            } );
        }
        else
        {
            std::sort( _eventList.begin(), _eventList.end(),
                       []( const std::shared_ptr< ScheduledEvent >& a, const std::shared_ptr< ScheduledEvent >& b ) {
                return a->getId() < b->getId();
            } );
        }
    }


    void executeSortByNextTrigger( time_t currentTime, bool reverse = false )
    {
        if( !reverse )
        {
            std::sort( _eventList.begin(), _eventList.end(),
                       [ currentTime ]( const std::shared_ptr< ScheduledEvent >& a, const std::shared_ptr< ScheduledEvent >& b ) {
                return a->compareNextTriggers( currentTime, *b ) < 0;
            } );
        }
        else
        {
            std::sort( _eventList.begin(), _eventList.end(),
                       [ currentTime ]( const std::shared_ptr< ScheduledEvent >& a, const std::shared_ptr< ScheduledEvent >& b ) {
                return a->compareNextTriggers( currentTime, *b ) > 0;
            } );
        }
    }

    std::vector< std::shared_ptr< ScheduledEvent > > _eventList;
    std::shared_ptr< IStorage > _storage;
    SortBy _sortOrder;

};

#endif // SCHEDULER_H