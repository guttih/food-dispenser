#ifndef RECURRENCE_HELPER_H
#define RECURRENCE_HELPER_H

#include <Arduino.h>

enum class RecurrenceType {
    BASIC,
    DAILY,
    WEEKLY,
    UNKNOWN,
};

class RecurrenceTypeHelper
{
public:
    static String toString( RecurrenceType type )
    {
        switch( type )
        {
            case RecurrenceType::BASIC:
                return "BASIC";
            case RecurrenceType::DAILY:
                return "DAILY";
            case RecurrenceType::WEEKLY:
                return "WEEKLY";
            default:
                return "UNKNOWN";
        }
    }

    static RecurrenceType fromString( String type )
    {
        if( type == toString( RecurrenceType::BASIC ) )
        {
            return RecurrenceType::BASIC;
        }
        else if( type == toString( RecurrenceType::DAILY ) )
        {
            return RecurrenceType::DAILY;
        }
        else if( type == toString( RecurrenceType::WEEKLY ) )
        {
            return RecurrenceType::WEEKLY;
        }
        else
        {
            return RecurrenceType::UNKNOWN;
        }
    }

    static bool isValid( RecurrenceType type )
    {
        return type != RecurrenceType::UNKNOWN;
    }

    static bool isValid( String type )
    {
        // use fromString to check if the string matches any of defined enum's string representation
        return isValid( fromString( type ) );
    }
};

#endif