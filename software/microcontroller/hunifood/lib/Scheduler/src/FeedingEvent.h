#ifndef FEEDING_EVENT_H
#define FEEDING_EVENT_H

#include "Event.h"
#include "EventType.h"

#define ATTR_PORTION_SIZE "portionSize"

class FeedingEvent : public Event
{
public:
    FeedingEvent( String _description, unsigned long portionSize )
        : Event( _description ), _portionSize( portionSize )
    {
    }

    ~FeedingEvent() override
    {
    }                           // Virtual Destructor

    EventType getType() const override
    {
        return EventType::FEEDING;
    }

    unsigned long getPortionSize() const
    {
        return _portionSize;
    }

    void setPortionSize( unsigned long portionSize )
    {
        _portionSize = portionSize;
    }


    String serialize() const override
    {
        DynamicJsonDocument doc( 512 );
        doc[ ATTR_TYPE ] = getTypeString();
        doc[ ATTR_DESCRIPTION ] = _description;
        doc[ ATTR_PORTION_SIZE ] = _portionSize;
        return doc.as< String >();
    }

    bool deserialize( const String& input ) override
    {
        DynamicJsonDocument doc( 512 );
        deserializeJson( doc, input );
        if( doc.isNull() || doc.size() == 0 || !doc.containsKey( ATTR_TYPE ) || doc[ ATTR_TYPE ].as< String >() != getTypeString() )
        {
            Serial.println( "--------Fail: deserialize Invalid JSON FeedingEvent type ----------" );
            return false;
        }
        _description = doc[ ATTR_DESCRIPTION ].as< String >();
        _portionSize = doc[ ATTR_PORTION_SIZE ].as< unsigned long >();
        return true;
    }

    FeedingEvent& operator=( const FeedingEvent& other )
    {
        // self-assignment guard
        if( this == &other || this->getType() != other.getType() )
            return *this;

        // base class assignment
        Event::operator=( other );

        // copy data from other's fields to this FeedingEvent's fields
        _portionSize = other._portionSize;

        // return the existing object so we can chain this operator
        return *this;
    }

    bool equals( const Event& other ) const override
    {
        return Event::equals( other ) && this->getPortionSize() == static_cast< const FeedingEvent& >( other ).getPortionSize();
    }

protected:
    unsigned long _portionSize;
};

#endif