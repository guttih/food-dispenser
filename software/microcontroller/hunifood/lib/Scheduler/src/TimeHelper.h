#ifndef TIMEHELPER_H
#define TIMEHELPER_H

#include <Arduino.h>
#include <TimeLib.h>

enum Weekdays
{
    // Decimal value: 0, Represents no days of the week
    WEEKLY_NONE = 0b00000000,
    // Decimal value: 1
    WEEKLY_SUNDAY = 0b00000001,
    // Decimal value: 2
    WEEKLY_MONDAY = 0b00000010,
    // Decimal value: 4
    WEEKLY_TUESDAY = 0b00000100,
    // Decimal value: 8
    WEEKLY_WEDNESDAY = 0b00001000,
    // Decimal value: 16
    WEEKLY_THURSDAY = 0b00010000,
    // Decimal value: 32
    WEEKLY_FRIDAY = 0b00100000,
    // Decimal value: 64
    WEEKLY_SATURDAY = 0b01000000,

    /**
     * @brief All days of the week
     * Decimal value: 127
     */
    WEEKLY_ALL = 0b01111111,

    /**
     * @brief Monday, Tuesday, Wednesday, Thursday and Friday
     * Decimal value: 62
     */
    WEEKLY_WORK = 0b00111110,
    /**
     * @brief Saturday and Sunday
     * Decimal value: 65
     */
    WEEKLY_WEEKEND = 0b01000001
};

class TimeHelper
{
public:
    static String formatTimeString( time_t t )
    {
        tmElements_t tm;
        breakTime( t, tm );
        return formatTimeString( tm );
    }
    static String formatTimeString( const tmElements_t tm )
    {
        char buf[ 150 ];
        snprintf( buf, sizeof( buf ), "%04d-%02d-%02d %02d:%02d:%02d", tmYearToCalendar( tm.Year ), tm.Month, tm.Day, tm.Hour, tm.Minute, tm.Second );
        return String( buf );
    }

    /*
    * @brief Creates a tmElements_t object from the specified time_t object
    *
    * @param year - int type.  The year to set in the tmElements_t object, offset from 1970 (e.g. 2021 = 51 and 2010 = 40)
    * @param month - byte type.  The month to set in the tmElements_t object (e.g. 1 = January and 12 = December)
    * @param day - byte type.  The day to set in the tmElements_t object (e.g. 1 = 1st and 31 = 31st)
    * @param hour - byte type.  The hour to set in the tmElements_t object (e.g. 0 = 12am and 23 = 11pm)
    * @param minute - byte type.  The minute to set in the tmElements_t object (e.g. 0 = 0 minutes and 59 = 59 minutes)
    * @param second - byte type.  The second to set in the tmElements_t object (e.g. 0 = 0 seconds and 59 = 59 seconds)
    * @return tmElements_t
    * tmElements_t object with the specified time fields set
    */
    static tmElements_t createTmElement( int year, byte month, byte day, byte hour, byte minute, byte second )
    {
        tmElements_t currentTime;
        currentTime.Year = CalendarYrToTm( year );
        currentTime.Month = month;
        currentTime.Day = day;
        currentTime.Hour = hour;
        currentTime.Minute = minute;
        currentTime.Second = second;

        return currentTime;
    }


    /**
     * @brief Joins the specified date and time parts into a time_t variable
     *
     * @param datePart - long type.  The date part to join with the time part (YY:MM:DD)
     * @param timePart - long type.  The time part to join with the date part (HH:MM:SS)
     * @return time_t
     * The time_t variable with the specified date and time parts joined
     */
    static time_t joinExtractedDateAndTime( long datePart, long timePart )
    {
        // Extract date components
        int dYear = datePart / 10000L;
        int dMonth = ( datePart % 10000L ) / 100L;
        int dDay = datePart % 100L;

        // Extract time components
        int dHour = timePart / 3600L;
        int dMinute = ( timePart % 3600L ) / 60L;
        int dSecond = timePart % 60L;

        // Setup tmElements_t structure
        tmElements_t tmSet;
        tmSet.Year = CalendarYrToTm( dYear );
        tmSet.Month = dMonth;
        tmSet.Day = dDay;
        tmSet.Hour = dHour;
        tmSet.Minute = dMinute;
        tmSet.Second = dSecond;

        // Generate the equivalent time_t value
        return makeTime( tmSet );
    }

    /**
     * @brief Extracts the time part from the specified time_t variable
     *
     * @param rawTime - time_t type.  The time to extract the time from
     * @return long - The time part of the specified time_t variable this is not a time_t variable, but a long variable with the the HH:MM:SS part.
     */
    static long extractTime( time_t rawTime )
    {
        // Notice that these functions take the raw time as their argument
        int hr = hour( rawTime );
        int min = minute( rawTime );
        int sec = second( rawTime );

        long returnTime = hr * 3600L + min * 60L + sec;

        return returnTime;
    }

    /**
     * @brief Extracts the date part from the specified time_t variable
     *
     * @param rawTime - time_t type.  The time to extract the date from
     * @return long - The date part of the specified time_t variable this is not a time_t variable, but a long variable with the YY:MM:DD part.
     */
    static long extractDate( time_t rawTime )
    {
        int iYear = year( rawTime );
        int iMonth = month( rawTime );
        int iDay = day( rawTime );

        long returnDate = iYear * 10000L + iMonth * 100L + iDay;

        return returnDate;
    }

    /**
    * @brief Calculates the next trigger time for this recurrence
    *
    * @param startTime - type_t type.  The start time of the recurrence
    * @param stopTime - type_t type.  The stop time of the recurrence
    * @param currentTime - type_t type.  If time part of this variable is less than the
    * When comparing the time part of currentTime and the member variable _startTime the following rules apply:
    * If time part currentTime <= _startTime, the next trigger date is same as currentTime
    * If time part currentTime >  _startTime, the next trigger date is same as currentTime + 1 day
    * If time part of currentTime > _stopTime, the next trigger date is same as _stopTime

    * @return time_t
    * time part of the returned value is always same as _startTime
    * date part of the returned value is either same as currentTime or currentTime + 1 day
    */
    static time_t calculateNextTriggerTime( time_t startTime, time_t stopTime, time_t currentTime )
    {

        time_t timePartOfStartTime = extractTime( startTime );
        time_t timePartOfCurrentTime = extractTime( currentTime );

        if( stopTime < currentTime )
        {
            time_t lastTrigger = joinExtractedDateAndTime( extractDate( stopTime ), timePartOfStartTime );

            while( lastTrigger > stopTime )
                lastTrigger -= SECS_PER_DAY;

            return lastTrigger;

        }

        if( timePartOfCurrentTime > timePartOfStartTime )
        {
            return joinExtractedDateAndTime( extractDate( currentTime + SECS_PER_DAY ), timePartOfStartTime );
        }
        return joinExtractedDateAndTime( extractDate( currentTime ), timePartOfStartTime );

    }

    /**
     * @brief Checks if the weekday of the specified time_t variable is in the specified weekdays
     *
     * @param timeToCheck - time_t type.  The time to extract the weekday from
     * @param weekdays - Weekdays type.  The weekdays that are valid
     * @return true - if the weekday of the specified time_t variable is in the specified weekdays
     * @return false - if the weekday of the specified time_t variable is not in the specified weekdays
     */
    static bool isTimeDayOfWeekInWeekdays( time_t timeToCheck, Weekdays weekdays )
    {
        // Retrieve weekday from time_t variable
        int wday = weekday( timeToCheck ); // This will return a value from 1 (Sunday) to 7 (Saturday)

        // Convert weekday to appropriate enum value
        // Be careful with off-by-one errors – TimeLib uses 1-based days and your enum uses 0-based days
        Weekdays enumDay = static_cast< Weekdays >( 1 << ( wday - 1 ) );

        // Check if the enumDay is in _weekdays
        return ( weekdays & enumDay ) != 0;
    }

    /**
     * @brief Converts the specified Weekdays variable to a comma-separated string of weekdays.
     *
     * If the Weekdays variable contains no weekdays, an empty string will be returned.
     * Otherwise, a comma-separated list of days will be returned.
     *
     * @param weekdays - Weekdays type. The weekdays to convert to a string.
     * @return String - Comma-separated string of weekdays.
     */
    static String WeekdaysToString( Weekdays weekdays )
    {
        String result = "";
        if( weekdays & Weekdays::WEEKLY_SUNDAY )
            result += "Sunday, ";
        if( weekdays & Weekdays::WEEKLY_MONDAY )
            result += "Monday, ";
        if( weekdays & Weekdays::WEEKLY_TUESDAY )
            result += "Tuesday, ";
        if( weekdays & Weekdays::WEEKLY_WEDNESDAY )
            result += "Wednesday, ";
        if( weekdays & Weekdays::WEEKLY_THURSDAY )
            result += "Thursday, ";
        if( weekdays & Weekdays::WEEKLY_FRIDAY )
            result += "Friday, ";
        if( weekdays & Weekdays::WEEKLY_SATURDAY )
            result += "Saturday, ";

        // Remove the last ", "
        if( result.length() > 2 )
            result.remove( result.length() - 2 );
        return result;
    }


    /**
     * @brief Converts a weekday value to a string
     *
     * @param weekday - int type.  The weekday value which where 1 is sunday and 7 is saturday
     * @return String - The string value for a given weekday.  (Only "Sunday" to "Saturday" are valid)
     */
    static String weekdayNumberToString( int weekday )
    {
        return WeekdaysToString( weekdayNumberToEnum( weekday ) );
    }

    static String getWeekdayString( time_t time )
    {
        return WeekdaysToString( weekdayNumberToEnum( weekday( time ) ) );
    }


    static int weekdaysEnumToNumber( Weekdays weekdays )
    {
        switch( weekdays )
        {
            case WEEKLY_SUNDAY:
                return 1;
            case WEEKLY_MONDAY:
                return 2;
            case WEEKLY_TUESDAY:
                return 3;
            case WEEKLY_WEDNESDAY:
                return 4;
            case WEEKLY_THURSDAY:
                return 5;
            case WEEKLY_FRIDAY:
                return 6;
            case WEEKLY_SATURDAY:
                return 7;
            default:
                return 0;
        }
    }

    /**
     * @brief Converts a weekday value to Weekdays value
     *
     * @param weekday - int type.  The weekday value which where 1 is sunday and 7 is saturday
     * @return Weekdays - The Weekdays value for a given weekday.  (Only Weekdays::WEEKLY_SUNDAY to Weekdays::WEEKLY_SATURDAY are valid)
     */
    static Weekdays weekdayNumberToEnum( int weekday )
    {
        if( weekday < 1 || weekday > 7 )
        {
            // insert here your handling code for the invalid weekday,
            // for example, return WEEKLY_SUNDAY or throw an exception
            return WEEKLY_NONE;
        }
        // Shifts 1 to the left by (weekday - 1) positions and performs bitwise AND with WEEKLY_ALL
        // This extracts the corresponding bitmask value for the given weekday
        // int ret = Weekdays::WEEKLY_ALL & ( 1 << ( weekday - 1 ) );
        // return static_cast<Weekdays>(ret);

        return static_cast< Weekdays >( 1 << ( weekday - 1 ) );
    }



private:

};


/**
 * @brief Helper function to combine two Weekdays values
 *
 * @param first The first Weekdays value
 * @param second The second Weekdays value
 * @return The combined Weekdays value
 */
inline Weekdays combineTwoDays( Weekdays first, Weekdays second )
{
    return static_cast< Weekdays >( first | second );
}

/**
 * @brief End case for the recursive CombineWeekdays function
 *
 * @param single The single Weekdays value to be returned
 * @return The input Weekdays value
 */
inline Weekdays CombineWeekdays( Weekdays single )
{
    return single;
}

/**
 * @brief Combines any number of Weekdays values into a single Weekdays value.
 *
 * This is a variadic template function that accepts an arbitrary number of
 * parameters of type Weekdays and combines them using the bitwise OR operator.
 *
 * @tparam Arg First Weekdays type
 * @tparam Args The parameter pack representing any number of Weekdays values.
 * @param arg The actual first Weekdays value
 * @param args The actual remaining Weekdays values to be combined.
 * @return A single Weekdays value that is the result of combining all input Weekdays values.
 *
 * @example Usage examples:
 *      Weekdays workDays = CombineWeekdays(WEEKLY_MONDAY, WEEKLY_TUESDAY, WEEKLY_WEDNESDAY, WEEKLY_THURSDAY, WEEKLY_FRIDAY);
 *      Weekdays weekendDays = CombineWeekdays(WEEKLY_SATURDAY, WEEKLY_SUNDAY);
 */
template< typename Arg, typename ... Args >
inline Weekdays CombineWeekdays( Arg arg, Args... args )
{
    return combineTwoDays( arg, CombineWeekdays( args ... ) );
}

#endif