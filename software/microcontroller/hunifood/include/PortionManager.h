#ifndef PORTIONER_H
#define PORTIONER_H

#pragma once

#include "AugerController.h"
#include "DispensingController.h"
#include <Weigher.h>


/*
    Esp32 : http://parts.guttih.com/parts/view/5bfa77a3a66f3c7ee749efe3
    Possible free pins on ESP32:
         ADC1 (8 channels, attached to GPIOs 32 - 39)
        -Free of those are: G32, G33,
           - ADC1 input only: G35, G36, G39
        - With least functionality:
            G16,G22
        - Free but need to gather information if they are wifi save
        - Not wifi save
            G0, G12, G26, G27

    In Water mixer project
        These pins are in use
        Display─╢-G2
        Display─╢-G4
        Display─╢-G5
            DAC─╢-G13
            DAC─╢-G14
        Display─╢-G15
        Display─╢-G18
        Display─╢-G19
        Display─╢-G21
        Display─╢-G23
    Temperature ─╢-G25
        Pressure─╢-G34

    Summary, free output pins are then
        G16, _G22, _G32, _G33
        G17,  G26,  G27

For reading: https://randomnerdtutorials.com/esp32-load-cell-hx711/

*/


/**
 * @brief Handles the logic of moving, portioning and
 *        dispensing one portion of food into the bowl.
 *
 * The auger is used to move the food from the storage container to the portion container.
 * The weigher is used to measure the amount of food that should be moved to the portion container.
 * The dispenser is used to open and close the door of the portion container, to get the food into the bowl.
 *
 */
class PortionManager
{
public:
    ~PortionManager();
    PortionManager( AugerController *auger, DispensingController *dispenser, Weigher *storageWeigher, Weigher *portionWeigher );
    float readPortionWeightInGrams( bool waitForScaleToBeReady, uint8_t sampleCount = 1 );
    float readStorageWeightInGrams( bool waitForScaleToBeReady, uint8_t sampleCount = 1 );
    void enableStepper( bool enable );
    void rotateStepper( long deg );
    void begin();
    void openDispensingDoor();
    void closeDispensingDoor();
    void setServoAngle( uint16_t angle );
    void enableServo( bool enable );



private:
    // A4988 *_stepper;
    AugerController *_foodTransporter;
    DispensingController *_foodDispenser;
    Weigher *_portionScale;
    Weigher *_storageScale;
    short _scalePinData = 0;
    short _scalePinClock = 0;
    float _scaleFactor = 0;

};

#endif