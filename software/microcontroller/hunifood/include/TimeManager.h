#ifndef TIME_MANAGER_H
#define TIME_MANAGER_H

#include <Wire.h>
#include <TimeLib.h>
#include <queue>
#include "TimeAction.h"

#ifndef byte
typedef unsigned char byte; // Define byte as an alias to unsigned char
#endif


class TimeManager
{
public:
    TimeManager();
    ~TimeManager();
    /**
    * \brief Initializes the RTC.
    *
    * \note Call `begin()` on the TwoWire object before using this function.
    *
    * \param wire Pointer to TwoWire object for I2C communication.
    * \param address I2C address of the RTC. Default is 0x68.
    */
    void initRtc( TwoWire* wire, byte address = 0x68 );

    /**
     * @brief Checks if the RTC is connected.
     *
     * @return true
     * @return false
     */
    bool isDeviceConnected();

    /**
     * @brief Creates a tmElements_t object from the given time values.
     *
     * @param year The year.
     * @param month The month.
     * @param dayOfMonth The day of the month.
     * @param hour The hour.
     * @param minute The minute.
     * @param second The second.
     * @param dayOfWeek The day of the week.
     * @return tmElements_t The tmElements_t object.
     */
    tmElements_t makeTimeElements( int year, byte month, byte dayOfMonth, byte hour, byte minute, byte second );
    uint64_t getFutureMillis( const tmElements_t tm );

    /**
     * @brief Sets the RTC time.
     *
     * @param tm The time to set.
     * @return true if the time was set successfully.
     * @return false if the time was not set successfully.
     *
     */
    bool setRtcTime( tmElements_t tm );

    /**
     * @brief Gets the current time from the RTC.
     *
     * @return tmElements_t the RTC time.
     *
     * @throw std::runtime_error if the RTC clock is halted.
     * @throw std::runtime_error if there is insufficient data from the RTC.
     * @throw std::runtime_error if the RTC is not connected.
     *
     */
    tmElements_t getRtcTime();

    /**
     * @brief Save the current time to memory this value can be retrieved with `now()` function.
     *
     * @param t The time to save.
     */
    void setCurrentTime( time_t t );
    tmElements_t toTmElement( time_t t );
    time_t toTimeT( tmElements_t tm );

    /**
     * @brief Gets the current time in milliseconds.
     *
     * @return uint64_t The current time in milliseconds.
     */
    uint64_t nowMillis();

    bool syncWithRtc();

    /**
     * @brief Checks if the RTC is connected and synchronizes the time if it is.
     *
     * This function checks if the RTC is connected and synchronizes the time with the RTC if it is connected.
     * The synchronization interval can be specified in milliseconds (default is 60000 milliseconds or 1 minute).
     *
     * @param intervalMillis The interval in milliseconds between synchronizations.
     */
    void checkResyncWithRtc( uint64_t intervalMillis = 60000 );

    String formatTimeString( time_t t );
    String formatTimeString( const tmElements_t tm );
    String nowString();

    /**
     * @brief Gets the time in milliseconds when the next action is due.
     *
     * @return uint64_t The time in milliseconds when the next action is due.
     *
     * @throw std::runtime_error if there are no actions in the queue.
     */
    uint64_t getNextActionTriggerMillis() const;
    uint64_t getLastActionTriggerMillis() const;
    int getActionCount() const;

    /**
     * @brief Adds the given time action to the queue of scheduled actions.
     *
     * @param action The action to add.
     * @param tm The time when the action will be executed.
     *
     */
    void addTimeAction( tmElements_t tm, action_t action );
    void addTimeAction( uint64_t triggerMillis, action_t action );
    void addTimeAction( uint64_t triggerMillis, action_with_String_ulong action, String description, unsigned long paramUlong );
    void addTimeAction( uint64_t triggerMillis, action_with_one_int action, int param1 );
    void addTimeAction( uint64_t triggerMillis, action_with_two_ints action, int param1, int param2 );
    void addTimeAction( uint64_t triggerMillis, action_with_one_float action, float param1 );

    bool handleTimeActions();
    tmElements_t millisToTimeElements( uint64_t currentMillis );

private:
    TwoWire* _wire = nullptr;
    byte _address = 0x68;
    uint64_t _lastSyncMillis;

    // Convert Binary Coded Decimal (BCD) to Decimal
    uint8_t bcd2dec( uint8_t num );

    // Convert Decimal to Binary Coded Decimal (BCD)
    uint8_t dec2bcd( uint8_t num );

    void pushToQueue( TimeAction* newAction );
    void updateLastTriggerAction( TimeAction* newAction );
    TimeAction* getLastTriggerAction() const;
    struct CompareActionTime {
        bool operator()( const TimeAction* t1, const TimeAction* t2 ) // Note the change of argument type here.
        {
            // Make sure to access the objects via pointer dereference
            return t1->getTriggerMillis() > t2->getTriggerMillis();
        }
    };

    std::priority_queue< TimeAction*, std::vector< TimeAction* >, CompareActionTime > _actionQueue;
    TimeAction* _lastTriggerAction = nullptr;

};

#endif