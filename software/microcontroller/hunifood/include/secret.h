#ifndef SECRET_H
#define SECRET_H


// How to add this file to git but tell git to always ignore changes to it
// git update-index --assume-unchanged include/secret.h
#define WIFI_SSID "your_SSID"
#define WIFI_PASSWORD "your_PASSWORD"

#endif