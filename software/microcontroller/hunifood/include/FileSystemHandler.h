#ifndef FILESYSTEMHANDLER_H
#define FILESYSTEMHANDLER_H

#include <ESPAsyncWebServer.h>
#include <SD.h>

class FileSystemHandler
{
public:
    FileSystemHandler();
    ~FileSystemHandler();

    void onUpload( AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final );
    void serveFileFromSD( AsyncWebServerRequest *request, const char* filename, const char* contentType, int code = 200 );
    void serveAnyFileFromSD( AsyncWebServerRequest *request, const char* filename, const char* );
    void onIndexRequest( AsyncWebServerRequest *request );
    void onDirectoryStructureRequest( AsyncWebServerRequest *request );
    String buildDirectoryStructure( File dir, int numTabs, bool includeCardSize, bool includeFiles = true );

    void notFound( AsyncWebServerRequest *request );

    void onGetFile( AsyncWebServerRequest *request );
    void onCreateOrUpdateFile( AsyncWebServerRequest *request );
    void onDeleteFile( AsyncWebServerRequest *request );

private:
    File uploadFile;
};

#endif