#ifndef WEB_SERVER_HANDLER_H
#define WEB_SERVER_HANDLER_H


#include <ESPAsyncWebServer.h>

class WebServerHandler
{
public:
    WebServerHandler();
    void begin();
private:
    AsyncWebServer server;
};

#endif