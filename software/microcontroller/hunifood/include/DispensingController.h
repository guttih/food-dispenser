#ifndef DISPENSER_H
#define DISPENSER_H

#include <Servo.h>

/**
 * @brief Controls the dispenser servo motor which opens and closes
 * the door of the portion container allowing food to fall into the bowl.
 *
 */

class DispensingController
{
public:
    DispensingController( short pin, short channel );
    void enable( bool enable );
    void setServoAngle( uint16_t angle );
    void openDispensingDoor();
    void closeDispensingDoor();
    ~DispensingController();

private:
    Servo *_servo;
};

#endif