#ifndef CONSTANTS_H
#define CONSTANTS_H

#define BRIGHTNESS_CHANNEL 1
#define SERVO_CHANNEL 2

#endif //CONSTANTS_H


/*

        Connection of all the components to the ESP32

 Display  Display  Display         Stepper          two
|Display|    SD   | Touch | Esp32 | A4988  | Servo | HX711 | TinyRTC |MCP23017 |
|------:|:-------:|:------|------:|:------:|:-----:|:-----:|:-------:|:-------:|
| VCC   |         |       |  3.3V |        |       |       |   VCC   | VCC/RST |
| GND   |         |       |   GND |        |       |       |   GND   |   GND   |
| GND   |         |       |   GND |        |       |       |         |A0/A1/A2 |
| CS    |         |       |    15 |        |       |       |         |         |
| LED   |         |       |    17 |        |       |       |         |         |
| RESET |         |       |     4 |        |       |       |         |         |
| DC/RS |         |       |     2 |        |       |       |         |         |
| MOSI  | SD_MOSI | T_DIN |    23 |        |       |       |         |         |
| SCK   | SD_SCK  | T_CLK |    18 |        |       |       |         |         |
|       | SD_MISO | T_DO  |    19 |        |       |       |         |         |
|       | SD_CS   |       |     5 |        |       |       |         |         |
|       |         | T_CS  |    21 |        |       |       |         |         |
|       |         |       |    16 |        | Orange|       |         |         |
|       |         |       |    22 | DIRECT |       |       |         |         |
|       |         |       |    32 |  STEP  |       |       |         |         |
|       |         |       |    33 | ENABLE |       |       |         |         |
|       |         |       |    35 |        |       |  DT   |         |         |
|       |         |       |    14 |        |       |  SCK  |         |         |
|       |         |       |    34 |        |       |  DT   |         |         |
|       |         |       |    13 |        |       |  SCK  |         |         |
|       |         |       |    25 |        |       |       |   SDA   |   SDA   |
|       |         |       |    26 |        |       |       |   SCL   |   SCL   |
|       |         |       |       |RESET─┐ |       |       |         |         |
|       |         |       |       |SLEEP─┘ |       |       |         |         |

################################################################################
#                   Schematic For ESP32, motors, and display                   #
################################################################################

        ╔════════╗          ╔═══════════╗              ╔═════════════╗
        ║  Load  ║          ║  -Servo-  ║              ║ DC_STEPDOWN ║
        ║  Cell  ║          ║           ║              ║             ║
        ║        ║          ║       Red-╟──────────────╢-  +Vo(+6V)  ║
┌───────╢-Green  ║          ║     Brown-╟────┬─────────╢-  -Vo(GND)  ║
│ ┌─────╢-White  ║        ┌─╢-Orange    ║    ┴   ┌─────╢-GND         ║
│ │ ┌───╢-Black  ║        │ ╚═══════════╝   GND  │  ┌──╢-VIN         ║
│ │ │ ┌─╢-Red    ║        │ ╔═══════════╗        │  │  ╚═════════════╝
│ │ │ │ ╚════════╝        │ ║  -ESP3-   ║        │  │  ╔══════════════╗
│ │ │ │ ╔═════════════╗   │ ║           ║        │  │  ║ POWER SOURCE ║
│ │ │ │ ║    HX711    ║   └─╢-G16       ║        │  │  ║              ║
│ │ │ └─╢ E+          ║     ║           ║        │  │  ║              ║
│ │ └───╢ E-       DT-╟─────╢-G35       ║        │  └──╢-    +12V    -╟────────┐
│ └─────╢-A-      SCK-╟─────╢-G14       ║    ┌───┴─────╢-    -GND    -╟─────┐  │
└───────╢-A+          ║     ║           ║    ┴         ╚══════════════╝     │  │
        ║             ║  DT ╢-G34       ║   GND                             │  │
  ┌─────╢- +Vo(+3V3)  ║  SCK╢-13        ║            ╔═══════════╗          │  │
  │   ┌─╢- -Vo( GND)  ║     ║           ║            ║ -Stepper- ║          │  │
  │   │ ╚═════════════╝     ║           ║            ║       Red-╟───────┐  │  │
  │   └────────┬────────────╢-GND       ║            ║     Black-╟─────┐ │  │  │
  │              ┴          ║       G25-╟─See below  ║      Blue-╟───┐ │ │  │  │
  │             GND         ║       G26-╟─See below  ║     Green-╟─┐ │ │ │  │  │
  ├─────────────────────────╢-3V3       ║            ╚═══════════╝ │ │ │ │  │  │
  │  ╔══════════════╗       ║           ║         ╔══════════════╗ │ │ │ │  │  │
  │  ║   LCD TOUCH  ║       ║           ║         ║    -A4988-   ║ │ │ │ │  │  │
  │  ║    DISPLAY   ║       ║  -ESP32-  ║         ╢-MS1       2B-╟─┘ │ │ │  │  │
  │  ║              ║       ║           ║         ╢-MS2       2A-╟───┘ │ │  │  │
  │  ║              ║       ║           ║         ╢-MS3       1A-╟─────┘ │  │  │
  │  ║      SD_MOSI-╟─┐     ║       G33-╟─────────╢-Enable    1B-╟───────┘  │  │
  │  ║         MOSI-╟─┼───┐ ║       G22-╟─────────╢-Direction    ║          │  │
  │  ║        T_DIN-╟─┘   │ ║       G32-╟─────────╢-Step         ║          │  │
  │  ║       SD_SCK-╟─┐   │ ║           ║       ┌─╢-Reset        ║          │  │
  │  ║          SCK-╟─┼─┐ │ ║           ║       └─╢-Sleep        ║          │  │
  │  ║        T_CLK-╟─┘ │ └─╢-G23       ║  ┌──────╢-GND      GND-╟──────────┘  │
  │  ║         T_DO-╟─┐ └───╢-G18       ║  │    ┌─╢-VDD     VMOT-╟─────────────┤
  │  ║      SD_MISO-╟─┴─────╢-G19       ║  │    │ ╚══════════════╝             │
  │  ║           CS-╟───────╢-G15       ║  │    └─────────────────────────┐    │
  │  ║        RESET-╟───────╢-G4        ║  ├─────┬────────────────────┐   │    │
  │  ║           DC-╟───────╢-G2        ║  │     ┴   ╔═════════════╗  │   │    │
  │  ║        SD_CS-╟───────╢-G5    GND-╟──┘    GND  ║ DC_STEPDOWN ║  │   │    │
  │  ║         T_CS-╟───────╢-G21   G17-╟─┐          ║             ║  │   │    │
  │  ║              ║       ╚═══════════╝ │    ┌─────╢- -Vo( GND) -╟──┘   │    │
  │  ║              ║       ╔═══════════╗ │    │  ┌──╢- +Vo(+3V3) -╟──────┘    │
  │  ║          LED-╟───────╢  Resistor ╟─┘    │  │  ║             ║  ┌───┐    │
  │  ║          GND-╟────┐  ║-  220Ω   -║      │  │  ║             ║  │   ┴    │
  │  ║          VCC-╟─┐  │  ╚═══════════╝      │  │  ║         GND-╟──┘  GND   │
  │  ╚══════════════╝ │  └─────────────────────┘  │  ║         VIN-╟───────────┘
  └───────────────────┴───────────────────────────┘  ╚═════════════╝



################################################################################
#              Schematic for the ESP32, the RTC, and the MCP23017              #
################################################################################

  ╔══════════════╗     ╔═════════════╗
  ║ POWER SOURCE ║     ║ DC_STEPDOWN ║
  ║              ║     ║             ║
  ╢-    +12V    -╟─────╢-VIN         ║                ┌────────────────────────┐
  ╢-    -GND    -╟──┬──╢-GND         ║    ┌─────┐     │     ╔════════════╗     │
  ╚══════════════╝  ┴  ║             ║    │     ┴     │     ║ -MCP23017- ║     │
                   GND ║             ║    │    GND    │     ║            ║     │
 ┌─────────────────────║- -Vo( GND) -╟────┘           │     ╢-GPB0  GPA7-╟     │
 │                 ┌───╢- +Vo(+3V3) -╟────────────────┘     ╢-GPB1  GPA6-╟     │
 │                 │   ╚═════════════╝                      ╢-GPB2  GPA5-╟     │
 │  ╔═══════════╗  │                                        ╢-GPB3  GPA4-╟     │
 │  ║  -ESP32-  ║  │                        ╔════════════╗  ╢-GPB4  GPA3-╟     │
 │  ║           ║  │                        ║ -TINY RTC- ║  ╢-GPB5  GPA2-╟     │
 └──╢-GND   3V3-╟──┴────────────────────┐   ║            ║  ╢-GPB6  GPA1-╟     │
    ║           ║      ╔═══════════╗    │   ╢-SQ         ║  ╢-GPB7  GPA0-╟     │
    ║           ║      ║  Resistor ║    ├───╢-VDD    VDD-╟──╢-VDD   INTA-╟     │
    ║           ║  ┌───╢-  4.7KΩ  -╟────┘   ║        GND-╟──╢-VSS   INTB-╟     │
    ║           ║  │   ╚═══════════╝        ╢-DS      DS-╟  ║-NC   RESET-╟─────┤
    ║       G26-╟──┴────────────────────────╢-SCL    SCL-╟──╢-SCL     A2-╟──┐  │
    ║       G25-╟──┬────────────────────────╢-SDA    SDA-╟──╢-SDA     A1-╟──┤  │
 ┌──╢-GND       ║  │                  ┌───┬─╢-GND    BAT-╟  ║-NC      A0-╟──┤  │
 ┴  ║           ║  │  ╔═══════════╗   ┴   │ ╚════════════╝  ╚════════════╝  │  │
GND ║           ║  │  ║  Resistor ║  GND  └─────────────────────────────────┘  │
    ╚═══════════╝  └──║-  4.7KΩ  -╟────────────────────────────────────────────┘
                      ╚═══════════╝

 */

