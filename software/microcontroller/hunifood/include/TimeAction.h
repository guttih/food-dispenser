#ifndef TIMEACTION_H
#define TIMEACTION_H

#include <Arduino.h>
#include <TimeLib.h>


// Function pointer types
typedef void (*action_t)( void );
typedef void (*action_with_String_ulong)( String, unsigned long );
typedef void (*action_with_one_int)( int );
typedef void (*action_with_two_ints)( int, int );
typedef void (*action_with_one_float)( float );

class TimeManager;  // Forward declaration

class TimeAction
{
public:
    // Base constructor
    TimeAction( uint64_t millis, action_t action ) : _triggerMillis( millis ), _action_basic( action ), _actionType( 0 )
    {
    }

    // Constructor that accepts action with String and unsigned long
    TimeAction( uint64_t millis, action_with_String_ulong action, String stringOne, unsigned long ulongOne ) : _triggerMillis( millis ),
        _action_String_ulong( action ), _actionType( 4 ), _paramStringOne( stringOne ), _paramULongOne( ulongOne )
    {
    }

    // Constructor that accepts action with one integer
    TimeAction( uint64_t millis, action_with_one_int action, int paramOne ) : _triggerMillis( millis ),
        _action_one_int( action ), _actionType( 1 ), _paramOne( paramOne )
    {
    }

    // Constructor that accepts action with two integers
    TimeAction( uint64_t millis, action_with_two_ints action, int paramOne, int paramTwo ) : _triggerMillis( millis ),
        _action_two_ints( action ), _actionType( 2 ), _paramOne( paramOne ), _paramTwo( paramTwo )
    {
    }

    TimeAction( uint64_t millis, action_with_one_float action, float param1 ) : _triggerMillis( millis ),
        _action_one_float( action ), _actionType( 3 ), _paramOne( param1 )
    {
    }

    bool isValid();
    uint64_t getTriggerMillis() const;

    // An update to executeAction to account for the parameters
    void executeAction() const;

private:
    int _actionType; // 0 - action_t, 1 - action_with_one_int, 2 - action_with_two_ints
    action_t _action_basic;
    action_with_one_int _action_one_int;
    action_with_two_ints _action_two_ints;
    action_with_one_float _action_one_float;
    action_with_String_ulong _action_String_ulong;

    // Params to be passed to the function pointers
    int _paramOne;
    int _paramTwo;
    unsigned long _paramULongOne;
    String _paramStringOne;


    tmElements_t _time;
    uint64_t _triggerMillis;
};

#endif  // TIMEACTION_H