#ifndef AUGERCONTROLLER_H
#define AUGERCONTROLLER_H

#include <A4988.h>


/**
 * @brief Turns the auger motor to move food from
 * the storage container to the portion container.
 *
 */
class AugerController
{
public:
    AugerController( short motorPinDirection, short motorPinStep, short motorPinEnable );
    void rotateStepper( long deg );
    void enableStepper( bool enable );
    ~AugerController();

private:
    A4988 *_stepper;
};

#endif