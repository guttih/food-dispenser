#ifndef DISPLAY_H
#define DISPLAY_H

#include <FS.h>
#include "Free_Fonts.h" // Include the header file attached to this sketch

#include <TFT_eSPI.h>              // Hardware-specific library
#include <TFT_eWidget.h>           // Widget library
#include <SD.h>

#include "constants.h"

TFT_eSPI tft = TFT_eSPI();         // Invoke custom library
TFT_eSprite knob = TFT_eSprite( &tft ); // Sprite for the slide knob


#define CALIBRATION_FILE "/TouchCalData1"
#define REPEAT_CAL true

void touch_calibrate()
{
    uint16_t calData[ 5 ];
    uint8_t calDataOK = 0;

    // check file system exists
    if( !LittleFS.begin() )
    {
        Serial.println( "Formatting file system" );
        LittleFS.format();
        LittleFS.begin();
    }

    // check if calibration file exists and size is correct
    if( LittleFS.exists( CALIBRATION_FILE ) )
    {
        if( REPEAT_CAL )
        {
            // Delete if we want to re-calibrate
            LittleFS.remove( CALIBRATION_FILE );
        }
        else
        {
            File f = LittleFS.open( CALIBRATION_FILE, "r" );
            if( f )
            {
                if( f.readBytes( ( char * ) calData, 14 ) == 14 )
                    calDataOK = 1;
                f.close();
            }
        }
    }

    // set color scheme

    if( calDataOK && !REPEAT_CAL )
    {
        // calibration data valid
        tft.setTouch( calData );
    }
    else
    {


        // data not valid so recalibrate
        tft.fillScreen( TFT_BLACK );
        tft.setCursor( 20, 0 );
        tft.setTextFont( 2 );
        tft.setTextSize( 1 );
        tft.setTextColor( TFT_WHITE, TFT_BLACK );

        tft.println( "Touch corners as indicated" );

        tft.setTextFont( 1 );
        tft.println();

        if( REPEAT_CAL )
        {
            tft.setTextColor( TFT_RED, TFT_BLACK );
            tft.println( "Set REPEAT_CAL to false to stop this running again!" );
        }

        tft.calibrateTouch( calData, TFT_MAGENTA, TFT_BLACK, 15 );

        tft.setTextColor( TFT_GREEN, TFT_BLACK );
        tft.println( "Calibration complete!" );

        // store data
        File f = LittleFS.open( CALIBRATION_FILE, "w" );
        if( f )
        {
            f.write( ( const unsigned char * ) calData, 14 );
            f.close();
        }
    }
}

void setBrightnessPin( uint8_t pin, uint8_t channel )
{
    ledcSetup( channel, 5000, 8 );
    ledcAttachPin( pin, channel );
}


void setBrightness( uint8_t brightness, uint8_t channel )
{
    uint8_t outPWM = map( brightness, 0, 100, 0, 255 );
    ledcWrite( channel, outPWM );
    Serial.printf( "Brightness: %d\n", outPWM );
}

void drawCentered( String text, int y )
{
    int screenWidth = tft.width();         // Get the screen width
    int textWidth = tft.textWidth( text ); // Get the text string width
    int xStartPosition = ( screenWidth - textWidth ) / 2; // Calculate the x position

    tft.drawString( text, xStartPosition, y ); // Draw the string at calculated (x, y)
}

void displaySetup()
{

    String msg;

    delay( 500 );
    tft.begin();
    tft.invertDisplay( false );
    tft.setRotation( 3 );
    tft.setSwapBytes( true );
    tft.fillScreen( TFT_BLACK );
    tft.setFreeFont( FF18 );
    setBrightnessPin( LED_PIN, BRIGHTNESS_CHANNEL );
    setBrightness( 30, BRIGHTNESS_CHANNEL );


    if( !SD.begin( SD_CS ) )
    {
        msg = "Unable to open sd card";
        tft.drawString( msg,  80, 100 );
    }
    else
    {
        float sizeInGB = static_cast< double >( SD.cardSize() ) / ( 1024 * 1024 * 1024 );
        float freeSizeInGB = static_cast< double >( SD.totalBytes() ) / ( 1024 * 1024 * 1024 );
        drawCentered( "SD Card size", 100 );
        tft.setFreeFont( FF17 );
        msg = "Free " + String( freeSizeInGB ) + " GB  " + String( sizeInGB ) + " GB  ";
        drawCentered( msg, 120 );


        Serial.println( "Calibrating touch" );
        delay( 3000 );


    }
    // touch_calibrate();
    Serial.println( msg );

    delay( 1000 );
}


#endif