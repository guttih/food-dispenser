#include "unity.h"
#include "./constants.h"              // Constants used in tests
#include <Event.h> // Your EventType helper class


void test_Event_constructor( void )
{
    String description = "Test Event";
    Event testEvent( description );

    TEST_ASSERT_EQUAL_STRING( description.c_str(), testEvent.getDescription().c_str() );
}

void test_Event_set_description( void )
{
    String description = "Test Event";
    Event testEvent( "Initial Description" );
    testEvent.setDescription( description );

    TEST_ASSERT_EQUAL_STRING( description.c_str(), testEvent.getDescription().c_str() );
}

void test_Event_type_should_be_basic( void )
{
    String description = "Test Event";
    Event testEvent( description );

    TEST_ASSERT_EQUAL_INT( EventType::BASIC, testEvent.getType() );
}

void test_Event_equality_operator( void )
{
    String description = "Test Event";
    Event event1( description );
    Event event2( description );

    TEST_ASSERT_TRUE( event1 == event2 );
}

void test_Event_inequality_operator( void )
{
    String description1 = "Test Event 1";
    Event event1( description1 );

    String description2 = "Test Event 2";
    Event event2( description2 );

    TEST_ASSERT_TRUE( event1 != event2 );
    TEST_ASSERT_FALSE( event1 == event2 );

    event2.setDescription( event1.getDescription() );
    TEST_ASSERT_TRUE( event1 == event2 );
}

void test_Event_serialization_deserialization( void )
{
    String description = "Test Event";
    Event event1( description );
    String serialized = event1.serialize();

    Event event2( "Some other event" );
    event2.deserialize( serialized );

    TEST_ASSERT_TRUE( event1 == event2 );
}

