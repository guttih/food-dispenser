#include "unity.h"
#include "constants.h"
#include <TimeLib.h>


void test_TimeHelperFormatTimeString( void )
{
    const time_t t = 1634001939; // 61112 12th Oct 2021
    TEST_ASSERT_EQUAL_STRING( "2021-10-12 01:25:39", TimeHelper::formatTimeString( t ).c_str() );
}



void test_TimeHelper_formatTimeString( void )
{
    time_t testTime = makeTime( TimeHelper::createTmElement( 2022, 1, 1, 12, 0, 0 ) );
    TEST_ASSERT_EQUAL_STRING( "2022-01-01 12:00:00", TimeHelper::formatTimeString( testTime ).c_str() );
}

void test_TimeHelper_createTmElement( void )
{
    tmElements_t testObj = TimeHelper::createTmElement( 1970, 1, 1, 0, 0, 0 );
    TEST_ASSERT_EQUAL_INT( 0, testObj.Year );
    TEST_ASSERT_EQUAL_INT( 1, testObj.Month );
    TEST_ASSERT_EQUAL_INT( 1, testObj.Day );
    TEST_ASSERT_EQUAL_INT( 0, testObj.Hour );
    TEST_ASSERT_EQUAL_INT( 0, testObj.Minute );
    TEST_ASSERT_EQUAL_INT( 0, testObj.Second );
}

void test_TimeHelper_createTmElementLeapYear( void )
{
    tmElements_t testObj = TimeHelper::createTmElement( 2000, 2, 29, 23, 59, 59 ); // Leap year 2000 and the last moment of February 29
    TEST_ASSERT_EQUAL_INT( CalendarYrToTm( 2000 ), testObj.Year );
    TEST_ASSERT_EQUAL_INT( 2, testObj.Month );
    TEST_ASSERT_EQUAL_INT( 29, testObj.Day );
    TEST_ASSERT_EQUAL_INT( 23, testObj.Hour );
    TEST_ASSERT_EQUAL_INT( 59, testObj.Minute );
    TEST_ASSERT_EQUAL_INT( 59, testObj.Second );
}

void test_TimeHelper_joinExtractedDateAndTime( void )
{
    tmElements_t tmSet = TimeHelper::createTmElement( 1970, 1, 1, 1, 0, 0 );
    time_t expectedTime = makeTime( tmSet );

    // datePart for 1970-01-01, timePart for 01:00:00
    long datePart = 19700101;
    long timePart = 3600L;

    // Joining the inputs and creating actualTime
    time_t actualTime = TimeHelper::joinExtractedDateAndTime( datePart, timePart );

    // Checking that the expected output is equal to the function output
    TEST_ASSERT_EQUAL_INT32( expectedTime, actualTime );
}

void test_TimeHelper_joinExtractedDateAndTimeLeapYear( void )
{
    tmElements_t tmSet = TimeHelper::createTmElement( 2000, 2, 29, 23, 0, 0 );
    time_t expectedTime = makeTime( tmSet );

    // datePart for 2000-02-29, timePart for 23:00:00
    long datePart = 20000229;
    long timePart = 82800L; //23*60*60

    // Joining the inputs and creating actualTime
    time_t actualTime = TimeHelper::joinExtractedDateAndTime( datePart, timePart );

    // Checking that the expected output is equal to the function output
    TEST_ASSERT_EQUAL_INT32( expectedTime, actualTime );
}

void test_TimeHelper_extractTime( void )
{
    time_t testTime = makeTime( TimeHelper::createTmElement( 2022, 1, 1, 12, 0, 0 ) );
    TEST_ASSERT_EQUAL_INT32( 43200L, TimeHelper::extractTime( testTime ) );
}

void test_TimeHelper_extractDate( void )
{
    time_t testTime = makeTime( TimeHelper::createTmElement( 2022, 1, 1, 12, 0, 0 ) );
    TEST_ASSERT_EQUAL_INT32( 20220101L, TimeHelper::extractDate( testTime ) );
}

void test_TimeHelper_calculateNextTriggerTime( void )
{
    String errorMessage = "Test case 1: startTime and stopTime are equal";
    time_t startTime = 1623000000, stopTime = startTime, currentTime = 1623001000, expected = startTime;
    TEST_ASSERT_EQUAL_INT_MESSAGE( expected, TimeHelper::calculateNextTriggerTime( startTime, stopTime, currentTime ),
                                   formatTriggerValues( startTime, stopTime, currentTime,
                                                        errorMessage ).c_str() );

    errorMessage="Test case 2: stopTime is less than currentTime";
    startTime = 1623000000;    stopTime = 1623000900;    currentTime = 1623001000;    expected = 1623000000;
    TEST_ASSERT_EQUAL_INT_MESSAGE( expected, TimeHelper::calculateNextTriggerTime( startTime, stopTime, currentTime ),
                                   formatTriggerValues( startTime, stopTime, currentTime,
                                                        errorMessage ).c_str() );

    errorMessage="Test case 3: timePartOfCurrentTime is greater than timePartOfStartTime";
    startTime = 1623000000;    stopTime = 1623001000;    currentTime = 1623000900;    expected = 1623086400;
    TEST_ASSERT_EQUAL_INT_MESSAGE( expected, TimeHelper::calculateNextTriggerTime( startTime, stopTime, currentTime ),
                                   formatTriggerValues( startTime, stopTime, currentTime,
                                                        errorMessage ).c_str() );

    errorMessage="Test case 4: timePart of startTime < current. ";
    startTime = 1623000000;    stopTime = MAX_LONG;    currentTime = 1623000500;    expected = 1623086400;
    TEST_ASSERT_EQUAL_INT_MESSAGE( expected, TimeHelper::calculateNextTriggerTime( startTime, stopTime, currentTime ),
                                   formatTriggerValues( startTime, stopTime, currentTime,
                                                        errorMessage ).c_str() );

    errorMessage="Test case 5: tomorrow ==: timePart of startTime = current. ";
    startTime = 1623000000;    stopTime = MAX_LONG;    currentTime = startTime + SECS_PER_DAY;    expected = currentTime;
    TEST_ASSERT_EQUAL_INT_MESSAGE( expected, TimeHelper::calculateNextTriggerTime( startTime, stopTime, currentTime ),
                                   formatTriggerValues( startTime, stopTime, currentTime,
                                                        errorMessage ).c_str() );

    errorMessage="Test case 6: tomorrow <: timePart of startTime < current. ";
    startTime = 1623000000;    stopTime = MAX_LONG;    currentTime = startTime + SECS_PER_DAY + 1;    expected = startTime + ( SECS_PER_DAY * 2 );
    TEST_ASSERT_EQUAL_INT_MESSAGE( expected, TimeHelper::calculateNextTriggerTime( startTime, stopTime, currentTime ),
                                   formatTriggerValues( startTime, stopTime, currentTime,
                                                        errorMessage ).c_str() );

    errorMessage="Test case 7: tomorrow >: timePart of startTime > current. ";
    startTime = 1623000000;    stopTime = MAX_LONG;    currentTime = startTime + SECS_PER_DAY - 1;    expected = startTime + SECS_PER_DAY;
    TEST_ASSERT_EQUAL_INT_MESSAGE( expected, TimeHelper::calculateNextTriggerTime( startTime, stopTime, currentTime ),
                                   formatTriggerValues( startTime, stopTime, currentTime,
                                                        errorMessage ).c_str() );



    errorMessage="Test case 8 General currentTime>startTime & time part of start time less than time part of stop:";
    startTime = 1623000000;    stopTime = 1623001000;    currentTime = 1623000500;    expected = 1623086400;
    TEST_ASSERT_EQUAL_INT_MESSAGE( expected, TimeHelper::calculateNextTriggerTime( startTime, stopTime, currentTime ),
                                   formatTriggerValues( startTime, stopTime, currentTime,
                                                        errorMessage ).c_str() );
}

String formatDayCheck( time_t time, Weekdays weekdays, String errorMessage )
{
    int wday = weekday( time );
    return TimeHelper::formatTimeString( time ) + " " + weekdaysNames[ wday ] +
           ", wday: " + String( wday ) +
           " isTimeDayOfWeekInWeekdays( date, WEEKLY_ALL ):" + String( TimeHelper::isTimeDayOfWeekInWeekdays( time, weekdays ) );
}

// Test for isTimeDayOfWeekInWeekdays( time_t timeToCheck, Weekdays weekdays )
void test_isTimeDayOfWeekInWeekdays()
{
    time_t now = 946684800; // 01-01-2000 00:00:00. This date is a Saturday

    TEST_ASSERT_TRUE( TimeHelper::isTimeDayOfWeekInWeekdays( now, Weekdays::WEEKLY_SATURDAY ) );
    TEST_ASSERT_FALSE( TimeHelper::isTimeDayOfWeekInWeekdays( now, Weekdays::WEEKLY_MONDAY ) );
    TEST_ASSERT_TRUE( TimeHelper::isTimeDayOfWeekInWeekdays( now, Weekdays::WEEKLY_ALL ) );
    TEST_ASSERT_FALSE( TimeHelper::isTimeDayOfWeekInWeekdays( now, Weekdays::WEEKLY_WORK ) );
    TEST_ASSERT_TRUE( TimeHelper::isTimeDayOfWeekInWeekdays( now, Weekdays::WEEKLY_WEEKEND ) );
}



void test_isTimeDayOfWeekInWeekdays2()
{
    // Test for each day of the week
    // Sunday = 1, Monday = 2, Tuesday = 3, etc.

    int wday;
    bool result, shouldBe;
    String errorMessage;
    time_t timeStart = makeTime( TimeHelper::createTmElement( 2023, 12, 11, 0, 0, 0 ) );// 2023-12-11 is a Monday
    for( time_t time = timeStart; time < ( timeStart + ( SECS_PER_WEEK ) ); time+=SECS_PER_DAY )
    {
        wday = weekday( time );

        result = TimeHelper::isTimeDayOfWeekInWeekdays( time, Weekdays::WEEKLY_ALL );
        shouldBe = true;
        errorMessage = "Case All days:" + TimeHelper::formatTimeString( time ) + " (" + String( wday ) + "=" + weekdaysNames[ wday ] +
                       ") isTimeDayOfWeekInWeekdays( time, WEEKLY_ALL )";
        TEST_ASSERT_EQUAL_MESSAGE( shouldBe, result, errorMessage.c_str() );

        result = TimeHelper::isTimeDayOfWeekInWeekdays( time, Weekdays::WEEKLY_WEEKEND );
        shouldBe = ( wday == 1 || wday == 7 );
        errorMessage = "Case Weekend:" + TimeHelper::formatTimeString( time ) + " (" + String( wday ) + "=" + weekdaysNames[ wday ] +
                       ") isTimeDayOfWeekInWeekdays( time, WEEKLY_WEEKEND )";
        TEST_ASSERT_EQUAL_MESSAGE( shouldBe, result, errorMessage.c_str() );


        result = TimeHelper::isTimeDayOfWeekInWeekdays( time, Weekdays::WEEKLY_WORK );
        shouldBe = ( wday >= 2 && wday <= 6 );
        errorMessage = "Case Workday:" + TimeHelper::formatTimeString( time ) + " (" + String( wday ) + "=" + weekdaysNames[ wday ] +
                       ") isTimeDayOfWeekInWeekdays( time, WEEKLY_WORK )";
        TEST_ASSERT_EQUAL_MESSAGE( shouldBe, result, errorMessage.c_str() );


        shouldBe = true;
        switch( wday )
        {
            case 1:
                result = TimeHelper::isTimeDayOfWeekInWeekdays( time, Weekdays::WEEKLY_SUNDAY );

                break;
            case 2:
                result = TimeHelper::isTimeDayOfWeekInWeekdays( time, Weekdays::WEEKLY_MONDAY );
                break;
            case 3:
                result = TimeHelper::isTimeDayOfWeekInWeekdays( time, Weekdays::WEEKLY_TUESDAY );
                break;
            case 4:
                result = TimeHelper::isTimeDayOfWeekInWeekdays( time, Weekdays::WEEKLY_WEDNESDAY );
                break;
            case 5:
                result = TimeHelper::isTimeDayOfWeekInWeekdays( time, Weekdays::WEEKLY_THURSDAY );
                break;
            case 6:
                result = TimeHelper::isTimeDayOfWeekInWeekdays( time, Weekdays::WEEKLY_FRIDAY );
                break;

            case 7:
                result = TimeHelper::isTimeDayOfWeekInWeekdays( time, Weekdays::WEEKLY_SATURDAY );
                break;
            default:
                result = false;
        }

        errorMessage = "Case one workday:" + TimeHelper::formatTimeString( time ) + " (" + String( wday ) + "=" + weekdaysNames[ wday ] +
                       ") isTimeDayOfWeekInWeekdays( time, WEEKLY_WORK )";
        TEST_ASSERT_EQUAL_MESSAGE( shouldBe, result, errorMessage.c_str() );


    }

}

// Test for method formatTimeString( time_t t )
void test_formatTimeString()
{
    time_t now = 946684800;     // 01-01-2000 00:00:00
    String expectedResult = "2000-01-01 00:00:00";

    TEST_ASSERT_EQUAL_STRING( expectedResult.c_str(), TimeHelper::formatTimeString( now ).c_str() );
}



// Test for WeekdaysToString( Weekdays weekdays )
void test_WeekdaysToString()
{
    TEST_ASSERT_EQUAL_STRING( "Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday",
                              TimeHelper::WeekdaysToString( Weekdays::WEEKLY_ALL ).c_str() );
    TEST_ASSERT_EQUAL_STRING( "Sunday, Saturday", TimeHelper::WeekdaysToString( Weekdays::WEEKLY_WEEKEND ).c_str() );
}

void test_weekdayNumberToString()
{
    TEST_ASSERT_EQUAL_STRING( "Sunday", TimeHelper::weekdayNumberToString( 1 ).c_str() );
    TEST_ASSERT_EQUAL_STRING( "Saturday", TimeHelper::weekdayNumberToString( 7 ).c_str() );
}

void test_weekdayNumberToEnum()
{
    TEST_ASSERT_EQUAL( Weekdays::WEEKLY_SUNDAY, TimeHelper::weekdayNumberToEnum( 1 ) );
    TEST_ASSERT_EQUAL( Weekdays::WEEKLY_MONDAY, TimeHelper::weekdayNumberToEnum( 2 ) );
    TEST_ASSERT_EQUAL( Weekdays::WEEKLY_TUESDAY, TimeHelper::weekdayNumberToEnum( 3 ) );
    TEST_ASSERT_EQUAL( Weekdays::WEEKLY_WEDNESDAY, TimeHelper::weekdayNumberToEnum( 4 ) );
    TEST_ASSERT_EQUAL( Weekdays::WEEKLY_THURSDAY, TimeHelper::weekdayNumberToEnum( 5 ) );
    TEST_ASSERT_EQUAL( Weekdays::WEEKLY_FRIDAY, TimeHelper::weekdayNumberToEnum( 6 ) );
    TEST_ASSERT_EQUAL( Weekdays::WEEKLY_SATURDAY, TimeHelper::weekdayNumberToEnum( 7 ) );
    TEST_ASSERT_EQUAL( Weekdays::WEEKLY_NONE, TimeHelper::weekdayNumberToEnum( -1 ) );
    TEST_ASSERT_EQUAL( Weekdays::WEEKLY_NONE, TimeHelper::weekdayNumberToEnum( 0 ) );
    TEST_ASSERT_EQUAL( Weekdays::WEEKLY_NONE, TimeHelper::weekdayNumberToEnum( 8 ) );

}

void test_weekdaysEnumToNumber()
{
    TEST_ASSERT_EQUAL_INT( 1, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_SUNDAY ) );
    TEST_ASSERT_EQUAL_INT( 2, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_MONDAY ) );
    TEST_ASSERT_EQUAL_INT( 3, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_TUESDAY ) );
    TEST_ASSERT_EQUAL_INT( 4, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_WEDNESDAY ) );
    TEST_ASSERT_EQUAL_INT( 5, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_THURSDAY ) );
    TEST_ASSERT_EQUAL_INT( 6, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_FRIDAY ) );
    TEST_ASSERT_EQUAL_INT( 7, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_SATURDAY ) );
    TEST_ASSERT_EQUAL_INT( 0, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_ALL ) );
    TEST_ASSERT_EQUAL_INT( 0, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_WEEKEND ) );
    TEST_ASSERT_EQUAL_INT( 0, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_WORK ) );
    TEST_ASSERT_EQUAL_INT( 0, TimeHelper::weekdaysEnumToNumber( Weekdays::WEEKLY_NONE ) );

}


