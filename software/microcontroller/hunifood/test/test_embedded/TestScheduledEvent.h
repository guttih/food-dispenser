#include "unity.h"
#include "./constants.h"
#include <Event.h>
#include <FeedingEvent.h>
#include <DailyRecurrence.h>
#include <WeeklyRecurrence.h>
#include <ScheduledEvent.h>
#include <memory>


void test_DailyRecurrence_ScheduledEvent_deserialize_then_serialize_returns_same_string( void )
{
    std::shared_ptr< Event > event = std::make_shared< FeedingEvent >( "Feeding cats", 10 );
    std::shared_ptr< Recurrence > recurrence = std::make_shared< DailyRecurrence >( 10, 20 );
    ScheduledEvent origEvent( "event1", event, recurrence );
    String origSerial = origEvent.serialize();

    ScheduledEvent deserialEvent( "event2", event, recurrence );
    deserialEvent.deserialize( origSerial );
    String resultSerial = deserialEvent.serialize();
    TEST_ASSERT_EQUAL_STRING( origSerial.c_str(), resultSerial.c_str() );
}

void test_DailyRecurrence_ScheduledEvent_equals_returns_true( void )
{
    std::shared_ptr< Event > event1 = std::make_shared< FeedingEvent >( "Feeding cats", 10 );
    std::shared_ptr< Recurrence > recurrence1 = std::make_shared< DailyRecurrence >( 10, 20 );
    ScheduledEvent se1( "event1", event1, recurrence1 );

    std::shared_ptr< Event > event2 = std::make_shared< FeedingEvent >( "Feeding cats", 10 );
    std::shared_ptr< Recurrence > recurrence2 = std::make_shared< DailyRecurrence >( 10, 20 );
    ScheduledEvent se2( "event1", event2, recurrence2 );

    TEST_ASSERT_TRUE( se1 == se2 );
}

void test_DailyRecurrence_ScheduledEvent_not_equals_returns_true( void )
{
    std::shared_ptr< Event > event1 = std::make_shared< FeedingEvent >( "Feeding cats", 10 );
    std::shared_ptr< Recurrence > recurrence1 = std::make_shared< DailyRecurrence >( 10, 20 );
    ScheduledEvent se1( "event1", event1, recurrence1 );

    std::shared_ptr< Event > event2 = std::make_shared< FeedingEvent >( "Feeding dogs", 15 );
    std::shared_ptr< Recurrence > recurrence2 = std::make_shared< DailyRecurrence >( 15, 30 );
    ScheduledEvent se2( "event2", event2, recurrence2 );

    TEST_ASSERT_TRUE( event1 != event2 );
}


void test_ScheduledEvent_set_and_get_id( void )
{
    std::shared_ptr< Event > event = std::make_shared< Event >( "any event" );
    std::shared_ptr< DailyRecurrence > recurrence = std::make_shared< DailyRecurrence >( 10, 20 );
    ScheduledEvent scheduled_event( "ID123", event, recurrence );

    scheduled_event.setId( "New ID" );
    TEST_ASSERT_EQUAL_STRING( "New ID", scheduled_event.getId().c_str() );
}

void test_ScheduledEvent_assignment_operator_different_FeedingEvents_returns_true( void )
{
    std::shared_ptr< Event > event1 = std::make_shared< FeedingEvent >( "Feeding cats", 10 );
    std::shared_ptr< Recurrence > recurrence1 = std::make_shared< DailyRecurrence >( 10, 20 );
    ScheduledEvent scheduled_event1( "event1", event1, recurrence1 );

    std::shared_ptr< Event > event2 = std::make_shared< FeedingEvent >( "Feeding dogs", 20 );
    std::shared_ptr< Recurrence > recurrence2 = std::make_shared< DailyRecurrence >( 15, 30 );
    ScheduledEvent scheduled_event2( "event2", event2, recurrence2, false );

    // Assign event1 to event2
    scheduled_event2 = scheduled_event1;

    // Both scheduled_events should be equal now
    TEST_ASSERT_TRUE_MESSAGE( scheduled_event1 == scheduled_event2,
                              ( String(
                                    "scheduled events should be the same se1: " ) + scheduled_event1.serialize() + " se2: " +
                                scheduled_event2.serialize() ).c_str() );
}

void test_ScheduledEvent_assignment_operator_different_Events_returns_true( void )
{
    std::shared_ptr< Event > event1 = std::make_shared< Event >( "Life is good" );
    std::shared_ptr< Recurrence > recurrence1 = std::make_shared< WeeklyRecurrence >( 10, 20, WEEKLY_WEEKEND );
    ScheduledEvent scheduled_event1( "event1", event1, recurrence1 );

    std::shared_ptr< Event > event2 = std::make_shared< Event >( "Life is bad" );
    std::shared_ptr< Recurrence > recurrence2 = std::make_shared< WeeklyRecurrence >( 15, 30, WEEKLY_WORK );
    ScheduledEvent scheduled_event2( "event2", event2, recurrence2 );

    // Assign event1 to event2
    scheduled_event2 = scheduled_event1;

    // Both scheduled_events should be equal now
    TEST_ASSERT_TRUE_MESSAGE( scheduled_event1 == scheduled_event2,
                              ( String(
                                    "scheduled events should be the same se1: " ) + scheduled_event1.serialize() + " se2: " +
                                scheduled_event2.serialize() ).c_str() );
}

void test_ScheduledEvent_assignment_operator_different_types_returns_false( void )
{
    std::shared_ptr< Event > event1 = std::make_shared< Event >( "Life is good" );
    std::shared_ptr< Recurrence > recurrence1 = std::make_shared< WeeklyRecurrence >( 10, 20, WEEKLY_THURSDAY );
    ScheduledEvent scheduled_event1( "event1", event1, recurrence1 );

    std::shared_ptr< Event > feedingEvent1 = std::make_shared< FeedingEvent >( "Feeding dogs", 20 );
    std::shared_ptr< Recurrence > recurrence2 = std::make_shared< DailyRecurrence >( 15, 30 );
    ScheduledEvent scheduled_event2( "event2", feedingEvent1, recurrence2 );

    // Assign event1 to event2
    scheduled_event2 = scheduled_event1;

    // Both scheduled_events should be equal now
    TEST_ASSERT_FALSE_MESSAGE( scheduled_event1 == scheduled_event2,
                               ( String(
                                     "scheduled events should NOT be the same : " ) + scheduled_event1.serialize() + " se2: " +
                                 scheduled_event2.serialize() ).c_str() );
}

void test_ScheduledEvent_assignment_operator_different_type_events_returns_false( void )
{
    std::shared_ptr< Event > event1 = std::make_shared< FeedingEvent >( "Feeding cats", 10 );
    std::shared_ptr< Recurrence > recurrence1 = std::make_shared< DailyRecurrence >( 10, 20 );
    ScheduledEvent scheduled_event1( "event1", event1, recurrence1 );

    std::shared_ptr< Event > event2 = std::make_shared< Event >( "Basic event" );
    std::shared_ptr< Recurrence > recurrence2 = std::make_shared< WeeklyRecurrence >( 30 );
    ScheduledEvent scheduled_event2( "event2", event2, recurrence2 );

    // Attempt to assign event1 to event2, this should fail due to type mismatch
    scheduled_event2 = scheduled_event1;

    // Events should not be equal
    TEST_ASSERT_TRUE( scheduled_event1 != scheduled_event2 );
}

