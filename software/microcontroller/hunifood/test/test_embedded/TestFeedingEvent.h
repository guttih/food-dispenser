#include "unity.h"
#include "./constants.h"
#include <FeedingEvent.h>

void test_create_FeedingEvent( void )
{
    String description = "Feeding Event Test";
    unsigned long portionSize = 100;

    FeedingEvent feedingEvent( description, portionSize );

    TEST_ASSERT_EQUAL_STRING( description.c_str(), feedingEvent.getDescription().c_str() );
    TEST_ASSERT_EQUAL( portionSize, feedingEvent.getPortionSize() );
    TEST_ASSERT_EQUAL( EventType::FEEDING, feedingEvent.getType() );
}

void test_FeedingEvent_serialization( void )
{
    String description = "Feeding Event Test";
    unsigned long portionSize = 100;

    FeedingEvent feedingEvent1( description, portionSize );
    String serializedEvent = feedingEvent1.serialize();
    FeedingEvent feedingEvent2( "Default", 0 );

    feedingEvent2.deserialize( serializedEvent );

    TEST_ASSERT_EQUAL_STRING( description.c_str(), feedingEvent2.getDescription().c_str() );
    TEST_ASSERT_EQUAL( portionSize, feedingEvent2.getPortionSize() );
    TEST_ASSERT_EQUAL( EventType::FEEDING, feedingEvent2.getType() );

}

void test_FeedingEventEqualOperator( void )
{
    String description = "Feeding Event Test";
    unsigned long portionSize = 100;

    FeedingEvent feedingEvent1( description, portionSize );
    FeedingEvent feedingEvent2( description, portionSize );

    TEST_ASSERT_TRUE( feedingEvent1 == feedingEvent2 );
}

void test_virtual_EventType_toString( void )
{
    String description = "Feeding Event Test";
    unsigned long portionSize = 100;

    FeedingEvent feedingEvent( description, portionSize );

    TEST_ASSERT_EQUAL_STRING( STR_FEEDING, feedingEvent.getTypeString().c_str() );
}

