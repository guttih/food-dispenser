#ifndef SCHEDULER_TEST_H
#define SCHEDULER_TEST_H

#include "unity.h"
#include "./constants.h"
#include <ScheduledEvent.h>
#include <Scheduler.h>
#include <SDCardStorage.h>
#include <memory>

// Mock the IStorage interface
class MockStorage : public IStorage
{
public:
    bool loadCall = false; // A flag to indicate if the 'load' method was called
    bool saveCall = false; // A flag to indicate if the 'save' method was called

    bool load( std::vector< std::shared_ptr< ScheduledEvent > >& eventList ) override
    {
        // Set the flag to true when the 'load' method is called
        loadCall = true;
        eventList.push_back( std::make_shared< ScheduledEvent >( "feeding1",
                                                                 std::make_shared< FeedingEvent >( "I am a daily scheduled feeding event", 12 ),
                                                                 std::make_shared< DailyRecurrence >( now() + 60 ) ) );

        eventList.push_back( std::make_shared< ScheduledEvent >( "feeding2",
                                                                 std::make_shared< FeedingEvent >( "I am the second daily scheduled feeding event", 8 ),
                                                                 std::make_shared< DailyRecurrence >( now() + 120 ) ) );

        eventList.push_back( std::make_shared< ScheduledEvent >( "feeding3",
                                                                 std::make_shared< FeedingEvent >( "I am the third daily scheduled feeding event", 16 ),
                                                                 std::make_shared< DailyRecurrence >( now() + 300 ) ) );
        return true;

    }

    bool save( const std::vector< std::shared_ptr< ScheduledEvent > >& eventList ) override
    {
        saveCall = true;
        return true;
    }

    String createId( unsigned long ul = 0 ) override
    {

        static unsigned long lastId = 0;
        lastId++;
        return String( lastId );
    }
    bool begin() override
    {
        return true;
    }

};

// Create the MockStorage.
std::shared_ptr< MockStorage > storage = std::make_shared< MockStorage >();

//Inject it to the Scheduler.
Scheduler scheduler( storage );


void test_scheduler_add_getItem( void )
{
    std::shared_ptr< ScheduledEvent > sd1 =
        std::make_shared< ScheduledEvent >( "feeding1", std::make_shared< FeedingEvent >( "I am a daily scheduled feeding event", 12 ),
                                            std::make_shared< DailyRecurrence >( now() + 60 ) );
    // auto sd2 = std::make_shared< ScheduledEvent >( "feeding2", std::make_shared< FeedingEvent >( "I am a daily scheduled feeding event", 13 ),
    //                                                std::make_shared< DailyRecurrence >( now() + 60 ) );

    TEST_ASSERT_EQUAL( 0, scheduler.count() );

    scheduler.add( sd1 );
    TEST_ASSERT_EQUAL( 1, scheduler.count() );

    std::shared_ptr< ScheduledEvent > retrievedSd1 = scheduler.getItem( 0 );
    auto retrievedEvent = scheduler.getItem( 0 );
    // The compareScheduledEvent would be your own function to compare two ScheduledEvents
    TEST_ASSERT_TRUE( sd1 == retrievedEvent );
}

void test_Scheduler_sortByNextTrigger( void )
{
    int index = 0;
    scheduler.removeAllItems();
    auto event1 = std::make_shared< ScheduledEvent >( "Event1", std::make_shared< Event >( "Life is good +60" ),
                                                      std::make_shared< DailyRecurrence >( now() + 60 ) );
    auto event2 = std::make_shared< ScheduledEvent >( "Event2", std::make_shared< Event >( "Yes, life is wonderful +20" ),
                                                      std::make_shared< DailyRecurrence >( now() + 20 ) );
    auto event3 = std::make_shared< ScheduledEvent >( "FeedingEvent3", std::make_shared< FeedingEvent >( "I am a daily scheduled feeding event +40", 13 ),
                                                      std::make_shared< DailyRecurrence >( now() + 40 ) );
    auto event4 = std::make_shared< ScheduledEvent >( "FeedingEvent4", std::make_shared< FeedingEvent >( "I am a daily scheduled feeding event +0", 14 ),
                                                      std::make_shared< DailyRecurrence >( now() ) );

    scheduler.add( event4 );
    scheduler.add( event3 );
    scheduler.add( event2 );
    scheduler.add( event1 );



    scheduler.sortByNextTrigger( now() );
    TEST_ASSERT_TRUE_MESSAGE( event4 == scheduler.getItem( index ), (
                                  "case a: event4 should have been first, but got" +
                                  ( scheduler.getItem( index )->serialize() ) + " instead" ).c_str() );

    index = 1;
    TEST_ASSERT_TRUE_MESSAGE( event2 == scheduler.getItem( index ), (
                                  "case b: event2 should be at index " + String( index ) + ", but got" +
                                  ( scheduler.getItem( index )->serialize() ) + " instead" ).c_str() );

    index = 2;
    TEST_ASSERT_TRUE_MESSAGE( event3 == scheduler.getItem( index ), (
                                  "case c: event3 should be at index " + String( index ) + ", but got" +
                                  ( scheduler.getItem( index )->serialize() ) + " instead" ).c_str() );
    index = 3;
    TEST_ASSERT_TRUE_MESSAGE( event1 == scheduler.getItem( index ), (
                                  "case d: event4 should be at index " + String( index ) + ", but got" +
                                  ( scheduler.getItem( index )->serialize() ) + " instead" ).c_str() );

    //reverse order
    scheduler.sortByNextTrigger( now(), true );

    index = 0;
    TEST_ASSERT_TRUE_MESSAGE( event1 == scheduler.getItem( index ), (
                                  "case e: event1 should be at index " + String( index ) + ", but got" +
                                  ( scheduler.getItem( index )->serialize() ) + " instead" ).c_str() );
    index = 3;
    TEST_ASSERT_TRUE_MESSAGE( event4 == scheduler.getItem( index ), (
                                  "case f: event4 should be at index " + String( index ) + ", but got" +
                                  ( scheduler.getItem( index )->serialize() ) + " instead" ).c_str() );

}

void test_Scheduler_Load_Calls_Storage_LoadAndSave()
{
    auto storage = std::make_shared< MockStorage >();
    Scheduler scheduler( storage );
    TEST_ASSERT_FALSE_MESSAGE( storage->loadCall, "Load should not have been called yet" );
    scheduler.load();
    TEST_ASSERT_TRUE_MESSAGE( storage->loadCall, "Load should have been called on the storage object" );

    TEST_ASSERT_EQUAL_MESSAGE( 3, scheduler.count(), "There should be 3 items in the scheduler" );

    TEST_ASSERT_FALSE_MESSAGE( storage->saveCall, "Save should not have been called yet" );
    scheduler.save();
    TEST_ASSERT_TRUE_MESSAGE( storage->saveCall, "Save should have been called on the storage object" );
}

void test_SchedulerIStorageMockCreateId()
{
    std::shared_ptr< MockStorage > storage = std::make_shared< MockStorage >();

    //Inject it to the Scheduler.
    Scheduler scheduler( storage );

    String id1 = scheduler.createId();
    String id2 = scheduler.createId();

    TEST_ASSERT_NOT_EQUAL( id1, id2 );
    TEST_ASSERT_EQUAL_INT_MESSAGE( 1, id1.toInt(),
                                   "This storage should have it's own createId method that starts with 1" );
    TEST_ASSERT_EQUAL_INT_MESSAGE( 2, id2.toInt(),
                                   "This storage should have it's own createId method that starts with 1 and then increments by 1" );
}

void test_SchedulerSDCardStorageCreateId()
{
    // Create the SDCardStorage.
    std::shared_ptr< SDCardStorage > storage = std::make_shared< SDCardStorage >( false );

    Scheduler scheduler( storage );
    String id1 = scheduler.createId(),
           id2 = scheduler.createId(),
           id3 = scheduler.createId();
    TEST_ASSERT_TRUE_MESSAGE( id1 != id2, String( "Case 1: Id's should be different: (" + id1 + " == " + id2 + ")" ).c_str() );
    TEST_ASSERT_TRUE_MESSAGE( id1 != id3, String( "Case 2: Id's should be different: (" + id1 + " == " + id3 + ")" ).c_str() );
}


#endif // SCHEDULER_TEST_H