#include "unity.h"
#include "./constants.h"
#include <DailyRecurrence.h>


void test_DailyRecurrence_constructor( void )
{
    time_t startTime = 12345678;
    DailyRecurrence dr = DailyRecurrence( startTime );
    TEST_ASSERT_EQUAL_UINT32( startTime, dr.getStartTime() );
    TEST_ASSERT_EQUAL( RecurrenceType::DAILY, dr.getType() );
    TEST_ASSERT_EQUAL_STRING( STR_RECURRENCE_DAILY, dr.getTypeString().c_str() );
}

/**
 * @brief Test that the next trigger is calculated correctly
 *
 * @param objectTriggerDifferenceFromCurrentTime - The number of seconds the current time is offset from the start time
 * @param correctValue - How many seconds should pass until DailyRecurrence object is triggered
 *
 */

void DailyRecurrence_SecondsUntilTriggeredHelper( int objectTriggerDifferenceFromCurrentTime, int correctValue )
{
    time_t startTime = 1702364835;
    time_t current = startTime - objectTriggerDifferenceFromCurrentTime;

    DailyRecurrence dr = DailyRecurrence( startTime );

    long secondsUntil = dr.getSecondsUntilTriggered( current );
    String error = "There should be " + String( correctValue ) + " seconds until the next trigger.  " +
                   String( "start:" )     + TimeHelper::formatTimeString( startTime ) + " - " + String( startTime ) +
                   String( "  current:" ) + TimeHelper::formatTimeString( current )   + " - " + String( current ) +
                   String( "  trigger:" ) + dr.getNextTriggerAsString( current )      + " - " + String( dr.getNextTrigger( current ) );

    TEST_ASSERT_EQUAL_INT32_MESSAGE( correctValue, secondsUntil, error.c_str() );
}

void DailyRecurrence_SecondsUntilTriggeredMinus2()
{
    DailyRecurrence_SecondsUntilTriggeredHelper( -2, ( SECS_PER_DAY - 2 ) );
}
void DailyRecurrence_SecondsUntilTriggeredMinus1()
{
    DailyRecurrence_SecondsUntilTriggeredHelper( -1, ( SECS_PER_DAY - 1 ) );
}

void DailyRecurrence_SecondsUntilTriggeredZero()
{
    DailyRecurrence_SecondsUntilTriggeredHelper( 0, 0 );
}

void DailyRecurrence_SecondsUntilTriggeredPlus1()
{
    DailyRecurrence_SecondsUntilTriggeredHelper( 1, 1 );
}


void test_DailyRecurrence_Equals( void )
{
    time_t testTime = 1623751562;     // Some random time
    DailyRecurrence dr1( testTime );
    DailyRecurrence dr2( testTime );
    DailyRecurrence dr3( testTime + 1000 );

    TEST_ASSERT_TRUE( dr1.equals( dr2 ) );
    TEST_ASSERT_FALSE( dr1.equals( dr3 ) );
}

void test_DailyRecurrence_Equality_Operator( void )
{
    time_t testTime = 1623751562;     // Some random time
    DailyRecurrence dr1( testTime );
    DailyRecurrence dr2( testTime );
    DailyRecurrence dr3( testTime + 1000 );

    TEST_ASSERT_TRUE( dr1 == dr2 );
    TEST_ASSERT_FALSE( dr1 == dr3 );
}

void test_DailyRecurrence_Inequality_Operator( void )
{
    time_t testTime = 1623751562;     // Some random time
    DailyRecurrence dr1( testTime );
    DailyRecurrence dr2( testTime );
    DailyRecurrence dr3( testTime + 1000 );

    TEST_ASSERT_FALSE( dr1 != dr2 );
    TEST_ASSERT_TRUE( dr1 != dr3 );
}

void test_DailyRecurrence_deserialize( void )
{
    time_t startTime = 12345678;
    DailyRecurrence dr = DailyRecurrence( startTime );
    String serialized = dr.serialize();
    dr.deserialize( serialized );

    TEST_ASSERT_EQUAL( dr.getType(), RecurrenceType::DAILY );
}

void test_DailyRecurrence_AssignmentOperator_SameType( void )
{
    DailyRecurrence rec1( 10, 20 );
    DailyRecurrence rec2( 30, 40 );

    rec1 = rec2;

    TEST_ASSERT_EQUAL( rec1.getType(), rec2.getType() );
    TEST_ASSERT_EQUAL( rec1.getStartTime(), rec2.getStartTime() );
    TEST_ASSERT_EQUAL( rec1.getStopTime(), rec2.getStopTime() );

    TEST_ASSERT_TRUE( rec1 == rec2 );
    TEST_ASSERT_FALSE( rec1 != rec2 );
}

void test_WeeklyRecurrence_DailyRecurrence_AssignmentOperator_WhenDeclaring( void )
{
    DailyRecurrence rec1( 10, 20 ); // Subclass
    DailyRecurrence rec2=rec1;


    TEST_ASSERT_EQUAL( rec2.getType(), RecurrenceType::DAILY );
    TEST_ASSERT_EQUAL( rec2.getType(), rec1.getType() );
    TEST_ASSERT_EQUAL( 10, rec2.getStartTime() );
    TEST_ASSERT_EQUAL( 20, rec1.getStopTime() );

    TEST_ASSERT_TRUE( rec1 == rec2 );
    TEST_ASSERT_FALSE( rec1 != rec2 );
}
