#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <TimeHelper.h>
#include <Recurrence.h>

const char* STR_EVENT_BASIC = "BASIC";
const char* STR_WEEKLY = "WEEKLY";
const char* STR_FEEDING = "FEEDING";
const char* STR_UNKNOWN = "UNKNOWN";
const char* STR_RECURRENCE_BASIC = "BASIC";
const char* STR_RECURRENCE_DAILY = "DAILY";

char weekdaysNames[ 8 ][ 10 ] = { "", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

String formatTriggerValues( time_t current, Recurrence& dr )
{
    return
        String( "(start:" ) + TimeHelper::formatTimeString( dr.getStartTime() ) + " - " + String( dr.getStartTime() ) +
        String( ") (current:" ) + TimeHelper::formatTimeString( current )   + " - " + String( current ) +
        String( ") (trigger:" ) + dr.getNextTriggerAsString( current )      + " - " + String( dr.getNextTrigger( current ) ) + ")" +
        String( " After: " ) + String( dr.getSecondsUntilTriggered( current ) ) + " seconds. ";
}

String formatTriggerValues( time_t &startTime, time_t &stoptime, time_t &current )
{

    return
        String( "(start:" )     + TimeHelper::formatTimeString( startTime ) + " - " + String( startTime ) +
        String( ") (current:" ) + TimeHelper::formatTimeString( current )   + " - " + String( current )   +
        String( ") (trigger:" ) + TimeHelper::formatTimeString( TimeHelper::calculateNextTriggerTime( startTime, stoptime, current ) ) +
        " - " + String( TimeHelper::calculateNextTriggerTime( startTime, stoptime, current ) ) +
        String( ") (stop:" )      + TimeHelper::formatTimeString( stoptime )  + " - " + String( stoptime ) + ")";
}

String formatTriggerValues( time_t &startTime, time_t &stoptime, time_t &current, String errorMessage )
{

    return errorMessage + formatTriggerValues( startTime, stoptime, current );
}

#endif