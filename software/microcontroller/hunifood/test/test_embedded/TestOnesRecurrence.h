#include "unity.h"

#include "./constants.h"
#include <DailyRecurrence.h>




void test_OnesRecurrence_constructor( void )
{
    time_t startTime = 1629863386; // Random timestamp
    DailyRecurrence ones = DailyRecurrence( startTime, SECS_PER_DAY );

    TEST_ASSERT_TRUE( RecurrenceType::DAILY == ones.getType() );
    TEST_ASSERT_EQUAL_INT( startTime, ones.getStartTime() );
    TEST_ASSERT_EQUAL_INT( startTime, ones.getStopTime() );
    TEST_ASSERT_EQUAL_INT( startTime, ones.getNextTrigger( startTime ) );
    TEST_ASSERT_EQUAL_STRING( RecurrenceTypeHelper::toString( RecurrenceType::DAILY ).c_str(), ones.getTypeString().c_str() );

    DailyRecurrence two = DailyRecurrence( startTime );
    TEST_ASSERT_EQUAL_INT( MAX_LONG, two.getStopTime() );
}

void test_OnesRecurrence_equals( void )
{
    DailyRecurrence or1( 12345 );
    DailyRecurrence or2( 12345 );
    TEST_ASSERT_TRUE( or1.equals( or2 ) );
    TEST_ASSERT_TRUE( or1.getStartTime() == 12345 );

    or2.setStopTime( 12346 );
    TEST_ASSERT_EQUAL_INT_MESSAGE( 12346, or2.getStopTime(), "Stop time should be 12346" );
    TEST_ASSERT_FALSE( or1.equals( or2 ) );
}

void test_OnesRecurrence_operator_not_equal()
{
    time_t startTime1 = now();
    DailyRecurrence recurrence1( startTime1, startTime1 + SECS_PER_DAY );

    time_t startTime2 = now() + 100;
    DailyRecurrence recurrence2( startTime2, SECS_PER_DAY );

    // Assuming two OnesRecurrence instances are not equal if they have different start times
    TEST_ASSERT_FALSE_MESSAGE( recurrence1.equals( recurrence2 ), "case 1 equal: DailyRecurrence instances with different start times should not be equal" );
    TEST_ASSERT_TRUE_MESSAGE( recurrence1 !=  recurrence2, "case 2 != operator: DailyRecurrence instances with different start times should not be equal" );

    time_t stopTime3 = now();
    DailyRecurrence recurrence3( startTime1, stopTime3 );

    TEST_ASSERT_FALSE_MESSAGE( recurrence1.equals( recurrence3 ), "case 3 equal: DailyRecurrence instances with different stop times should not be equal" );
    TEST_ASSERT_TRUE_MESSAGE( recurrence1 != recurrence3, "case 4 != operator: DailyRecurrence instances with different stop times should not be equal" );
}

void test_OnesRecurrence_serialize( void )
{
    time_t startTime = 1629863386;  // Random timestamp
    time_t stopTime = 1838265600;
    DailyRecurrence ones( startTime, stopTime );

    String serializedData = ones.serialize();

    TEST_ASSERT_TRUE( serializedData.indexOf( RecurrenceTypeHelper::toString( RecurrenceType::DAILY ) ) != -1 );
    TEST_ASSERT_TRUE( serializedData.indexOf( String( ATTR_STARTTIME ) ) != -1 );
    TEST_ASSERT_TRUE( serializedData.indexOf( String( startTime ) ) != -1 );
    TEST_ASSERT_TRUE( serializedData.indexOf( String( ATTR_STOPTIME ) ) != -1 );
    TEST_ASSERT_TRUE( serializedData.indexOf( String( stopTime ) ) != -1 );
    TEST_ASSERT_TRUE( serializedData.indexOf( String( ATTR_TYPE ) ) != -1 );
    TEST_ASSERT_TRUE( serializedData.indexOf( String( RecurrenceTypeHelper::toString( RecurrenceType::DAILY ) ) ) != -1 );
    TEST_ASSERT_TRUE( serializedData.indexOf( String( "1838265600" ) ) != -1 );
}

void test_OnesRecurrence_deserialize( void )
{
    time_t startTime = 1629863386;  // Random timestamp
    time_t stopTime = MAX_LONG;
    time_t stopTimeManual = 1629863386 + 10000;
    DailyRecurrence ones( startTime );

    String serializedData = ones.serialize();
    ones.deserialize( serializedData );

    TEST_ASSERT_EQUAL_INT( startTime, ones.getStartTime() );
    TEST_ASSERT_EQUAL_INT( stopTime, ones.getStopTime() );
    TEST_ASSERT_TRUE( RecurrenceType::DAILY == ones.getType() );
    TEST_ASSERT_EQUAL_STRING( RecurrenceTypeHelper::toString( RecurrenceType::DAILY ).c_str(), ones.getTypeString().c_str() );

    DailyRecurrence second( 0, stopTimeManual );
    TEST_ASSERT_EQUAL_INT( 0, second.getStartTime() );
    TEST_ASSERT_TRUE( stopTimeManual == second.getStopTime() );

    second.deserialize( ones.serialize() );

    TEST_ASSERT_EQUAL_INT( second.getStartTime(), ones.getStartTime() );
    TEST_ASSERT_EQUAL_INT( second.getStopTime(), ones.getStopTime() );
}

void test_OnesRecurrence_serialize_and_deserialize( void )
{
    DailyRecurrence or1( 12345, SECS_PER_DAY );
    String serialized = or1.serialize();

    DailyRecurrence or2( 0, SECS_PER_DAY );
    TEST_ASSERT_TRUE( or2.deserialize( serialized ) );
    TEST_ASSERT_TRUE( or1.equals( or2 ) );
}

void test_OnesRecurrence_CompareOnesRecurrence( void )
{

    DailyRecurrence ones( 1702364835, SECS_PER_DAY );
    DailyRecurrence ones2( 100, SECS_PER_DAY );

    TEST_ASSERT_FALSE( ones == ones2 );
    TEST_ASSERT_TRUE( ones != ones2 );

    ones2.deserialize( ones.serialize() );

    TEST_ASSERT_TRUE( ones == ones2 );
    TEST_ASSERT_FALSE( ones != ones2 );

}

void test_OnesRecurrence_GetSecondsUntilNextTrigger( void )
{
    time_t now = 1000;
    DailyRecurrence ones( now, now );
    time_t nowTest;
    String errorMessage;;
    int offset;

    offset = -2;
    nowTest = now + offset;
    errorMessage = "Should return 2 if next trigger is 2 second in the ";
    errorMessage +=String( offset > 0 ? "future" : "past" ) + ".  trigger:" + String( nowTest ) + ".  trigger:" + String( ones.getNextTrigger( nowTest ) );

    TEST_ASSERT_EQUAL_INT_MESSAGE( 2, ones.getSecondsUntilTriggered( nowTest ), errorMessage.c_str() );

    offset = -1;
    nowTest = now + offset;
    errorMessage = "Should return 1 if next trigger is 1 second in the ";
    errorMessage +=String( offset > 0 ? "future" : "past" ) + ".  trigger:" + String( nowTest ) + ".  trigger:" + String( ones.getNextTrigger( nowTest ) );

    TEST_ASSERT_EQUAL_INT_MESSAGE( 1, ones.getSecondsUntilTriggered( nowTest ), errorMessage.c_str() );

    offset = 0;
    nowTest = now + offset;
    errorMessage = "Should return 0 if next trigger is now.";
    errorMessage +=String( offset > 0 ? "future" : "past" ) + ".  trigger:" + String( nowTest ) + ".  trigger:" + String( ones.getNextTrigger( nowTest ) );


    offset = 1;
    nowTest = now + offset;
    errorMessage = "Should return -1 if next trigger is 1 seconds in the ";
    errorMessage +=String( offset > 0 ? "future" : "past" ) + ".  trigger:" + String( nowTest ) + ".  trigger:" + String( ones.getNextTrigger( nowTest ) );
    TEST_ASSERT_EQUAL_INT_MESSAGE( -1, ones.getSecondsUntilTriggered( nowTest ), errorMessage.c_str() );

    offset = 2;
    nowTest = now + offset;
    errorMessage = "Should return -2 if next trigger is 2 seconds in the ";
    errorMessage +=String( offset > 0 ? "future" : "past" ) + ".  trigger:" + String( nowTest ) + ".  trigger:" + String( ones.getNextTrigger( nowTest ) );
    TEST_ASSERT_EQUAL_INT_MESSAGE( -2, ones.getSecondsUntilTriggered( nowTest ), errorMessage.c_str() );
}
