#include "unity.h"
#include "./constants.h"
#include <WeeklyRecurrence.h>

void test_WeeklyRecurrence_type()
{
    WeeklyRecurrence wr( 0 );
    TEST_ASSERT_EQUAL( RecurrenceType::WEEKLY, wr.getType() );
}

// Test NextTrigger method for WEEKLY_ALL
void test_WeeklyRecurrence_next_trigger_all()
{
    WeeklyRecurrence wr( 0, MAX_LONG, WEEKLY_ALL );
    int increment=0, current = 0;
    TEST_ASSERT_TRUE_MESSAGE( TimeHelper::isTimeDayOfWeekInWeekdays( wr.getNextTrigger( current ), WEEKLY_ALL ),
                              ( String( "Case 0: Next trigger should be today " ) ).c_str() );

    increment=1; current=SECS_PER_DAY * increment;
    TEST_ASSERT_TRUE_MESSAGE( TimeHelper::isTimeDayOfWeekInWeekdays( wr.getNextTrigger( current ), WEEKLY_ALL ),
                              ( String( "Case 1: Next trigger should be today + " ) + String( increment ) ).c_str() );

    increment=2; current=SECS_PER_DAY * increment;
    TEST_ASSERT_TRUE_MESSAGE( TimeHelper::isTimeDayOfWeekInWeekdays( wr.getNextTrigger( current ), WEEKLY_ALL ),
                              ( String( "Case 2: Next trigger should be today + " ) + String( increment ) ).c_str() );
    increment=3; current=SECS_PER_DAY * increment;
    TEST_ASSERT_TRUE_MESSAGE( TimeHelper::isTimeDayOfWeekInWeekdays( wr.getNextTrigger( current ), WEEKLY_ALL ),
                              ( String( "Case 3: Next trigger should be today + " ) + String( increment ) ).c_str() );
    increment=4; current=SECS_PER_DAY * increment;
    TEST_ASSERT_TRUE_MESSAGE( TimeHelper::isTimeDayOfWeekInWeekdays( wr.getNextTrigger( current ), WEEKLY_ALL ),
                              ( String( "Case 4: Next trigger should be today + " ) + String( increment ) ).c_str() );
    increment=5; current=SECS_PER_DAY * increment;
    TEST_ASSERT_TRUE_MESSAGE( TimeHelper::isTimeDayOfWeekInWeekdays( wr.getNextTrigger( current ), WEEKLY_ALL ),
                              ( String( "Case 5: Next trigger should be today + " ) + String( increment ) ).c_str() );
    increment=6; current=SECS_PER_DAY * increment;
    TEST_ASSERT_TRUE_MESSAGE( TimeHelper::isTimeDayOfWeekInWeekdays( wr.getNextTrigger( current ), WEEKLY_ALL ),
                              ( String( "Case 6: Next trigger should be today + " ) + String( increment ) ).c_str() );
    increment=7; current=SECS_PER_DAY * increment;
    TEST_ASSERT_TRUE_MESSAGE( TimeHelper::isTimeDayOfWeekInWeekdays( wr.getNextTrigger( current ), WEEKLY_ALL ),
                              ( String( "Case 7: Next trigger should be today + " ) + String( increment ) ).c_str() );
}

// Test equals method when comparing similar objects
void test_WeeklyRecurrence_equals_same()
{
    WeeklyRecurrence wr1( 0, MAX_LONG, WEEKLY_ALL );
    WeeklyRecurrence wr2( 0, MAX_LONG, WEEKLY_ALL );
    TEST_ASSERT_TRUE( wr1.equals( wr2 ) );
}

// Test equals method when comparing different objects
void test_WeeklyRecurrence_equals_different()
{
    WeeklyRecurrence wr1( 0, MAX_LONG, WEEKLY_ALL );
    WeeklyRecurrence wr2( 0, MAX_LONG, WEEKLY_SUNDAY );
    TEST_ASSERT_FALSE( wr1.equals( wr2 ) );
}

void test_WeeklyRecurrence_equalOperator()
{
    WeeklyRecurrence wr1( 0, MAX_LONG, WEEKLY_WEEKEND );
    WeeklyRecurrence wr2( 0, MAX_LONG, WEEKLY_WEEKEND );

    String errorStr = "Case 1: Same object" + wr1.serialize() + " " + wr2.serialize();
    TEST_ASSERT_TRUE_MESSAGE( wr1 == wr1, errorStr.c_str() );

    wr1.setWeekdays( WEEKLY_ALL ); errorStr = "Case 2: Weekdays are different" + wr1.serialize() + " " + wr2.serialize();
    TEST_ASSERT_FALSE_MESSAGE( wr1 == wr2, errorStr.c_str() );

    wr1.setWeekdays( WEEKLY_WEEKEND );  errorStr = "Case 3: Objects are the same" + wr1.serialize() + " " + wr2.serialize();
    TEST_ASSERT_TRUE_MESSAGE( wr1 == wr2, errorStr.c_str() );

    wr1.setStopTime( 0 );  errorStr = "Case 4: Stop times are different" + wr1.serialize() + " " + wr2.serialize();
    TEST_ASSERT_FALSE_MESSAGE( wr1 == wr2, errorStr.c_str() );

    wr1.setStopTime( MAX_LONG );  errorStr = "Case 5: Objects are the same" + wr1.serialize() + " " + wr2.serialize();
    TEST_ASSERT_TRUE_MESSAGE( wr1 == wr2, errorStr.c_str() );

    wr1.setStartTime( 1 );  errorStr = "Case 6: Start times are different" + wr1.serialize() + " " + wr2.serialize();
    TEST_ASSERT_FALSE_MESSAGE( wr1 == wr2, errorStr.c_str() );

    wr1.setStartTime( 0 );  errorStr = "Case 7: Objects are the same" + wr1.serialize() + " " + wr2.serialize();
    TEST_ASSERT_TRUE_MESSAGE( wr1 == wr2, errorStr.c_str() );

}

void test_WeeklyRecurrence_notEqualOperator()
{
    WeeklyRecurrence wr1( 0, MAX_LONG, WEEKLY_ALL );
    WeeklyRecurrence wr2( 0, MAX_LONG, WEEKLY_SUNDAY );
    TEST_ASSERT_TRUE_MESSAGE( wr1 != wr2, "Case 1:  Weekdays are different" );
    wr2.setWeekdays( WEEKLY_ALL );
    TEST_ASSERT_FALSE_MESSAGE( wr1 != wr2, "Case 2: Objects are the same" );

    wr2.setStopTime( wr2.getStopTime() + 1 );
    TEST_ASSERT_TRUE_MESSAGE( wr1 != wr2, "Case 3: Stop times are different" );

    wr2.setStopTime( MAX_LONG );
    TEST_ASSERT_FALSE_MESSAGE( wr1 != wr2, "Case 4: Objects are the same" );
    wr2.setStartTime( wr2.getStartTime() + 1 );
    TEST_ASSERT_TRUE_MESSAGE( wr1 != wr2, "Case 5: Start times are different" );
    wr2.setStartTime( 0 );
    TEST_ASSERT_FALSE_MESSAGE( wr1 != wr2, ( String( "Case 6: Objects are the same" ) +  wr1.serialize() + " " + wr2.serialize() ).c_str() );

}

// Test deserialize with valid data
void test_WeeklyRecurrence_deserialize_valid()
{
    WeeklyRecurrence wr1( 22, MAX_LONG, WEEKLY_ALL );
    WeeklyRecurrence wr2( 0, MAX_LONG - 1, WEEKLY_SUNDAY );
    TEST_ASSERT_TRUE( wr2.deserialize( wr1.serialize() ) );
    TEST_ASSERT_TRUE( wr2 == wr1 );

    wr1.setWeekdays( WEEKLY_WEEKEND );
    wr1.setStartTime( 50 );
    wr1.setStopTime( MAX_LONG - 10000 );

    TEST_ASSERT_TRUE( wr1 != wr2 );
    TEST_ASSERT_TRUE( wr2.deserialize( wr1.serialize() ) );
    TEST_ASSERT_TRUE( wr2 == wr1 );


}

// Test deserialize with invalid data
void test_WeeklyRecurrence_deserialize_invalid()
{
    String data = "{\"type\": \"weekdays\", \"startTime\": 0, \"stopTime\": 123456}";
    WeeklyRecurrence wr( 0 );
    TEST_ASSERT_FALSE( wr.deserialize( data ) );
}

String weekDayString( time_t time )
{
    return TimeHelper::weekdayNumberToString( weekday( time ) ).substring( 0, 3 );
}



/**
 * @brief Test that the next trigger is calculated correctly
 *
 * Creates a WeeklyRecurrence object with a start time of 2023-12-04 08:00:10 Monday
 *
 * @param objectTriggerDifferenceFromCurrentTime - The number of seconds the current time is offset from the start time
 * @param correctValue - How many seconds should pass until WeeklyRecurrence object is triggered
 * @param weekdays - The weekdays the WeeklyRecurrence object is set to
 *
 */
void WeeklyRecurrence_SecondsUntilTriggeredHelperWeek( int objectTriggerDifferenceFromCurrentTime, int correctValue, Weekdays weekdays )
{
    time_t startTime = makeTime( TimeHelper::createTmElement( 2023, 12, 4, 8, 0, 10 ) ); // 2023-12-04 08:00:10 Monday
    time_t current = startTime - objectTriggerDifferenceFromCurrentTime;

    WeeklyRecurrence wr = WeeklyRecurrence( startTime, MAX_LONG, weekdays );

    long secondsUntil = wr.getSecondsUntilTriggered( current );
    String error = "There should be " + String( correctValue ) + " seconds until trigger  " +
                   String( "(start:" )     + TimeHelper::formatTimeString( startTime ) + " " + weekDayString( startTime ) + " " + String( startTime ) +
                   String( ") (current:" ) + TimeHelper::formatTimeString( current )   + " " + weekDayString( current ) +
                   String( ") trigger:" ) + wr.getNextTriggerAsString( current )      + " " + weekDayString( wr.getNextTrigger( current ) ) + ")";

    TEST_ASSERT_EQUAL_INT32_MESSAGE( correctValue, secondsUntil, error.c_str() );
}

void WeeklyRecurrence_SecondsUntilTriggeredMinus2AllWeek()
{
    WeeklyRecurrence_SecondsUntilTriggeredHelperWeek( -2, ( SECS_PER_DAY - 2 ), WEEKLY_ALL );
}

void WeeklyRecurrence_SecondsUntilTriggeredMinus1AllWeek()
{
    WeeklyRecurrence_SecondsUntilTriggeredHelperWeek( -1, ( SECS_PER_DAY - 1 ), WEEKLY_ALL );
}


void WeeklyRecurrence_SecondsUntilTriggeredZeroAllWeek()
{
    WeeklyRecurrence_SecondsUntilTriggeredHelperWeek( 0, 0, WEEKLY_ALL );
}

void WeeklyRecurrence_SecondsUntilTriggeredPlus1AllWeek()
{
    time_t offset = 1; //1 seconds to trigger:2023-12-04 08:00:10 Mon

    // Creates a WeeklyRecurrence object with a start time of 2023-12-04 08:00:10 Mon
    //                               and sets current time to 2023-12-04 08:00:09 Mon
    WeeklyRecurrence_SecondsUntilTriggeredHelperWeek( 1, offset, WEEKLY_ALL );
}

void WeeklyRecurrence_SecondsUntilTriggeredMinus2Weekend()
{
    time_t offset = 431998; //431998 seconds to trigger:2023-12-09 08:00:10 Sat

    // Creates a WeeklyRecurrence object with a start time of 2023-12-04 08:00:10 Monday
    //                               and sets current time to 2023-12-04 08:00:08 Monday
    WeeklyRecurrence_SecondsUntilTriggeredHelperWeek( -2, offset, WEEKLY_WEEKEND );
}

void WeeklyRecurrence_SecondsUntilTriggerZeroSunday()
{
    time_t offset = 518400.; //518400. seconds to  trigger:2023-12-10 08:00:10 Sun

    // Creates a WeeklyRecurrence object with a start time of 2023-12-04 08:00:10 Monday
    //                               and sets current time to 2023-12-04 08:00:10 Monday
    WeeklyRecurrence_SecondsUntilTriggeredHelperWeek( 0, offset, WEEKLY_SUNDAY );
}

void WeeklyRecurrence_SecondsUntilTriggerZeroWorkday()
{
    time_t offset = 0.; //0 seconds to trigger:2023-12-04 08:00:10 Mon
    // Creates a WeeklyRecurrence object with a start time of 2023-12-04 08:00:10 Monday
    //                               and sets current time to 2023-12-04 08:00:10 Monday
    WeeklyRecurrence_SecondsUntilTriggeredHelperWeek( 0, offset, WEEKLY_WORK );
}

void test_WeeklyRecurrence_AssignmentOperator_PreDeclared( void )
{
    WeeklyRecurrence rec1( 10, 20, WEEKLY_MONDAY );
    WeeklyRecurrence rec2( 30, 40, WEEKLY_NONE );

    rec1 = rec2;

    TEST_ASSERT_EQUAL( rec1.getType(), rec2.getType() );
    TEST_ASSERT_EQUAL( rec1.getStartTime(), rec2.getStartTime() );
    TEST_ASSERT_EQUAL( rec1.getStopTime(), rec2.getStopTime() );
    TEST_ASSERT_EQUAL( rec1.getWeekdays(), rec2.getWeekdays() );

    TEST_ASSERT_TRUE( rec1 == rec2 );
    TEST_ASSERT_FALSE( rec1 != rec2 );
}

void test_DailyRecurrence_WeeklyRecurrence_AssignmentOperator_WhenDeclaring( void )
{
    WeeklyRecurrence rec1( 10, 20, WEEKLY_MONDAY );
    WeeklyRecurrence rec2=rec1;

    TEST_ASSERT_EQUAL( 10, rec2.getStartTime() );
    TEST_ASSERT_EQUAL( 20, rec2.getStopTime() );
    TEST_ASSERT_EQUAL( WEEKLY_MONDAY, rec2.getWeekdays() );

    TEST_ASSERT_TRUE( rec1 == rec2 );
    TEST_ASSERT_FALSE( rec1 != rec2 );

}