// // root_test_setup.c

#include "unity.h"                 // Unity test framework
#include "./constants.h"              // Constants used in tests
#include <EventType.h>  // Your EventType helper class

// Test for eventType toString method
void test_EventType_toString( void )
{
    TEST_ASSERT_EQUAL_STRING( STR_EVENT_BASIC, EventTypeHelper::toString( EventType::BASIC ).c_str() );
    TEST_ASSERT_EQUAL_STRING( STR_FEEDING, EventTypeHelper::toString( EventType::FEEDING ).c_str() );
    TEST_ASSERT_EQUAL_STRING( STR_UNKNOWN, EventTypeHelper::toString( EventType::UNKNOWN ).c_str() );
}

// Test for eventType fromString method
void test_EventType_fromString( void )
{
    TEST_ASSERT_EQUAL( EventType::BASIC, EventTypeHelper::fromString( STR_EVENT_BASIC ) );
    TEST_ASSERT_EQUAL( EventType::FEEDING, EventTypeHelper::fromString( STR_FEEDING ) );
    TEST_ASSERT_EQUAL( EventType::UNKNOWN, EventTypeHelper::fromString( STR_UNKNOWN ) );
}

// Test for eventType isValid method
void test_EventType_isValid( void )
{
    TEST_ASSERT_TRUE( EventTypeHelper::isValid( EventType::BASIC ) );
    TEST_ASSERT_TRUE( EventTypeHelper::isValid( EventType::FEEDING ) );
    TEST_ASSERT_FALSE( EventTypeHelper::isValid( EventType::UNKNOWN ) );
}

// Test for eventType isValidAsString method
void test_EventType_isValidAsString( void )
{
    TEST_ASSERT_TRUE( EventTypeHelper::isValid( STR_EVENT_BASIC ) );
    TEST_ASSERT_TRUE( EventTypeHelper::isValid( STR_FEEDING ) );
    TEST_ASSERT_FALSE( EventTypeHelper::isValid( STR_UNKNOWN ) );
}

