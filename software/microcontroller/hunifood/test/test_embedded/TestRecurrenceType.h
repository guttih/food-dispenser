#include "unity.h"
#include "./constants.h"
#include <RecurrenceType.h>

void test_RecurrenceType_to_string_basic( void )
{
    TEST_ASSERT_EQUAL_STRING( STR_RECURRENCE_BASIC, RecurrenceTypeHelper::toString( RecurrenceType::BASIC ).c_str() );
}

void test_RecurrenceType_to_string_daily( void )
{
    TEST_ASSERT_EQUAL_STRING( STR_RECURRENCE_DAILY, RecurrenceTypeHelper::toString( RecurrenceType::DAILY ).c_str() );
}

void test_RecurrenceType_to_string_unknown( void )
{
    TEST_ASSERT_EQUAL_STRING( "UNKNOWN", RecurrenceTypeHelper::toString( RecurrenceType::UNKNOWN ).c_str() );
}

void test_RecurrenceType_from_string_valid( void )
{
    TEST_ASSERT_EQUAL( RecurrenceType::BASIC, RecurrenceTypeHelper::fromString( STR_RECURRENCE_BASIC ) );
    TEST_ASSERT_EQUAL( RecurrenceType::DAILY, RecurrenceTypeHelper::fromString( STR_RECURRENCE_DAILY ) );
    TEST_ASSERT_EQUAL( RecurrenceType::UNKNOWN, RecurrenceTypeHelper::fromString( "XYZABCDEFGH" ) );
}

void test_RecurrenceType_is_valid( void )
{
    TEST_ASSERT_TRUE( RecurrenceTypeHelper::isValid( RecurrenceType::BASIC ) );
    TEST_ASSERT_TRUE( RecurrenceTypeHelper::isValid( RecurrenceType::DAILY ) );
    TEST_ASSERT_FALSE( RecurrenceTypeHelper::isValid( RecurrenceType::UNKNOWN ) );
}

void test_RecurrenceType_is_valid_from_string( void )
{
    TEST_ASSERT_TRUE( RecurrenceTypeHelper::isValid( STR_RECURRENCE_BASIC ) );
    TEST_ASSERT_TRUE( RecurrenceTypeHelper::isValid( STR_RECURRENCE_DAILY ) );
    TEST_ASSERT_FALSE( RecurrenceTypeHelper::isValid( "XYZABCDEFGH" ) );
}

