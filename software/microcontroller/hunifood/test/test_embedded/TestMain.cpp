// // root_test_setup.c

#include "unity.h"

#include "Arduino.h"
#include "./TestRecurrenceType.h"
#include "./TestOnesRecurrence.h"
#include "./TestDailyRecurrence.h"
#include "./TestEventType.h"
#include "./TestFeedingEvent.h"
#include "./TestEvent.h"
#include "./TestTimeHelper.h"
#include "./TestWeeklyRecurrence.h"
#include "./TestScheduledEvent.h"
#include "./TestScheduler.h"


// Method to execute before each test
void setUp( void )
{
    // This is run before EACH test
}

// Method to execute after each test
void tearDown( void )
{
    // This is run after EACH test
}


void setup()
{
    delay( 2000 );

    UNITY_BEGIN();


    TEST_MESSAGE( "Running tests EventType and EventTypeHelper" );
    RUN_TEST( test_EventType_toString );
    RUN_TEST( test_EventType_fromString );
    RUN_TEST( test_EventType_isValid );
    RUN_TEST( test_EventType_isValidAsString );

    TEST_MESSAGE( "Running tests for Event class" );
    RUN_TEST( test_Event_constructor );
    RUN_TEST( test_Event_set_description );
    RUN_TEST( test_Event_type_should_be_basic );
    RUN_TEST( test_Event_equality_operator );
    RUN_TEST( test_Event_inequality_operator );
    RUN_TEST( test_Event_serialization_deserialization );

    TEST_MESSAGE( "Running tests for FeedingEvent class" );
    RUN_TEST( test_create_FeedingEvent );
    RUN_TEST( test_FeedingEvent_serialization );
    RUN_TEST( test_FeedingEventEqualOperator );
    RUN_TEST( test_virtual_EventType_toString );


    TEST_MESSAGE( "Running tests for RecurrenceType and RecurrenceTypeHelper" );
    RUN_TEST( test_RecurrenceType_to_string_basic );
    RUN_TEST( test_RecurrenceType_to_string_daily );
    RUN_TEST( test_RecurrenceType_to_string_unknown );
    RUN_TEST( test_RecurrenceType_from_string_valid );
    RUN_TEST( test_RecurrenceType_is_valid );
    RUN_TEST( test_RecurrenceType_is_valid_from_string );

    TEST_MESSAGE( "Running tests for TimeHelper class" );
    RUN_TEST( test_TimeHelperFormatTimeString );
    RUN_TEST( test_TimeHelper_formatTimeString );
    RUN_TEST( test_TimeHelper_createTmElement );
    RUN_TEST( test_TimeHelper_createTmElementLeapYear );
    RUN_TEST( test_TimeHelper_joinExtractedDateAndTime );
    RUN_TEST( test_TimeHelper_joinExtractedDateAndTimeLeapYear );
    RUN_TEST( test_TimeHelper_extractTime );
    RUN_TEST( test_TimeHelper_extractDate );
    RUN_TEST( test_TimeHelper_calculateNextTriggerTime );
    RUN_TEST( test_isTimeDayOfWeekInWeekdays );
    RUN_TEST( test_isTimeDayOfWeekInWeekdays2 );
    RUN_TEST( test_formatTimeString );
    RUN_TEST( test_WeekdaysToString );
    RUN_TEST( test_weekdayNumberToString );
    RUN_TEST( test_weekdayNumberToEnum );
    RUN_TEST( test_weekdaysEnumToNumber );

    TEST_MESSAGE( "Running tests for OnesRecurrence class" );
    RUN_TEST( test_OnesRecurrence_constructor );
    RUN_TEST( test_OnesRecurrence_equals );
    RUN_TEST( test_OnesRecurrence_operator_not_equal );
    RUN_TEST( test_OnesRecurrence_serialize );
    RUN_TEST( test_OnesRecurrence_deserialize );
    RUN_TEST( test_OnesRecurrence_serialize_and_deserialize );
    RUN_TEST( test_OnesRecurrence_CompareOnesRecurrence );
    RUN_TEST( test_OnesRecurrence_GetSecondsUntilNextTrigger );

    TEST_MESSAGE( "Running tests for DailyRecurrence class" );
    RUN_TEST( test_DailyRecurrence_constructor );
    RUN_TEST( DailyRecurrence_SecondsUntilTriggeredMinus2 );
    RUN_TEST( DailyRecurrence_SecondsUntilTriggeredMinus1 );
    RUN_TEST( DailyRecurrence_SecondsUntilTriggeredZero );
    RUN_TEST( DailyRecurrence_SecondsUntilTriggeredPlus1 );
    RUN_TEST( test_DailyRecurrence_Equals );
    RUN_TEST( test_DailyRecurrence_Equality_Operator );
    RUN_TEST( test_DailyRecurrence_Inequality_Operator );
    RUN_TEST( test_DailyRecurrence_deserialize );
    RUN_TEST( test_DailyRecurrence_AssignmentOperator_SameType );
    RUN_TEST( test_WeeklyRecurrence_DailyRecurrence_AssignmentOperator_WhenDeclaring );




    TEST_MESSAGE( "Running tests for WeeklyRecurrence class" );
    RUN_TEST( test_WeeklyRecurrence_type );
    RUN_TEST( test_WeeklyRecurrence_next_trigger_all );
    RUN_TEST( test_WeeklyRecurrence_equals_same );
    RUN_TEST( test_WeeklyRecurrence_equalOperator );
    RUN_TEST( test_WeeklyRecurrence_equals_different );
    RUN_TEST( test_WeeklyRecurrence_notEqualOperator );
    RUN_TEST( test_WeeklyRecurrence_deserialize_valid );
    RUN_TEST( test_WeeklyRecurrence_deserialize_invalid );
    RUN_TEST( WeeklyRecurrence_SecondsUntilTriggeredMinus2AllWeek );
    RUN_TEST( WeeklyRecurrence_SecondsUntilTriggeredMinus1AllWeek );
    RUN_TEST( WeeklyRecurrence_SecondsUntilTriggeredZeroAllWeek );
    RUN_TEST( WeeklyRecurrence_SecondsUntilTriggeredPlus1AllWeek );
    RUN_TEST( WeeklyRecurrence_SecondsUntilTriggeredMinus2Weekend );
    RUN_TEST( WeeklyRecurrence_SecondsUntilTriggerZeroSunday );
    RUN_TEST( WeeklyRecurrence_SecondsUntilTriggerZeroWorkday );
    RUN_TEST( test_WeeklyRecurrence_AssignmentOperator_PreDeclared );
    RUN_TEST( test_DailyRecurrence_WeeklyRecurrence_AssignmentOperator_WhenDeclaring );


    TEST_MESSAGE( "Running tests for ScheduledEvent class" );
    RUN_TEST( test_DailyRecurrence_ScheduledEvent_deserialize_then_serialize_returns_same_string );
    RUN_TEST( test_DailyRecurrence_ScheduledEvent_equals_returns_true );
    RUN_TEST( test_DailyRecurrence_ScheduledEvent_not_equals_returns_true );
    RUN_TEST( test_ScheduledEvent_set_and_get_id );
    RUN_TEST( test_ScheduledEvent_assignment_operator_different_Events_returns_true );
    RUN_TEST( test_ScheduledEvent_assignment_operator_different_FeedingEvents_returns_true );
    RUN_TEST( test_ScheduledEvent_assignment_operator_different_type_events_returns_false );
    RUN_TEST( test_ScheduledEvent_assignment_operator_different_types_returns_false );

    TEST_MESSAGE( "Running tests for Scheduler class" );
    RUN_TEST( test_scheduler_add_getItem );
    RUN_TEST( test_Scheduler_sortByNextTrigger );
    RUN_TEST( test_Scheduler_Load_Calls_Storage_LoadAndSave );
    RUN_TEST( test_SchedulerIStorageMockCreateId );
    RUN_TEST( test_SchedulerSDCardStorageCreateId );

    UNITY_END();
}
void loop()
{
    delay( 1000 );
}