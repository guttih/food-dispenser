#include "WebServerHandlerOld.h"

WebServerHandler::WebServerHandler() : server( 80 )
{
}

String WebServerHandler::buildDirectoryStructure( File dir, int numTabs, bool includeFiles )
{
    String result = "";
    while( true )
    {
        File entry = dir.openNextFile();
        if( !entry )
        {
            break;
        }

        for( int i = 0; i < numTabs - 1; i++ )
        {
            result += "|   ";
        }

        if( numTabs [] > 0 )
        {
            result += "|   ";
        }

        // Check if the opened entry is a directory or a file
        if( entry.isDirectory() )
        {
            result += "+---";
        }
        else
        {
            result += "|   ";
        }

        result += entry.name();

        // Put the file size immediately after the file name
        if( !entry.isDirectory() && includeFiles )
        {
            result += "\t\t" + String( entry.size() );
        }

        result += "\n";

        if( entry.isDirectory() )
        {
            result += buildDirectoryStructure( entry, numTabs + 1, includeFiles );
        }

        entry.close();
    }

    return result;
}

void WebServerHandler::begin()
{
    server.on( "/", HTTP_GET, onIndexRequest );

    server.on( "/items", HTTP_GET, onGetItem );
    server.on( "/items", HTTP_POST, onCreateItem );
    server.on( "/items", HTTP_PUT, onUpdateItem );
    server.on( "/items", HTTP_DELETE, onDeleteItem );

    server.on( "/download/*", HTTP_GET, []( AsyncWebServerRequest *request ) {
        String path = request->url();
        path.remove( 0, 9 ); // Remove "/files" from the URL
        path = "/" + path; // Add leading slash

        // Now the path variable would be equal to "/subfolder/file.name" or wherever the user pointed to in the URL

        // Print the path (optional)
        Serial.printf( "Requested path: %s\n", path.c_str() );

        // Serve the file
        serveFileFromSD( request, path.c_str(), "application/octet-stream" );
    } );

    server.on( "/files/*", HTTP_GET, []( AsyncWebServerRequest *request ) {
        String path = request->url();
        path.remove( 0, 6 ); // Remove "/files" from the URL
        path = "/" + path; // Add leading slash

        Serial.printf( "Requested path: %s\n", path.c_str() );
        // Now the path variable would be equal to "/subfolder/file.name" or wherever the user pointed to in the URL

        // Print the path (optional)
        if( path.endsWith( ".html" ) || path.endsWith( ".htm" ) )
        {
            request->send( SD, path, "text/html" );
        }
        else if( path.endsWith( ".css" ) )
        {
            request->send( SD, path, "text/css" );
        }
        else if( path.endsWith( ".js" ) )
        {
            request->send( SD, path, "application/javascript" );
        }
        else if( path.endsWith( ".png" ) )
        {
            request->send( SD, path, "image/png" );
        }
        else if( path.endsWith( ".gif" ) )
        {
            request->send( SD, path, "image/gif" );
        }
        else if( path.endsWith( ".jpg" ) || path.endsWith( ".jpeg" ) )
        {
            request->send( SD, path, "image/jpeg" );
        }
        else if( path.endsWith( ".ico" ) )
        {
            request->send( SD, path, "image/x-icon" );
        }
        else if( path.endsWith( ".xml" ) )
        {
            request->send( SD, path, "text/xml" );
        }
        else if( path.endsWith( ".pdf" ) )
        {
            request->send( SD, path, "application/x-pdf" );
        }
        else if( path.endsWith( ".zip" ) )
        {
            request->send( SD, path, "application/x-zip" );
        }
        else if( path.endsWith( ".gz" ) )
        {
            request->send( SD, path, "application/x-gzip" );
        }
        else if( path.endsWith( ".json" ) )
        {
            request->send( SD, path, "application/json" );
        }
        else if( path.endsWith( ".txt" ) ||
                 path.endsWith( ".md" ) )
        {
            request->send( SD, path, "text/plain" );
        }
        else
        {
            // Serve the file as download
            serveFileFromSD( request, path.c_str(), "application/octet-stream" );
        }
    } );

    server.on( "/dir/*", HTTP_GET, []( AsyncWebServerRequest *request ) {
        String result = "Directory structure:\n";
        String path = request->url();
        Serial.printf( "1. Requested path: %s\n", path.c_str() );
        path.remove( 0, 5 ); // Remove "/dir/" from the URL
        Serial.printf( "2.           path: %s\n", path.c_str() );
        path = "/" + path; // Add leading slash
        Serial.printf( "3.           path: %s\n", path.c_str() );

        result += buildDirectoryStructure( SD.open( path ), 0 );
        request->send( 200, "text/plain", result );

    } );
    server.on( "/files", HTTP_POST, onCreateOrUpdateFile );
    server.on( "/files", HTTP_DELETE, onDeleteFile );

    server.on( "/upload", HTTP_POST, []( AsyncWebServerRequest * request ){
        request->send( 200 );
    }, onUpload );

    // handle NotFound
    server.onNotFound( notFound );

    server.begin();
}

// Initialize the static File member in cpp file.
// Make sure the SD library is included, as the type File belongs to the SD library.
File WebServerHandler::file;

void WebServerHandler::onUpload( AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final )
{
    // handle upload of the file.
    if( !index ) // if first part of the file, create a new one
    {
        Serial.printf( "UploadStart: %s\n", filename.c_str() );
        if( !SD.exists( "/upload" ) )
        {
            SD.mkdir( "/upload" );
        }
        WebServerHandler::file = SD.open( "/upload/" + filename, FILE_WRITE ); // use WebServerHandler::file instead of file
    }

    for( size_t i=0; i < len; i++ )
    {
        WebServerHandler::file.write( data[ i ] ); // use WebServerHandler::file instead of file
    }

    if( final ) // if it is the last part of the file, close it
    {
        Serial.printf( "UploadEnd: %s, %u B\n", filename.c_str(), index + len );
        WebServerHandler::file.close(); // use WebServerHandler::file instead of file
    }
}

void WebServerHandler::serveFileFromSD( AsyncWebServerRequest *request, const char* filename, const char* contentType )
{
    if( SD.exists( filename ) )
    {
        File file = SD.open( filename );
        String content;
        while( file.available() )
        {
            content += String( ( char ) file.read() );
        }
        file.close();
        request->send( 200, contentType, content );
    }
    else
    {
        request->send( 404, "text/plain", "File not found" );
    }
}

void WebServerHandler::onIndexRequest( AsyncWebServerRequest *request )
{
    serveFileFromSD( request, "/index.html", "text/html" );
}

void WebServerHandler::notFound( AsyncWebServerRequest *request )
{
    request->send( 404, "text/plain", "Not found" );
}

void WebServerHandler::onGetItem( AsyncWebServerRequest *request )
{
    request->send( 200, "text/plain", "GET Item" );
}

void WebServerHandler::onCreateItem( AsyncWebServerRequest *request )
{
    request->send( 200, "text/plain", "POST Item" );
}

void WebServerHandler::onUpdateItem( AsyncWebServerRequest *request )
{
    request->send( 200, "text/plain", "PUT Item" );
}

void WebServerHandler::onDeleteItem( AsyncWebServerRequest *request )
{
    request->send( 200, "text/plain", "DELETE Item" );
}




// Create or Update File
void WebServerHandler::onCreateOrUpdateFile( AsyncWebServerRequest *request )
{
    String message;
    if( request->hasParam( "message", true ) ) //If received POST request to /file
    {
        message = request->getParam( "message", true )->value();//Get the value of the request
    }
    File file = SD.open( "/example.txt", FILE_WRITE );
    if( file )
    {
        file.println( message );
        file.close();
        request->send( 200, "text/plain", "File written successfully" );
    }
    else
    {
        request->send( 500, "text/plain", "File writing failed" );
    }
}



// Delete File
void WebServerHandler::onDeleteFile( AsyncWebServerRequest *request )
{
    if( SD.exists( "/example.txt" ) )
    {
        SD.remove( "/example.txt" );
        request->send( 200, "text/plain", "File deleted successfully" );
    }
    else
    {
        request->send( 404, "text/plain", "File not found" );
    }
}
