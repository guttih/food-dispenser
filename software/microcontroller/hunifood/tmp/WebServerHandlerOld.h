#ifndef WEBSERVERHANDLER_H
#define WEBSERVERHANDLER_H


#include <SD.h>
#include <FS.h>
#include <ESPAsyncWebServer.h>

class WebServerHandler
{
public:
    WebServerHandler();
    void begin();
    void handleClient();

private:

    static File file; // declaration of File for upload function

    static void notFound( AsyncWebServerRequest *request );
    static void serveFileFromSD( AsyncWebServerRequest *request, const char* filename, const char* contentType );
    static void onIndexRequest( AsyncWebServerRequest *request );

    static void onGetItem( AsyncWebServerRequest *request );
    static void onCreateItem( AsyncWebServerRequest *request );
    static void onUpdateItem( AsyncWebServerRequest *request );
    static void onDeleteItem( AsyncWebServerRequest *request );

    // file manipulation
    static void onUpload( AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final );
    static void onCreateOrUpdateFile( AsyncWebServerRequest *request );
    static void onDeleteFile( AsyncWebServerRequest *request );
    static String buildDirectoryStructure( File dir, int numTabs, bool includeFiles = true );


    AsyncWebServer server;
};

#endif