#ifndef CONSTANTS_H
#define CONSTANTS_H

#define BRIGHTNESS_CHANNEL 1
#define SERVO_CHANNEL 2
#define SERVO_FILE "/servo.txt"

#endif //CONSTANTS_H