#ifndef PAGE_DISPLAY_H
#define PAGE_DISPLAY_H

#pragma once

#include <FS.h>
#include "Free_Fonts.h" // Include the header file attached to this sketch

#include <TFT_eSPI.h>              // Hardware-specific library
#include <TFT_eWidget.h>           // Widget library

#include <constants.h>
#include "SD.h"

TFT_eSPI tft = TFT_eSPI();         // Invoke custom library

TFT_eSprite knob = TFT_eSprite( &tft ); // Sprite for the slide knob


#define CALIBRATION_FILE "/TouchCalData1"
#define REPEAT_CAL true

ButtonWidget btnPEnable = ButtonWidget( &tft );
ButtonWidget btnPPower = ButtonWidget( &tft );
ButtonWidget btnSEnable = ButtonWidget( &tft );
ButtonWidget btnFile = ButtonWidget( &tft );
SliderWidget sBright = SliderWidget( &tft, &knob );    // Slider 1 widget
SliderWidget sDeg = SliderWidget( &tft, &knob );    // Slider 2 widget

#define BUTTON_W 100
#define BUTTON_H 50



// Create a parameter set for the slider
slider_t param;

// Create an array of button instances to use in for() loops
// This is more useful where large numbers of buttons are employed
ButtonWidget* btn[] = { &btnPEnable, &btnPPower, &btnSEnable, &btnFile };;
uint8_t buttonCount = sizeof( btn ) / sizeof( btn[ 0 ] );


void btnPEnable_pressAction( void );
void btnSEnable_releaseAction( void )
{
}
void btnPEnable_releaseAction( void )
{
}
void printWeight( float weight );
void rotateServo( uint16_t percent );
void btnPPower_pressAction( void );
void btnPPower_releaseAction( void )
{
    // Not action
}
void btnFile_pressAction( void );
void btnFile_releaseAction( void )
{
    // Not action
}
void initButtons();


void touch_calibrate()
{
    uint16_t calData[ 5 ];
    uint8_t calDataOK = 0;

    // check file system exists
    if( !LittleFS.begin() )
    {
        Serial.println( "Formating file system" );
        LittleFS.format();
        LittleFS.begin();
    }

    // check if calibration file exists and size is correct
    if( LittleFS.exists( CALIBRATION_FILE ) )
    {
        if( REPEAT_CAL )
        {
            // Delete if we want to re-calibrate
            LittleFS.remove( CALIBRATION_FILE );
        }
        else
        {
            File f = LittleFS.open( CALIBRATION_FILE, "r" );
            if( f )
            {
                if( f.readBytes( ( char * ) calData, 14 ) == 14 )
                    calDataOK = 1;
                f.close();
            }
        }
    }

    // set color scheme

    if( calDataOK && !REPEAT_CAL )
    {
        // calibration data valid
        tft.setTouch( calData );
    }
    else
    {


        // data not valid so recalibrate
        tft.fillScreen( TFT_BLACK );
        tft.setCursor( 20, 0 );
        tft.setTextFont( 2 );
        tft.setTextSize( 1 );
        tft.setTextColor( TFT_WHITE, TFT_BLACK );

        tft.println( "Touch corners as indicated" );

        tft.setTextFont( 1 );
        tft.println();

        if( REPEAT_CAL )
        {
            tft.setTextColor( TFT_RED, TFT_BLACK );
            tft.println( "Set REPEAT_CAL to false to stop this running again!" );
        }

        tft.calibrateTouch( calData, TFT_MAGENTA, TFT_BLACK, 15 );

        tft.setTextColor( TFT_GREEN, TFT_BLACK );
        tft.println( "Calibration complete!" );

        // store data
        File f = LittleFS.open( CALIBRATION_FILE, "w" );
        if( f )
        {
            f.write( ( const unsigned char * ) calData, 14 );
            f.close();
        }
    }
}

void setBrightnessPin( uint8_t pin, uint8_t channel )
{
    ledcSetup( channel, 5000, 8 );
    ledcAttachPin( pin, channel );
}


void setBrightness( uint8_t brightness, uint8_t channel )
{
    uint8_t outPWM = map( brightness, 0, 100, 0, 255 );
    ledcWrite( channel, outPWM );
    Serial.printf( "Brightness: %d\n", outPWM );
}

void enableSd( bool enableSd );
bool saveServerPosition( uint16_t position );
uint16_t loadServerPosition();

void displaySetup()
{

    delay( 500 );
    tft.begin();
    tft.invertDisplay( false );
    tft.setRotation( 3 );
    tft.setSwapBytes( true );
    tft.fillScreen( TFT_BLACK );
    tft.setFreeFont( FF18 );
    setBrightnessPin( DLED_PIN, BRIGHTNESS_CHANNEL );
    setBrightness( 100, BRIGHTNESS_CHANNEL );


    if( !SD.begin( SD_CS ) )
    {
        Serial.println( "Unable to open sd card" );
        tft.drawString( "Unable to open sd card",  100, 100 );
        delay( 5000 );
    }
    else
    {
        Serial.print( "SD Card Size: " );
        float sizeInGB = static_cast< double >( SD.cardSize() ) / ( 1024 * 1024 * 1024 );
        Serial.println( sizeInGB, 2 );

    }

    // Calibrate the touch screen and retrieve the scaling factors
    touch_calibrate();
    tft.setTextSize( 1 );
    tft.setTextFont( 1 );

    param.slotWidth = 9;          // Note: ends of slot will be rounded and anti-aliased
    param.slotLength = 185;      // Length includes rounded ends
    param.slotColor = TFT_DARKGREY;  // Slot colour
    param.slotBgColor = TFT_BLACK; // Slot background colour for anti-aliasing
    param.orientation = H_SLIDER; // sets it "true" for horizontal

// Slider control knob parameters (smooth rounded rectangle)
    param.knobWidth = 15;        // Always along x axis
    param.knobHeight = 25;       // Always along y axis
    param.knobRadius = 5;        // Corner radius
    param.knobColor = TFT_LIGHTGREY; // Anti-aliased with slot backgound colour
    param.knobLineColor = TFT_DARKGREY; // Colour of marker line (set to same as knobColor for no line)


// Slider range and movement speed
    param.sliderLT = 0;        // Left side for horizontal, top for vertical slider
    param.sliderRB = 100;      // Right side for horizontal, bottom for vertical slider
    param.startPosition = 50;  // Start position for control knob
    param.sliderDelay = 0;     // Microseconds per pixel movement delay (0 = no delay)

    //Wait for the display to boot up and for circuit to stabilize and charge the capacitor

}

void displayLoop()
{
    static uint32_t scanTime = millis();
    uint16_t t_x = 9999, t_y = 9999; // To store the touch coordinates
    // Scan keys every 50ms at most
    if( millis() - scanTime >= 40 )
    {
        // Pressed will be set true if there is a valid touch on the screen
        bool pressed = tft.getTouch( &t_x, &t_y );
        scanTime = millis();
        for( uint8_t b = 0; b < buttonCount; b++ )
        {
            if( pressed )
            {
                if( btn[ b ]->contains( t_x, t_y ) )
                {
                    Serial.print( "Button pressed: " + String( b ) );
                    btn[ b ]->press( true );
                    btn[ b ]->pressAction();
                }
            }
            else
            {
                btn[ b ]->press( false );
                btn[ b ]->releaseAction();
            }
        }
        if( sBright.checkTouch( t_x, t_y ) )
        {
            Serial.print( "Brightness = " ); Serial.println( sBright.getSliderPosition() );
            setBrightness( sBright.getSliderPosition(), BRIGHTNESS_CHANNEL );
        }
        else if( sDeg.checkTouch( t_x, t_y ) )
        {
            Serial.print( "Degrees = " ); Serial.println( sDeg.getSliderPosition() );
            rotateServo( sDeg.getSliderPosition() );

        }

    }

}
#endif