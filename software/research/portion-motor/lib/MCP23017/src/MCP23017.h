#ifndef MCP23017_H
#define MCP23017_H

#include "Arduino.h"
#include <Wire.h>

class MCP23017
{
public:
    MCP23017( uint8_t addr, TwoWire *wire = &Wire );
    //  bool begin_I2C(uint8_t i2c_addr = MCP23XXX_ADDR, TwoWire *wire = &Wire);
    ~MCP23017();

private:
    TwoWire *_wire;
};

#endif