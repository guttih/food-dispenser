#include "PortionManager.h"


PortionManager::PortionManager( AugerController *auger, DispensingController *dispenser, Weigher *weigher )
{
    _foodTransporter = auger;
    _weigher = weigher;
    _foodDispenser = dispenser;
}

void PortionManager::begin()
{

    _weigher->begin();
}

void PortionManager::rotateStepper( long deg )
{
    _foodTransporter->rotateStepper( deg );
}

void PortionManager::enableStepper( bool enable )
{
    _foodTransporter->enableStepper( enable );
}

PortionManager::~PortionManager()
{

}

float PortionManager::readWeightInGrams( bool waitForScaleToBeReady )
{
    return _weigher->readWeightInGrams( waitForScaleToBeReady );

}
void PortionManager::openDispensingDoor()
{
    setServoAngle( 180 );
    enableServo( true );
}

void PortionManager::closeDispensingDoor()
{
    setServoAngle( 0 );
    enableServo( false );
}
void PortionManager::enableServo( bool enable )
{
    _foodDispenser->enable( enable );
}

void PortionManager::setServoAngle( uint16_t angle )
{
    _foodDispenser->setServoAngle( angle );
}

