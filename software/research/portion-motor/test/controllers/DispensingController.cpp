#include "DispensingController.h"

DispensingController::DispensingController( short pin, short channel )
{
    const short servoMaxAngle = 180;
    const short servoPulseMinUs = 500;
    const short servoPulseMaxUs = 2500;

    _servo = new Servo( pin, servoMaxAngle, channel, servoPulseMinUs, servoPulseMaxUs );
    _servo->setIdle( true );

}

void DispensingController::enable( bool enable )
{
    _servo->setIdle( !enable );
    Serial.println( String( "Servo " ) + ( enable ? "enabled" : "disabled" ) );
}

void DispensingController::setServoAngle( uint16_t angle )
{
    _servo->setAngle( angle );
    Serial.println( String( "Servo angle set to " ) + String( angle ) + "°" );
}

void DispensingController::openDispensingDoor()
{
    //todo: maybe this should be done tactically, by opening little first, then more, then all the way
    setServoAngle( 180 );
    enable( true );
}

void DispensingController::closeDispensingDoor()
{
    setServoAngle( 0 );
    enable( false );
}


DispensingController::~DispensingController()
{

}