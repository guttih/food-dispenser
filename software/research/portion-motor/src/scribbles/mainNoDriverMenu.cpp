// #include <Arduino.h>

// // defines pins numbers


// // Connect the following pins to the A4988 driver module to the ESP32
// // ESP32   A4988
// //  25 ---- DIR
// //  26 ---- STEP
// //  13 ---- ENABLE
// //  14 ---- MS1
// //  15 ---- MS2
// //  16 ---- MS3




// // struct with const pin numbers and dynamic values

// struct PIN {
//     const uint8_t pin;
//     int value;
// };

// // Declaring a structure for storing pin numbers when connecting to the A4988 driver
// struct StepperPins {
//     PIN enable = { 13, 0 };
//     PIN direction = { 25, 0 };
//     PIN step = { 26, 0 };
//     PIN ms1 = { 14, 0 };
//     PIN ms2 = { 15, 0 };
//     PIN ms3 = { 16, 0 };

//     //Usually we don't use the sleep and reset pins so we connect them together.
//     // int sleepPin; //The sleep pin is used to disable the motor
//     // int resetPin; //The reset pin is used to reset the driver
// };

// StepperPins motorPins;

// //  returns:
// //    success:
// //        A string containing the value of the given parameterName.
// //    fail:
// //        Empty string if
// //         - if url did not include the '?'
// //         - parameter name was not found after the '?'
// //         - character or had no value
// String getQueryParameterValue( String url, String parameterName )
// {
//     int start = url.lastIndexOf( '?' );
//     //if there is no ? in the url, there are not parameters
//     if( start < 0 )
//         return "";

//     //check if parameter name exits
//     start = url.indexOf( parameterName + '=', start + 1 );
//     if( start < 0 )
//         return "";
//     start += parameterName.length() + 1; //let start point to what comes after =
//     int end = url.indexOf( '&', start + 1 );
//     if( end < 0 )
//     {
//         //this parameter is the last one in the url
//         return url.substring( start );
//     }
//     //This parameter is not the last one in the url
//     return url.substring( start, end );
// }

// bool isValidNumber( String str )
// {
//     int len = str.length();
//     if( len < 1 )
//         return false;

//     for( byte i = 0; i < str.length(); i++ )
//     {
//         if( !isDigit( str.charAt( i ) ) )
//             return false;
//     }
//     return true;
// }

// String inputString = "";         // a string to hold incoming data
// boolean stringComplete = false;  // whether the string is complete
// void serialEvent()
// {
//     while( Serial.available() )
//     {
//         // get the new byte:
//         char inChar = ( char ) Serial.read();
//         // add it to the inputString:
//         inputString += inChar;
//         // if the incoming character is a newline, set a flag
//         // so the main loop can do something about it:
//         if( inChar == '\n' )
//         {
//             stringComplete = true;
//         }
//     }
// }

// enum PIN_ID {
//     ENABLE,
//     DIRECTION,
//     STEP,
//     MS1,
//     MS2,
//     MS3
// };

// String pinIdToString( PIN_ID pinId )
// {
//     switch( pinId )
//     {
//         case ENABLE:
//             return "enable";
//         case DIRECTION:
//             return "direction";
//         case STEP:
//             return "step";
//         case MS1:
//             return "ms1";
//         case MS2:
//             return "ms2";
//         case MS3:
//             return "ms3";
//         default:
//             return "unknown";
//     }
// }

// bool setPinValue( PIN_ID pinId, int value )
// {
//     if( value < 0 )
//         return false;


//     switch( pinId )
//     {
//         case ENABLE:
//             motorPins.enable.value = value;
//             digitalWrite( motorPins.enable.pin, motorPins.enable.value );
//             break;
//         case DIRECTION:
//             motorPins.direction.value = value;
//             digitalWrite( motorPins.direction.pin, motorPins.direction.value );
//             break;
//         case STEP:
//             motorPins.step.value = value;
//             digitalWrite( motorPins.step.pin, motorPins.step.value );
//             break;
//         case MS1:
//             motorPins.ms1.value = value;
//             digitalWrite( motorPins.ms1.pin, motorPins.ms1.value );
//             break;
//         case MS2:
//             motorPins.ms2.value = value;
//             digitalWrite( motorPins.ms2.pin, motorPins.ms2.value );
//             break;
//         case MS3:
//             motorPins.ms3.value = value;
//             digitalWrite( motorPins.ms3.pin, motorPins.ms3.value );
//             break;
//         default:
//             return false;
//     }
//     return true;
// }

// // returns -1 if value is not valid
// int getPositiveNumberValueFromUrl( String url, String parameterName )
// {
//     String value = getQueryParameterValue( url, parameterName );

//     if( value != "0" && value != "1" )
//     {
//         return -1;
//     }
//     return value.toInt();
// }

// void printPinValue( PIN_ID pinId )
// {
//     int val=-1;
//     switch( pinId )
//     {
//         case ENABLE:
//             val = motorPins.enable.value;
//             break;
//         case DIRECTION:
//             val = motorPins.direction.value;
//             break;
//         case STEP:
//             val = motorPins.step.value;
//             break;
//         case MS1:
//             val = motorPins.ms1.value;
//             break;
//         case MS2:
//             val = motorPins.ms2.value;
//             break;
//         case MS3:
//             val = motorPins.ms3.value;
//             break;
//         default:
//             return;
//     }

//     Serial.println( String( "   " ) + String( pinIdToString( pinId ) ) + "=" + String( val ) );
// }

// bool setPinValueIfValid( PIN_ID pinId, String url )
// {
//     String pinName=pinIdToString( pinId );
//     int inValue = getPositiveNumberValueFromUrl( url, pinName );
//     if( setPinValue( pinId, inValue ) )
//     {
//         printPinValue( pinId );
//         return true;
//     }

//     return false;
// }

// void printPinNumbers()
// {
//     Serial.println( String( "Pin numbers:" ) );
//     Serial.println( String( "   " ) + String( pinIdToString( ENABLE ) ) + "=" + String( motorPins.enable.pin ) );
//     Serial.println( String( "   " ) + String( pinIdToString( DIRECTION ) ) + "=" + String( motorPins.direction.pin ) );
//     Serial.println( String( "   " ) + String( pinIdToString( STEP ) ) + "=" + String( motorPins.step.pin ) );
//     Serial.println( String( "   " ) + String( pinIdToString( MS1 ) ) + "=" + String( motorPins.ms1.pin ) );
//     Serial.println( String( "   " ) + String( pinIdToString( MS2 ) ) + "=" + String( motorPins.ms2.pin ) );
//     Serial.println( String( "   " ) + String( pinIdToString( MS3 ) ) + "=" + String( motorPins.ms3.pin ) );
// }


// void printPinValues()
// {
//     Serial.println( "Pin values:" );
//     printPinValue( ENABLE );
//     printPinValue( DIRECTION );
//     printPinValue( STEP );
//     printPinValue( MS1 );
//     printPinValue( MS2 );
//     printPinValue( MS3 );

// }

// void reset()
// {
//     setPinValue( ENABLE, 0 );
//     setPinValue( DIRECTION, 0 );
//     setPinValue( STEP, 0 );
//     setPinValue( MS1, 0 );
//     setPinValue( MS2, 0 );
//     setPinValue( MS3, 0 );

// }

// void printMenu()
// {
//     Serial.println( "+------------------------        Menu        --------------------------+" );
//     Serial.println( "| Command                        : Command to type, then press enter   |" );
//     Serial.println( "| -----------------------------  : ----------------------------------- |" );
//     Serial.println( "| values                         : Print pin values                    |" );
//     Serial.println( "| pins                           : Print pin numbers                   |" );
//     Serial.println( "| menu                           : This menu                           |" );
//     Serial.println( "| reset                          : Reset pin values to default         |" );
//     Serial.println( "| -----------------------------  : ----------------------------------- |" );
//     Serial.println( "|                                                                      |" );
//     Serial.println( "|                          SETTING PIN VALUES                          |" );
//     Serial.println( "|                                                                      |" );
//     Serial.println( "|  &enable=1                     : Set enable pin to HIGH              |" );
//     Serial.println( "|  &direction=0&enable=0&step=1  : Set direction to LOW, set enable    |" );
//     Serial.println( "|                                  pin to LOW and set step pin to HIGH |" );
//     Serial.println( "+----------------------------------------------------------------------+" );

// }

// int value;
// void setup()
// {
//     // Sets the two pins as Outputs
//     pinMode( motorPins.direction.pin, OUTPUT );
//     pinMode( motorPins.step.pin, OUTPUT );
//     pinMode( motorPins.enable.pin, OUTPUT );
//     pinMode( motorPins.ms1.pin, OUTPUT );
//     pinMode( motorPins.ms2.pin, OUTPUT );
//     pinMode( motorPins.ms3.pin, OUTPUT );
//     Serial.begin( 115200 );
//     delay( 1000 );
//     printMenu();
// }

// void loop()
// {
//     if( stringComplete )
//     {
//         while( inputString.endsWith( "\n" ) || inputString.endsWith( "\r" ) )
//         {
//             inputString.remove( inputString.length() - 1 );
//         }
//         Serial.println( String( "\"" ) + inputString + "\"" );
//         if( inputString == "values" )
//         {
//             printPinValues();
//         }
//         else if( inputString == "pins" )
//         {
//             printPinNumbers();
//         }
//         else if( inputString == "reset" )
//         {
//             reset();
//         }
//         else if( inputString == "menu" )
//         {
//             printMenu();
//         }
//         else
//         {
//             setPinValueIfValid( ENABLE, inputString );
//             setPinValueIfValid( DIRECTION, inputString );
//             setPinValueIfValid( STEP, inputString );
//             setPinValueIfValid( MS3, inputString );
//             setPinValueIfValid( MS2, inputString );
//             setPinValueIfValid( MS1, inputString );
//         }

//         inputString = "";
//         stringComplete = false;


//     }

//     digitalWrite( motorPins.step.pin, motorPins.step.value );
//     delayMicroseconds( 500 );
//     digitalWrite( motorPins.step.pin, LOW );
//     delayMicroseconds( 500 );
//     // Serial.println( "Enables the motor to move in a particular direction..." );
//     // digitalWrite( dirPin, HIGH );//Enables the motor to move in a particular direction...
//     // // Makes 200 pulses for making one full cycle rotation
//     // for( int x = 0; x < 200; x++ )
//     // {
//     //     digitalWrite( stepPin, HIGH );
//     //     delayMicroseconds( 500 );
//     //     digitalWrite( stepPin, LOW );
//     //     delayMicroseconds( 500 );
//     // }
//     // delay( 1000 ); // One second delay

//     // Serial.println( "Changes the rotations direction" );
//     // digitalWrite( dirPin, LOW ); //Changes the rotations direction
//     // // Makes 400 pulses for making two full cycle rotation
//     // for( int x = 0; x < 400; x++ )
//     // {
//     //     digitalWrite( stepPin, HIGH );
//     //     delayMicroseconds( 500 );
//     //     digitalWrite( stepPin, LOW );
//     //     delayMicroseconds( 500 );
//     // }
//     // delay( 1000 );
// }