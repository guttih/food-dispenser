//
//    FILE: HX_is_ready.ino
//  AUTHOR: Rob Tillaart
// PURPOSE: HX711 demo
//     URL: https://github.com/RobTillaart/HX711


#include "HX711.h"

HX711 scale;

uint8_t dataPin = 13;
uint8_t clockPin = 14;


void setup()
{
    Serial.begin( 115200 );
    Serial.println( __FILE__ );
    Serial.print( "LIBRARY VERSION: " );
    Serial.println( HX711_LIB_VERSION );
    Serial.println();

    scale.begin( dataPin, clockPin );
/*
rafmælir: 397   use scale.set_offset(40265); and scale.set_scale(104.227844);
rafm. klip: 56  use scale.set_offset(40209); and scale.set_scale(-506.213379);
dúkahnífur: 94  use scale.set_offset(10395); and scale.set_scale(21.484041);

*/
    // TODO find a nice solution for this calibration..
    // load cell factor 20 KG
    scale.set_offset( 40265 );
    scale.set_scale( 104.227844 );


    // load cell factor 5 KG
    // scale.set_scale( 420.0983 );   // TODO you need to calibrate this yourself.
    // reset the scale to zero = 0
    scale.tare( 20 );
}


void loop()
{
    if( scale.is_ready() )
    {
        Serial.println( scale.get_units( 1 ) );
    }
}


// -- END OF FILE --
