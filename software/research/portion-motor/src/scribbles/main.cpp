
// #include <Arduino.h>
// #include <Weigher.h>
// #include "AugerController.h"
// #include "PortionManager.h"
// #include "display.h"

// /*

//         Connection of all the components to the ESP32

//  Display  Display  Display  Esp32  Stepper   Servo   LoadCell
// |Display|    SD   | Touch | Esp32 | A4988  | Servo  | HX711 |
// |------:|:-------:|:------|------:|:------:|:------:|:-----:|
// | VCC   |         |       |  3.3V |        |        |       |
// | GND   |         |       |   GND |        |        |       |
// | CS    |         |       |    15 |        |        |       |
// | LED   |         |       |    17 |        |        |       |
// | RESET |         |       |     4 |        |        |       |
// | DC/RS |         |       |     2 |        |        |       |
// | MOSI  | SD_MOSI | T_DIN |    23 |        |        |       |
// | SCK   | SD_SCK  | T_CLK |    18 |        |        |       |
// |       | SD_MISO | T_DO  |    19 |        |        |       |
// |       | SD_CS   |       |     5 |        |        |       |
// |       |         | T_CS  |    21 |        |        |       |
// |       |         |       |    16 |        | Orange |       |
// |       |         |       |    22 | DIRECT |        |       |
// |       |         |       |    32 |  STEP  |        |       |
// |       |         |       |    33 | ENABLE |        |       |
// |       |         |       |    13 |        |        |  DT   |
// |       |         |       |    14 |        |        |  SCK  |


// #### Schematic

//           ╔════════╗        ╔═══════════╗              ╔═════════════╗
//           ║  Load  ║        ║  -Servo-  ║              ║ DC_STEPDOWN ║
//           ║  Cell  ║        ║           ║              ║             ║
//           ║        ║        ║       Red-╟──────────────╢-  +Vo(+6V)  ║
//   ┌───────╢-Green  ║        ║     Brown-╟────┬─────────╢-  -Vo(GND)  ║
//   │ ┌─────╢-White  ║      ┌─╢-Orange    ║    ┴   ┌─────╢-GND         ║
//   │ │ ┌───╢-Black  ║      │ ╚═══════════╝   GND  │  ┌──╢-VIN         ║
//   │ │ │ ┌─╢-Red    ║      │ ╔═══════════╗        │  │  ╚═════════════╝
//   │ │ │ │ ╚════════╝      │ ║  -ESP3-   ║        │  │  ╔══════════════╗
//   │ │ │ │ ╔═════════════╗ │ ║           ║        │  │  ║ POWER SOURCE ║
//   │ │ │ │ ║    HX711    ║ └─╢-G16       ║        │  │  ║              ║
//   │ │ │ └─╢ E+          ║   ║           ║        │  │  ║              ║
//   │ │ └───╢ E-       DT-╟───╢-G13       ║        │  └──╢-    +12V    -╟──────┐
//   │ └─────╢-A-      SCK-╟───╢-G14       ║    ┌───┴─────╢-    -GND    -╟───┐  │
//   └───────╢-A+          ║   ║           ║    ┴         ╚══════════════╝   │  │
//           ║             ║   ║           ║   GND                           │  │
//   ┌───────╢- +Vo(+3V3)  ║   ║           ║          ╔═══════════╗          │  │
//   │     ┌─╢- -Vo( GND)  ║   ║           ║          ║ -Stepper- ║          │  │
//   │     │ ╚═════════════╝   ║           ║          ║       Red-╟───────┐  │  │
//   │     └────────┬──────────╢-GND       ║          ║     Black-╟─────┐ │  │  │
//   │              ┴          ║           ║          ║      Blue-╟───┐ │ │  │  │
//   │             GND         ║           ║          ║     Green-╟─┐ │ │ │  │  │
//   ├─────────────────────────╢-3V3       ║          ╚═══════════╝ │ │ │ │  │  │
//   │  ╔══════════════╗       ║           ║       ╔══════════════╗ │ │ │ │  │  │
//   │  ║   LCD TOUCH  ║       ║           ║       ║    -A4988-   ║ │ │ │ │  │  │
//   │  ║    DISPLAY   ║       ║  -ESP32-  ║       ╢-MS1       2B-╟─┘ │ │ │  │  │
//   │  ║              ║       ║           ║       ╢-MS2       2A-╟───┘ │ │  │  │
//   │  ║              ║       ║           ║       ╢-MS3       1A-╟─────┘ │  │  │
//   │  ║      SD_MOSI-╟─┐     ║           ║       ╢-Reset     1B-╟───────┘  │  │
//   │  ║         MOSI-╟─┼───┐ ║       G22-╟───────╢-Direction    ║          │  │
//   │  ║        T_DIN-╟─┘   │ ║       G32-╟───────╢-Step         ║          │  │
//   │  ║       SD_SCK-╟─┐   │ ║       G33-╟───────╢-Enable       ║          │  │
//   │  ║          SCK-╟─┼─┐ │ ║           ║       ╢-Sleep        ║          │  │
//   │  ║        T_CLK-╟─┘ │ └─╢-G23       ║  ┌────╢-GND      GND-╟──────────┘  │
//   │  ║         T_DO-╟─┐ └───╢-G18       ║  │  ┌─╢-VDD     VMOT-╟─────────────┤
//   │  ║      SD_MISO-╟─┴─────╢-G19       ║  │  │ ╚══════════════╝             │
//   │  ║           CS-╟───────╢-G15       ║  │  └─────────────────────────┐    │
//   │  ║        RESET-╟───────╢-G4        ║  ├───┬────────────────────┐   │    │
//   │  ║           DC-╟───────╢-G2        ║  │   ┴   ╔═════════════╗  │   │    │
//   │  ║        SD_CS-╟───────╢-G5    GND-╟──┘  GND  ║ DC_STEPDOWN ║  │   │    │
//   │  ║         T_CS-╟───────╢-G21   G17-╟─┐        ║             ║  │   │    │
//   │  ║              ║       ╚═══════════╝ │  ┌─────╢- -Vo( GND) -╟──┘   │    │
//   │  ║              ║       ╔═══════════╗ │  │  ┌──╢- +Vo(+3V3) -╟──────┘    │
//   │  ║          LED-╟───────╢  Resistor ╟─┘  │  │  ║             ║  ┌───┐    │
//   │  ║          GND-╟────┐  ║-  220Ω   -║    │  │  ║             ║  │   ┴    │
//   │  ║          VCC-╟─┐  │  ╚═══════════╝    │  │  ║         GND-╟──┘  GND   │
//   │  ╚══════════════╝ │  └───────────────────┘  │  ║         VIN-╟───────────┘
//   └───────────────────┴─────────────────────────┘  ╚═════════════╝
//  */


// Weigher weigher( LOAD_CELL_PIN_DATA, LOAD_CELL_PIN_CLOCK, 105.151169 );
// DispensingController dispenser( SERVO_PIN, SERVO_CHANNEL );
// AugerController auger( MOTOR_PIN_DIRECTION, MOTOR_PIN_STEP, MOTOR_PIN_ENABLE );

// PortionManager portioner( &auger, &dispenser,  &weigher );

// #define CIRCLE_COUNT 6

// void btnPEnable_pressAction( void )
// {
//     if( btnPEnable.justPressed() )
//     {
//         btnPEnable.drawSmoothButton( !btnPEnable.getState(), 3, TFT_BLACK, btnPEnable.getState() ? "Disabled" : "Enabled" );
//         Serial.print( "Enable Button toggled: " );
//         if( btnPEnable.getState() )
//             Serial.println( "ON" );
//         else
//             Serial.println( "OFF" );
//         btnPEnable.setPressTime( millis() );
//     }

//     // if button pressed for more than 1 sec...
//     if( millis() - btnPEnable.getPressTime() >= 1000 )
//         Serial.println( "Stop pressing my buttton......." );
//     else
//         Serial.println( "Enable button is being pressed" );

//     portioner.enableStepper( btnPEnable.getState() );
// }

// void btnSEnable_pressAction( void )
// {
//     if( btnSEnable.justPressed() )
//     {
//         btnSEnable.drawSmoothButton( !btnSEnable.getState(), 3, TFT_BLACK, btnSEnable.getState() ? "Disabled" : "Enabled" );
//         Serial.print( "Enable Button toggled: " );
//         if( btnSEnable.getState() )
//             Serial.println( "ON" );
//         else
//             Serial.println( "OFF" );
//         btnSEnable.setPressTime( millis() );
//     }

//     // if button pressed for more than 1 sec...
//     if( millis() - btnSEnable.getPressTime() >= 1000 )
//     {
//         Serial.println( "Stop pressing my buttton......." );
//     }
//     else
//         Serial.println( "Enable button is being pressed" );

//     portioner.enableServo( btnSEnable.getState() );
// }

// void btnPPower_pressAction( void )
// {
//     if( btnPPower.justPressed() )
//     {
//         btnPPower.drawSmoothButton( !btnPPower.getState(), 3, TFT_BLACK, btnPPower.getState() ? "OFF" : "ON" );
//         Serial.print( "Power Button toggled: " );
//         if( btnPPower.getState() )
//             Serial.println( "ON" );
//         else
//             Serial.println( "OFF" );
//         btnPPower.setPressTime( millis() );
//     }

//     // if button pressed for more than 1 sec...
//     if( millis() - btnPPower.getPressTime() >= 1000 )
//     {
//         Serial.println( "Stop pressing my buttton......." );
//     }
//     else
//         Serial.println( "Power button is being pressed" );
// }


// /**
//  * @brief Enables or disables the SD card and touch screen.
//  *
//  * If SD is enabled, touch is disabled and vice versa.
//  *
//  * @param enableSd
//  */
// void enableSd( bool enableSd )
// {
//     const int waitTime = 10;
//     if( enableSd )
//     {
//         // Disable touch
//         digitalWrite( T_CS, HIGH );
//         delayMicroseconds( waitTime );
//         // Enable SD
//         digitalWrite( SD_CS, LOW );
//         delayMicroseconds( waitTime );
//     }
//     else
//     {
//         // Disable SD
//         digitalWrite( SD_CS, HIGH );
//         delayMicroseconds( waitTime );
//         // Enable touch
//         digitalWrite( T_CS, LOW );
//         delayMicroseconds( waitTime );
//     }
// }

// bool saveServerPosition( uint16_t position )
// {
//     enableSd( true );
//     //Open or create file
//     File file = SD.open( SERVO_FILE, FILE_WRITE );
//     if( !file )
//     {
//         Serial.println( "Failed to open file for writing" );
//         enableSd( false );
//         return false;
//     }

//     //Write data to file
//     size_t bytesWritten = file.print( position );
//     file.close();
//     enableSd( false );
//     return bytesWritten > 0;
// }



// /**
//  * @brief Loads the server position.
//  *
//  * This function retrieves the server position from a specific source (e.g., a file, a network service, etc.).
//  * The position is represented as a uint16_t value.
//  *
//  * @return The server position as a uint16_t. If an error occurs during the retrieval of the server position,
//  * the function returns UINT16_MAX. Callers should check for this value to detect errors.
//  */
// uint16_t loadServerPosition()
// {
//     //Open or create file
//     // Disable touch
//     enableSd( true );
//     File file = SD.open( SERVO_FILE, FILE_READ );
//     if( !file )
//     {
//         Serial.println( "Failed to open file for reading" );
//         enableSd( false );
//         return UINT16_MAX;  // Indicate error with UINT16_MAX
//     }

//     //Read data from file
//     uint16_t position = file.parseInt();
//     file.close();
//     enableSd( false );
//     return position;
// }

// void btnFile_pressAction( void )
// {
//     if( btnFile.justPressed() )
//     {
//         int16_t pos;
//         btnFile.drawSmoothButton( !btnFile.getState(), 3, TFT_BLACK, btnFile.getState() ? "Save" : "Load" );
//         Serial.print( "File Button toggled: " );
//         if( btnFile.getState() )
//         {
//             // Saving the position to an sd card
//             pos = sDeg.getSliderPosition();
//             if( saveServerPosition( pos ) )
//                 Serial.println( "Saved slider position" + String( pos ) );
//             else
//                 Serial.println( "Failed to save slider position" + String( pos ) );
//         }
//         else
//         {
//             // Loading the position from an sd card
//             pos = loadServerPosition();
//             if( pos != UINT16_MAX )
//             {
//                 Serial.println( "Loaded slider position" + String( pos ) );
//                 sDeg.setSliderPosition( pos );
//                 rotateServo( pos );
//             }
//             else
//                 Serial.println( "Failed to load slider position" );
//         }
//         btnFile.setPressTime( millis() );

//     }
// }
// const int margin_x = 10;
// const int margin_y = 20;
// const uint16_t DEFAULT_TEXT_COLOR = TFT_WHITE;

// void drawGroup( String label, uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t fgColor=TFT_WHITE, uint16_t bgColor = TFT_BLACK )
// {
//     const int tPadding = 10;
//     const int tMargin = 30;
//     tft.drawRect( x, y, w, h, fgColor );
//     if( fgColor != DEFAULT_TEXT_COLOR )
//         tft.setTextColor( fgColor );
//     else
//         tft.setTextColor( DEFAULT_TEXT_COLOR );

//     tft.setTextSize( 2 );
//     tft.setTextDatum( TL_DATUM );

//     // clear text around the label
//     tft.fillRect( x + tMargin,                        y - tft.fontHeight() / 2,
//                   tft.textWidth( label ) + 2 * tPadding,  tft.fontHeight(), bgColor );
//     // write the label on top of the upper line of the rectangle
//     tft.drawString( label, x + tMargin + tPadding, y - tft.fontHeight() / 2 );

//     if( fgColor != DEFAULT_TEXT_COLOR )
//         tft.setTextColor( DEFAULT_TEXT_COLOR );
// }

// const int servoY = 172;

// void printWeight( float weight )
// {
//     const int y = 60;
//     const int x = 290;
//     tft.setTextColor( DEFAULT_TEXT_COLOR );
//     tft.setTextSize( 3 );
//     tft.setTextDatum( TL_DATUM );
//     String str="";
//     if( weight < 10 )
//         str = "   ";
//     else if( weight < 100 )
//         str = "  ";
//     else if( weight < 1000 )
//         str = " ";

//     // Clear the previous text
//     tft.fillRect( x + 20, tft.fontHeight() + y - tft.fontHeight(), tft.textWidth( "XXXXX.XX" ), tft.fontHeight(), TFT_BLACK );
//     tft.drawString( str + String( weight ), x + 20, y );
// }

// void rotateServo( uint16_t percent )
// {
//     uint16_t degrees = map( percent, 0, 100, 0, 180 );
//     const int y = servoY + 7;
//     const int x = 390;
//     tft.setTextColor( DEFAULT_TEXT_COLOR );
//     tft.setTextSize( 2 );
//     tft.setTextDatum( TL_DATUM );
//     String str="";
//     if( degrees < 10 )
//         str = "  ";
//     else if( degrees < 100 )
//         str = " ";
//     tft.fillRect( x + 20, tft.fontHeight() + y - tft.fontHeight(), tft.textWidth( "XXX" ), tft.fontHeight(), TFT_BLACK ); // clear the previous text
//     tft.drawCircle( x + 63, y, 3, DEFAULT_TEXT_COLOR );
//     tft.drawString( str + String( degrees ), x + 20, y );

//     portioner.setServoAngle( degrees );
// }


// void drawDecorations()
// {
//     drawGroup( "Portioner", margin_x, margin_y, 250, 100, TFT_DARKGREY );
//     drawGroup( "Scale",     280,      margin_y, 190, 100, TFT_DARKGREY );
//     drawGroup( "Dispatcher", margin_x, 135, tft.width() - 2 * margin_x, 100, TFT_DARKGREY );
//     drawGroup( "Brightness", margin_x, 250, tft.width() - 2 * margin_x, 60, TFT_DARKGREY );


//     param.slotLength = 265;
//     sDeg.drawSlider( 130, servoY, param );
//     sDeg.setSliderPosition( 1 );

//     param.slotLength = 310;
//     sBright.drawSlider( 30, 270, param );
//     sBright.setSliderPosition( 70 );
//     setBrightness( sBright.getSliderPosition(), BRIGHTNESS_CHANNEL );

// }

// void initButtons()
// {
//     uint16_t x = margin_x * 2;
//     uint16_t y = 45;


//     tft.fillScreen( TFT_BLACK );
//     drawDecorations();
//     btnPEnable.initButtonUL( x, y, BUTTON_W, BUTTON_H, TFT_WHITE, TFT_BLACK, TFT_GREEN, "Disabled", 1 );
//     btnPEnable.setPressAction( btnPEnable_pressAction );
//     btnPEnable.setReleaseAction( btnPEnable_releaseAction );
//     btnPEnable.drawSmoothButton( false, 3, TFT_BLACK );     // 3 is outline width, TFT_BLACK is the surrounding background colour for anti-aliasing

//     x+= BUTTON_W + 2 * margin_x;
//     btnPPower.initButtonUL( x, y, BUTTON_W, BUTTON_H, TFT_WHITE, TFT_BLACK, TFT_GREEN, "OFF", 1 );
//     btnPPower.setPressAction( btnPPower_pressAction );
//     btnPPower.setReleaseAction( btnPPower_releaseAction );
//     btnPPower.drawSmoothButton( false, 3, TFT_BLACK );     // 3 is outline width, TFT_BLACK is the surrounding background colour for anti-aliasing

//     x = margin_x * 2;
//     y = 160;
//     btnSEnable.initButtonUL( x, y, BUTTON_W, BUTTON_H, TFT_WHITE, TFT_BLACK, TFT_GREEN, "Disabled", 1 );
//     btnSEnable.setPressAction( btnSEnable_pressAction );
//     btnSEnable.setReleaseAction( btnSEnable_releaseAction );
//     btnSEnable.drawSmoothButton( false, 3, TFT_BLACK );     // 3 is outline width, TFT_BLACK is the surrounding background colour for anti-aliasing


//     y = 255;
//     x = 355;
//     btnFile.initButtonUL( x, y, BUTTON_W, BUTTON_H, TFT_WHITE, TFT_BLACK, TFT_GREEN, "Save", 1 );
//     btnFile.setPressAction( btnFile_pressAction );
//     btnFile.setReleaseAction( btnFile_releaseAction );
//     btnFile.drawSmoothButton( false, 3, TFT_BLACK );     // 3 is outline width, TFT_BLACK is the surrounding background colour for anti-aliasing
//     rotateServo( 0 );
//     printWeight( 0 );
// }


// void setup()
// {
//     Serial.begin( 115200 );
//     portioner.begin();
//     displaySetup();
//     initButtons();
// }
// float weight =-1;
// float lastWeight = -1;
// static uint32_t stepperTime;
// void loop()
// {
//     // Serial.println( String( "Reading scale: " ) + String( portioner.readWeightInGrams( true ) ) + "g" );
//     // Serial.println( "Rotating Clockwise..." );
//     // portioner.enableStepper();
//     // Serial.println( String( "Reading scale: " ) + String( portioner.readWeightInGrams( true ) ) + "g" );
//     // delay( 1000 );
//     // Serial.println( String( "Reading scale: " ) + String( portioner.readWeightInGrams( true ) ) + "g" );
//     // portioner.rotateStepper( 360 * CIRCLE_COUNT );
//     // Serial.println( String( "Reading scale: " ) + String( portioner.readWeightInGrams( true ) ) + "g" );
//     // delay( 500 );
//     // Serial.println( "Rotating Anti-clockwise..." );
//     // portioner.rotateStepper( -360 * CIRCLE_COUNT );
//     // Serial.println( String( "Reading scale: " ) + String( portioner.readWeightInGrams( true ) ) + "g" );
//     // portioner.disableStepper();
//     // Serial.println( String( "Reading scale: " ) + String( portioner.readWeightInGrams( true ) ) + "g" );

//     // Serial.println( "Opening door" );
//     // portioner.openDispensingDoor();
//     // delay( 2000 );
//     // Serial.println( "Closing door" );
//     // portioner.closeDispensingDoor();

//     // delay( 3000 );
//     displayLoop();
//     weight = portioner.readWeightInGrams( true );
//     if( lastWeight != weight )
//     {

//         printWeight( weight );
//         lastWeight = weight;
//     }

//     if( millis() - stepperTime >= 10 )
//     {
//         if( btnPEnable.getState() && btnPPower.getState() )
//         {
//             portioner.rotateStepper( 10 );
//         }
//         stepperTime = millis();
//     }

// }