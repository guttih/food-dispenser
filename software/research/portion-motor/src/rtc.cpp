// #include <Arduino.h>
// #include <Wire.h>

// #define MCP23017_ADDRESS 0x20 // Replace with your MCP23017's address
// #define IODIRA_REGISTER 0x00
// #define IODIRB_REGISTER 0x01
// #define GPIOA_REGISTER 0x12
// #define GPIOB_REGISTER 0x13

// // Initialize the Wire library
// const int delayTime = 4000;
// void setup()
// {
//     Serial.begin( 115200 );
//     Serial.println( "MCP23017 test" );
//     Wire.begin( SDA_PIN, SCL_PIN );
//     // Set all pins on Port A and Port B to output
//     Wire.beginTransmission( MCP23017_ADDRESS );
//     Wire.write( IODIRA_REGISTER ); // Select Port A direction register
//     Wire.write( 0x00 ); // Set all pins on Port A to output (0 for output, 1 for input)
//     Wire.endTransmission();

//     Wire.beginTransmission( MCP23017_ADDRESS );
//     Wire.write( IODIRB_REGISTER ); // Select Port B direction register
//     Wire.write( 0x00 ); // Set all pins on Port B to output
//     Wire.endTransmission();
//     delay( delayTime );
//     Serial.println( "starting loop" );

// }

// void loop()
// {
//     // Turn all LEDs on
//     Serial.println( "turning all leds on" );
//     Wire.beginTransmission( MCP23017_ADDRESS );
//     Wire.write( GPIOA_REGISTER ); // Select GPIO Port A
//     Wire.write( 0xFF ); // Turn all LEDs on
//     Wire.endTransmission();

//     Wire.beginTransmission( MCP23017_ADDRESS );
//     Wire.write( GPIOB_REGISTER ); // Select GPIO Port B
//     Wire.write( 0xFF ); // Turn all LEDs on
//     Wire.endTransmission();

//     delay( 100 );

//     Serial.println( "turning all leds off" );
//     // Turn all LEDs off
//     Wire.beginTransmission( MCP23017_ADDRESS );
//     Wire.write( GPIOA_REGISTER ); // Select GPIO Port A
//     Wire.write( 0x00 ); // Turn all LEDs off
//     Wire.endTransmission();

//     Wire.beginTransmission( MCP23017_ADDRESS );
//     Wire.write( GPIOB_REGISTER ); // Select GPIO Port B
//     Wire.write( 0x00 ); // Turn all LEDs off
//     Wire.endTransmission();

//     delay( 400 );
// }