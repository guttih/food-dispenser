
==== User Story Catalog



##### Issue #1       


As a pet I expect my food bowl to be filled with food repeatedly, so i can live.

##### Acceptance Criteria

 - It must be possible to -
 - Dispense food to the pet's bowl at a specific time.


##### Dependencies

* User stories #5, #6, #25, #28, #32, #40, #47, #51
* TODO : Write the hardware dependency stories on how to dispense food to the pet's bowl.

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested


 

##### Issue  #80



As a caretaker, I want to add food to the storage container, so it can be dispensed to the pet later.

##### Acceptance Criteria

 - Caretaker must be able to -
 - Open the storage container.
 - Add food to the storage container.
 - Close the storage container.

##### Dependencies

* User stories #40
 
##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue  #81      


As a owner, from a web page, I want to View scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Fetch vales from the web service
 - Display the values on the web page


##### Dependencies

* User stories #31, #35, #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested





##### Issue #4       



As a pet owner, I want to see how much food my pet has eaten, so I can monitor its health.

##### Acceptance Criteria

 - It must be possible to -
 - Store each feeding in a database.
 - Display the total amount of food dispensed on the LCD touch display.
 - Display the total amount of food dispensed on the web interface.

##### Dependencies

* User stories #40

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #5



As a caretaker, standing in front of the appliance, I want to Dispense one portion of food and cancel the next scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Press a button on LCD Touch Display to dispense one portion of food.
 - The appliance must cancel the next scheduled feeding.
 - The appliance must dispense one portion of food.

##### Dependencies

* User stories #6, #31

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested



##### Issue #6       


As appliance I want the correct amount of food moved from the storage container to the portion container and dispense it to a pet's bowl.

##### Acceptance Criteria

 - It must be possible to -
 - Move the food from the storage container to the portion container.
 - Weigh the food in the portion container.
 - The food must be dispensed in the correct amount.
 - Dispense the food from the portion container to the pet's bowl.


##### Dependencies

* User stories #2

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #7



As a owner, from a web page, I want to Modify scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Fetch vales from the web service and allow user to modify them.
 - Send changed values to the web service for saving.
 - Validate that the values were saved correctly.

##### Dependencies

* User stories #34, #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] The solution has been tested




##### Issue #8



As a owner, from a web page, I want to Remove scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Send a delete request for a specific scheduled feeding to the web service.
 - Validate that the scheduled feeding was deleted.

##### Dependencies

* User stories #35 , #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] The solution has been tested





##### Issue #9



As a owner, from a web page, I want to Stop scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Send a stop request for a specific scheduled feeding to the web service.
 - Validate that the scheduled feeding was stopped.

##### Dependencies

 * User stories #36, #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] The solution has been tested




##### Issue #10



As a owner, from a web page, I want to Start scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Send a start request for a specific scheduled feeding to the web service.
 - Validate that the scheduled feeding was started.

##### Dependencies

* User stories #37, #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] The solution has been tested




##### Issue #11       


As a owner, from a web page, I want to Change the next scheduled feeding time and portion size.

##### Acceptance Criteria

* [ ] Design a wireframe for the lcd touch display to show/and change the next scheduled feeding time and portion size.
* [ ] Use older screen designs in the LDD library created in WaterMixer project to ask user for number input.
* [ ] Invite the user to change the next time and portion size.  Do not allow changing of the Scheduled feeding it self.  Just the next one. 
* [ ] Display when the next scheduled feeding is due after the change.

##### Dependencies

* User stories #38, #69, #39, #28, #43, #44, #45

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #12       


As a owner, from a web page, I want to Dispense one portion of food and cancel the next scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 * Press a button on the web page to dispense one portion of food.
 * The appliance must cancel the next scheduled feeding.
 * The appliance must dispense one portion of food.
 * The appliance must show the next feeding time on the web page.
 * The appliance must show the next portion size on the LCD touch display.


##### Dependencies

* User stories #5, #6, #31, #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #13       


As a owner, from a web page, I want to Get appliance status (next feeding time, next portion weight, total food dispensed, number of feedings, last feeding time and portion).

##### Acceptance Criteria

* [ ] Request data from the GET /status route on the web service. #77

 - User must be able to -
* [ ] Select the status screen from the welcome page.
* [ ] Fetch vales from the web service and display them on the web page.
* [ ] Display the following values on the web page
    * [ ] Next feeding time
    * [ ] Next portion weight
    * [ ] Total food dispensed
    * [ ] Number of feedings
    * [ ] Last feeding time
    * [ ] Last portion size


##### Dependencies

* User stories  #77, #53, #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #14       


As a owner, from a VoffCon web page, I want to Add scheduled feeding.


##### Acceptance Criteria

 - User must be provided with inputs to -
* [ ] Name the scheduled feeding.
* [ ] Select the time for the scheduled feeding.
* [ ] Select the portion size for the scheduled feeding.
* [ ] Select the recurrence type for the scheduled feeding.
* [ ] Select the recurrence values for the scheduled feeding.
    * [ ] Allow user to select recurrence type "Ones".
    * [ ] Allow user to select recurrence type "Daily".
    * [ ] Allow user to select recurrence type "Weekly".

##### Dependencies

* User stories #31, #32, #42, #70, #71, #72, #73, #76

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #15       


As a owner, from a VoffCon web page, I want to Modify scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Fetch vales from the web service and allow user to modify them.
 - Send changed values to the web service for saving.
 - Validate that the values were saved correctly.


##### Dependencies

* User stories #14

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #16       


As a owner, from a VoffCon web page, I want to Remove scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Send a delete request for a specific scheduled feeding to the web service.
 - Validate that the scheduled feeding was deleted.


##### Dependencies

* User stories #32, #35, #14

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #17      


As a owner, from a VoffCon web page, I want to Stop scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Send a PUT request to the web service for a specific scheduled feeding be changed from active to inactive.
 - Validate that the scheduled feeding disabled.
 - Send a response to the client


##### Dependencies

* User stories #44, #76

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #18      


As a owner, from a VoffCon web page, I want to Start scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Send a PUT request to the web service for a specific scheduled feeding be changed from active to inactive.
 - Validate that the scheduled feeding disabled.
 - Send a response to the client


##### Dependencies

* User stories #44, #76

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #19       


As a owner, from a VoffCon web page, I want to Change the next scheduled feeding time and portion size.

##### Acceptance Criteria

* [ ] Design a wireframe for the lcd touch display to show/and change the next scheduled feeding time and portion size.
* [ ] Use older screen designs in the LDD library created in WaterMixer project to ask user for number input.
* [ ] Invite the user to change the next time and portion size.  Do not allow changing of the Scheduled feeding it self.  Just the next one. 
* [ ] Display when the next scheduled feeding is due after the change.

##### Dependencies

* User stories #38, #39, #28, #43, #44, #45, #73



* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #20       


As a owner, from a VoffCon web page, I want to Dispense one portion of food and cancel the next scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 * Press a button on the web page to dispense one portion of food.
 * The appliance must cancel the next scheduled feeding.
 * The appliance must dispense one portion of food.
 * The appliance must show the next feeding time on the web page.
 * The appliance must show the next portion size on the LCD touch display.


##### Dependencies

* User stories #5, #6, #31, #73

##### Definition of Done

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #21       


As a owner, from a VoffCon web page, I want to Get appliance status (next feeding time, next portion weight, total food dispensed, number of feedings, last feeding time and portion).

##### Acceptance Criteria

* [ ] Request data from the GET /status route on the web service. #77

 - User must be able to -
* [ ] Fetch vales from the web service and display them on the web page.
* [ ] Display the following values on the web page
    * [ ] Next feeding time
    * [ ] Next portion weight
    * [ ] Total food dispensed
    * [ ] Number of feedings
    * [ ] Last feeding time
    * [ ] Last portion size


##### Dependencies

* User stories  #77, #73

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #22       


As a caretaker, standing in front of the appliance, I want to Clean the portion container easily, by removing it and washing it in a sink.

##### Acceptance Criteria

 - It must be possible to easily-
 - Remove the portion container from the appliance.
 - Put it back into the appliance.

##### Dependencies

* User stories #40, #55

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested





##### Issue #23       


As a caretaker, standing in front of the appliance, I want to Clean the storage container easily, by removing it and washing it in a sink.

##### Acceptance Criteria

 - It must be possible to easily-
 - Remove the  storage container from the appliance.
 - Put it back into the appliance.

##### Dependencies

* User stories #40, #56

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #24       


As a caretaker, standing in front of the appliance, I want to Clean the insides of the appliance by opening it, and clean it with a vacum cleaner, brush or a cloth while the appliance is still mounted on the wall.

##### Acceptance Criteria

 - It must be possible to -
 - Remove the storage- and portion container from the appliance.
 - Open the appliance to access the inside.
 - Clean the inside of the appliance with a brush, cloth or a vacuum cleaner.

##### Dependencies

* User stories #22, #23, #40

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #25       


As a caretaker, standing in front of the appliance, I want to check if storage container is low on food, so I know if I need to to refill it.

##### Acceptance Criteria

 - It must be possible to -
* [ ] See the amount of food left in the storage container by looking through a transparent window on the storage container.

##### Dependencies

* User stories #40

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #26       


As a caretaker, standing in front of the appliance, I want to Check the appliance status (next feeding time, next portion weight, total food dispensed, number of feedings, last feeding time and portion).

##### Acceptance Criteria

* [ ] Design a wireframe for the lcd touch display to show the appliance status.

 - User must be able to -
* [ ] Select the status screen from the main menu.
* [ ] Fetch vales from the web service
* [ ] Display the following values on the screen
    * [ ] Next feeding time
    * [ ] Next portion weight
    * [ ] Total food dispensed
    * [ ] Number of feedings
    * [ ] Last feeding time
    * [ ] Last portion size


##### Dependencies

* User stories #78, #77

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #27       


As a caretaker, standing in front of the appliance, I want to Stop scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - There must be a pressable button on the LCD touch display that stops the next scheduled feeding.
 - Wireframe for this screen must be created.

##### Dependencies

* User stories #42, #44, #41, #6

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #28       


As a caretaker, standing in front of the appliance, I want to Start scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
 - Press a button on LCD Touch Display to start the last stopped scheduled feeding.
 - Wireframe for this screen must be created.
 - The display must show the next scheduled feeding time and portion size.


##### Dependencies

* User stories #42, #44, #41, #6

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #29       


As a caretaker, standing in front of the appliance, I want to Change the next scheduled feeding time and portion size.

##### Acceptance Criteria

* [ ] Design a wireframe for the lcd touch display to show/and change the next scheduled feeding time and portion size.
* [ ] Use older screen designs in the LDD library created in WaterMixer project to ask user for number input.
* [ ] Invite the user to change the next time and portion size.  Do not allow changing of the Scheduled feeding it self.  Just the next one.
* [ ] Display when the next scheduled feeding is due after the change.

##### Dependencies

* User stories #43, #44, #45, #45

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #30       


As a owner, from a web page, I want to have the option to insert and enable a PIN on a LCD to require a pin code for all commands, to prevent unauthorized access to the appliance.

##### Acceptance Criteria

 - Designed a wireframe for a web page, where user can 
    * [ ] enable and disable pin code requirement.
    * [ ] insert a pin code, but it must be hidden from view and masked with a character like *.
    * [ ] change the pin code.
    * [ ] reset the pin code to default.

 - Handle what to do if pin is forgotten
    * [ ] The appliance must have a way to reset the pin code to the default pin code.
    * [ ] The appliance could send an email to the owner with a reset link.

 - LCD Touch Display
* [ ] The appliance must have a default pin code, which can be changed by the user.
* [ ] The appliance must have the option to disable the pin code requirement.
* [ ] A wireframe asking form pin has been created on the LCD touch display, and in the wireframe the following is considered:
    * [ ] The pin code must be hidden from view and masked with a character like *.
    * [ ] The pin code must be validated to be 4 digits long.
    * [ ] An error message must be displayed if the pin code is not 4 digits long.
    * [ ] After x failed attempts, the appliance must lock the user screen for y minutes.

##### Dependencies

* User stories #53, #32, #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #31



As a client, I should be able to add scheduled feedings using a POST method in the web service.


 - Schedule a feeding properties
* Name
* Time
* Portion size
* Recurrence /  only ones

 - Recurrence types
* Must be implemented
    * Ones, Daily, weekly
* No must, but nice to have
*** Monthly, Yearly, Workdays, Weekends
* No must, but would be better to have also:
*** Every X days
*** Every X weeks

##### Acceptance Criteria

* [ ] Determine what the absolute minimum time between feedings is.
* [ ] The following POST request route must be implemented in the web service: /api/schedule
* [ ] The microcontroller must be able to receive the post request and save the schedule to the sd card.
* [ ] The microcontroller must be capable of receiving the POST request and storing the schedule on the SD card.
* [ ] Save the schedule with recurring Daily feedings
* [ ] Save the schedule with recurring Weekly feedings

##### Dependencies

* User stories #32, #76

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #32



The Appliance must be able to save data so it can be read later, even if it looses power.


##### Acceptance Criteria

* [ ] The microcontroller is wired to the touch display with SD card reader.
* [ ] Wiring includes wiring for the SD card reader.
* [ ] The microcontroller can read from SD card reader.
* [ ] The microcontroller can write to the SD card reader.

##### Dependencies

* Displaying and reacting to user input on the LCD touch display is described in story #52. 

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested





##### Issue #33



As a product owner, I want to have a wireframe design that allows users to add scheduled feedings from a web page.

##### Acceptance Criteria

* The wireframe should include a graphical user interface (GUI) that enables users to:
  - Provide a name for the scheduled feeding.
  - Set the feeding time and portion size.
  - Save these values to the SD card.

##### Dependencies

* User stories #31

##### Definition of Done

* [ ] The wireframe design has been created and reviewed.
* [ ] Documentation has been updated to reflect the wireframe design.
* [ ] Documentation is not needed for the wireframe.
* [ ] The wireframe has been tested for usability with relevant stakeholders.




##### Issue #34



As a client, I should be able to Change scheduled feedings using a PUT method in the web service.

##### Acceptance Criteria

 - It must be possible to -
1. Get a specific scheduled feeding.
1. Give the scheduled feeding a name.
1. Set the feeding time.
1. Set the portion size.
1. Make feeding recurring Ones.
1. Make feeding recurring Daily.
1. Make feeding recurring Weekly.

##### Dependencies

* User stories #38, #31, #76


##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #35



As a client, I should be able to Delete scheduled feedings using a DELETE method in the web service.

##### Acceptance Criteria

 - It must be possible to -
1. Find a scheduled feeding.
1. Delete the scheduled feeding.

##### Dependencies

* User stories #31, #76


##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested





##### Issue #36



As a client, I want to Stop scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
1. Find a scheduled feeding.
1. Send a PUT request to make schedule inactive.

##### Dependencies

* User stories #43, #76


##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested





##### Issue #37



As a client, I want to Start scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
1. Send a PUT request to make schedule active.
1. Find a scheduled feeding.

##### Dependencies

* User stories #43, #76


##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested





##### Issue #38



As a client, I want to Get scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
1. Get a specific scheduled feeding.


##### Dependencies

* User stories #38, #31, #76


##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested





##### Issue #39



As a owner, from a web page, I want to Add scheduled feeding.

##### Acceptance Criteria

 - It must be possible to -
1. Give the scheduled feeding a name.
1. Set the feeding time.
1. Set the portion size.
1. Make feeding recurring Ones.
1. Make feeding recurring Daily.
1. Make feeding recurring Weekly.

##### Dependencies

* User stories #33, #31, #69


##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested






##### Issue #40       


Design the external look and shape of the appliance.

##### Acceptance Criteria

 - It must be possible to -
 - A 3D model of the appliance must be created.
 - 3D model must be exportable to a 3D printer.

##### Dependencies

* User stories  #80, #4, #22, #23, #24, #25, #55, #56

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #41       


As a product owner, I want to have a wireframe design that allows users start and stop scheduled feedings from LCD touch display.

##### Acceptance Criteria

 - It must be possible to -
 - Press a Start button on the LCD touch display to start the feeding schedule.
 - Press a Stop button on the LCD touch display to stop the feeding schedule.

##### Dependencies

* User stories #27, #28

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #42       


As an appliance,I want to have a scheduler that can create scheduled feedings.

##### Acceptance Criteria

* An data structure for a scheduled feeding must be created.
* A scheduler class must be created.

##### Dependencies

* User stories #31

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #43       


As an appliance,I want to have a scheduler that can read, modify and delete scheduled feedings from a database.

##### Acceptance Criteria

 - It must be possible to -
 - Create a scheduled feeding.
 - Read a scheduled feeding from the database.
 - Write a scheduled feeding to the database.
 - Modify a scheduled feeding in the database.
 - Delete a scheduled feeding from the database.

##### Dependencies

* User stories #32

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #44       


As an appliance,I want to have a scheduler that can start and stop scheduled feedings.

##### Acceptance Criteria

 - It must be possible to -
 - Start a scheduled feeding so it will calculate which feeding is next
 - Monitor when next feeding is due
 - Stop the scheduled feeding and remove the next feeding time from the monitor.

##### Dependencies

* User stories #42, #43

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #45       


As an appliance,I want to have a scheduler that can monitor when the next scheduled feeding is due.

##### Acceptance Criteria

 - It must be possible to -
 - Add a monitor function to the scheduler that monitors when the next scheduled feeding is due.
 - Check if the next scheduled feeding is due.

##### Dependencies

* User stories #31

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #46       


As an appliance,I want to have a scheduler that can tell the portioner to dispense food when it is due.

##### Acceptance Criteria

 - It must be possible to -
 - Add a function to the scheduler that tells the portioner to dispense food when it is due.
 - Wait for the portioner to dispense the food.

##### Dependencies

* User stories #45, #47, #48, #49, #50, #51

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #47       


As an appliance,I want to have a dispenser that can use the dispenser motor to open and close the dispensing door.

##### Acceptance Criteria

 - It must be possible to -
 - Learn how to move a servo motor.
 - Create a class dispenser that can give commands to the servo motor.
 - Create a dispenser class function that can open the dispensing door.
 - Create a dispenser class function that can close the dispensing door.
 - Design the portion container with a dispensing door.

##### Dependencies

* User stories #59, #55

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #48       


As an appliance,I want to have a mover that can start and stop the portion motor.

##### Acceptance Criteria

 - It must be possible to -
 - Start and stop the portion motor.
   1. Create a function that can move the portion motor clockwise.
   1. Create a function that can move the portion motor counter clockwise.
   1. Create a function that can start the portion motor.
1. Create a function that can stop the portion motor.

##### Dependencies

* User stories #60


##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #49       


As an appliance,I want to have a storage weigher that can weigh the storage container.

##### Acceptance Criteria

* [ ] Declare a instance of the Weigher class which shall be connected to storage container load cell(s).
* [ ] Test reading from the load cell(s).
* [ ] Test if converting a load cell reading to weight is correct.
* [ ] Test taring the load cell(s).
* [ ] Test calibrating the load cell(s).


##### Dependencies

* User stories #62, #56

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #50       


As an appliance,I want to have a portion weigher that can weigh the portion container.

##### Acceptance Criteria

* [ ] Declare a instance of the Weigher class which shall be connected to portion container load cell(s).
* [ ] Test reading from the load cell(s).
* [ ] Test if converting a load cell reading to weight is correct.
* [ ] Test taring the load cell(s).
* [ ] Test calibrating the load cell(s).


##### Dependencies

* User stories #62, #58

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #51       


As an appliance,I want to have a portioner to use the mover, portion weigher and the dispenser to move food from the storage container to the pet's bowl.

##### Acceptance Criteria

 - The portioner must be able to -
 - Use the mover to connect to and start the portion motor.
 - Use the portion weigher to weigh the portion container.
 - Figure out when to stop the portion motor.
 - Use the dispenser to open and close the dispensing door.
 - Use the mover to start the portion motor.
 - Use the mover to stop the portion motor.
 - Use the mover to move food from the storage container to the portion container.

##### Dependencies

* User stories #48, #50, #47 #57, #55, #58

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #52       


As a product owner, I want to research how to use LCD touch display for showing data and receiving input from user.

##### Acceptance Criteria

 - It must be possible to -
 * Display text on the LCD touch display.
 * Display a button on the LCD touch display.
 * React to a button press on the LCD touch display.


##### Dependencies

* To read and write to the SD card is handled in story #32.
* User stories #32

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #53       


As a product owner, I want to have a webpage that welcomes users to the web interface.

##### Acceptance Criteria

* [ ] Design a wireframe for the web page that welcomes users to the web interface.

 - User must see
* [ ] A welcoming message involving the something warming about pets care
* [ ] Pet image and information
* [ ] Navigation links or buttons to sub pages
* [ ] A link to the status page
* [ ] A link to the schedule page
* [ ] A link to the settings page
* [ ] A link to the about page
* [ ] A link to a pet profile
* [ ] A link to a pet profile settings where users can edit the pet profile

##### Dependencies

* User stories #32, #68, #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #54       


As an appliance, I want to utilize the storage weigher to notify users when the storage container is low on food.

##### Acceptance Criteria

 - It must be possible to -
 - Weigh the storage container.
 - Monitor when the storage container has less than 1 portion of food left.
 - Figure out who to notify the user when the storage container is low on food.
 - Figure out how to notify the user when the storage container is low on food.
 - Notify the user when the storage container is low on food.
 
##### Dependencies

* User stories #56, #50

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #55       


As a product owner, I want designed a portion container with a dispensing door that can be opened and closed by a servo motor.

##### Acceptance Criteria

 - Design 
* The portion container must be designed with a dispensing door.
* The dispensing door must be designed to be opened and closed by a servo motor and food must fall through the dispensing door when it is opened.
* The portion container must be designed to be easily removed from the appliance.
* The portion container must be designed to be easily cleaned.
* The portion container must be designed to be easily put back into the appliance.


##### Dependencies

* User stories #22, #47, #57, #59, #60, #61, #62

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #56       


As a product owner, I want designed a storage container with a weighing mechanism that can be read by a load cell.

##### Acceptance Criteria

 - Design
* The storage container must be designed so two load cells can be mounted on it.
* The storage container must be designed to be easily removed from the appliance.
* The storage container must be designed to be easily cleaned.
* The storage container must be designed to be easily put back into the appliance.


##### Dependencies

* User stories #23, #49, #50, #51, #59, #60, #61, #62

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #57       


As a product owner, I want designed a storage container which can be emptied with an auger, turned by a servo motor.

##### Acceptance Criteria

 - Design
* The storage container must be designed with a door that can be opened and closed manually
* When you put the container back into the appliance, the door must will open by design.
* This is done by putting a handle on the door and when the container slid into it's slot, the handle will hit a obstruction which will force the door open.
* The storage container must be designed to be easily removed from the appliance.
* The storage container must be designed to be easily cleaned.
* The storage container must be designed to be easily put back into the appliance.

##### Dependencies

* User stories #23, #47, #49, #50, #51, #59, #60, #61, #62

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #58       


As a product owner, I want designed a portion container with a weighing mechanism that can be read by a load cell.

##### Acceptance Criteria

 - Design
* The portion container must be designed so one load cell can be mounted on it.
* The portion container must be designed to be easily removed from the appliance.
* The portion container must be designed to be easily cleaned.
* The portion container must be designed to be easily put back into the appliance.

##### Dependencies

* User stories #22, #49, #50, #51, #59, #60, #61, #62

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #59       


As a product owner, I want to research how to turn a servo motor.

##### Acceptance Criteria

 - Researched how to
* Turn a servo motor.
* Turn a servo motor 90 degrees.
* Turn a servo motor -90 degrees.
* Turn a servo motor 180 degrees.
* Turn a servo motor -180 degrees.
* How to connect a servo motor to a microcontroller.
* How to power a servo motor.
* Select a servo motor that can be used for the dispensing door.


##### Dependencies

* User stories #47

##### Definition of Done

* [ ] Research documentation has been updated.
* [ ] The solution has been tested




##### Issue #60       


As a product owner, I want to research how to turn a stepper motor.

##### Acceptance Criteria

 - Research how to
* Turn a stepper motor clockwise.
* Turn a stepper motor counter clockwise.
* How to connect a stepper motor to a microcontroller.
* How to power a stepper motor.
* Select a stepper motor that can be used for the portion motor.


##### Dependencies

* User stories #48

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #61       


As a product owner, I want research how to read a load cell

##### Acceptance Criteria

 - Research how to
* Read a load cell.
* Power a load cell.
* Connect a load cell to a microcontroller.
* Convert a load cell reading to weight.
* Select a load cell that can be used for the storage container.
* Select a load cell that can be used for the portion container.


##### Dependencies

* User stories #49, #50, #51

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested







##### Issue #62       


As an appliance,I want to have a storage weigher that can weigh the storage container.

##### Acceptance Criteria

* [ ] Create a Weigher class which can be configured to connect to a load cell(s).
* [ ] Create a function that can read from a load cell(s).
* [ ] Create a function that can convert the load cell reading to weight.
* [ ] Create a function that can tare the load cell(s).
* [ ] Create a function that can calibrate the load cell(s).


##### Dependencies

* User stories #61

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #63       


Design a PCB to connect all components, together, power them and connect them to the microcontroller.

##### Acceptance Criteria

* Design the PCB which includes
    * download and install KiCad
    * create a new project
    * create a schematic
    * create a PCB layout
* TODO: to be visited later

##### Dependencies

* User stories TODO: #63, 

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #64       


Prepare and send the PCB design to a PCB manufacturer.

##### Acceptance Criteria

 - Prepare the PCB design for manufacturing
* Create a bill of materials
* Create a Gerber file
* Create a drill file
* Create a pick and place file
* Create a PCB design file
* Create a PCB layout file
* TODO: to be visited later

##### Dependencies

* User stories #63

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #65       


Solder all seats and components to the PCB.

##### Acceptance Criteria

* Solder all seats and components to the PCB.
* Check / test
    * if all joints are soldered correctly and there are no shorts.
    * if any soldered joint conducts only what it is supposed to conduct.
*** test the that joint against all other soldered joints.
    * If components fit correctly in their seats.
    * if pins on inserted components conduct as they are supposed to.

##### Dependencies

* User stories #64

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #66       


Plug each component including the microcontroller to it's seat on the PCB and provide power to the PCB.

##### Acceptance Criteria

* Insert each component into it's seat on the PCB.
* Provide power to the PCB.
* Check / test
    * Voltages are correct over each component.
    * If any component gets too hot.
* Write a simple wifi server which is able to turn on and off connected pins.
1. Make sure the server supports uploads via wifi (Over-the-Air (OTA)).
1. Upload it to the microcontroller via usb.
1. Test the server be examining usb serial output while doing requests from a web browser from a computer on the same network as the microcontroller
1. Test if the microcontroller can be reached from a computer on the same network.
1. Unplug the microcontroller from the computer and plug it into the PCB.
1. Test if the microcontroller can still be reached from the same computer.
1. Now via web browser give commands to turn one pin on and off and measure the voltage over the pin with a multimeter.
1. Repeat for all pins that are suppose to be used.
    * Assess whether the microcontroller can successfully activate and deactivate each designated pin as required by measuring the pins' voltage.



##### Dependencies

* User stories #65

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #67       


Test if all components on the PCB are working correctly.

##### Acceptance Criteria

* When applicable, test if all components behave correctly by using the the program that was written for the microcontroller in story #66.
* Write a simple test application that only tests one component at a time, that can
1. Can you turn the portion motor clockwise, counter clockwise and stop it?
1. Can you turn the servo motor 90 degrees, -90 degrees and 180 degrees?
1. Can you open and close the dispensing door?
1. Can you move food from the storage container to the portion container?
1. Can you weigh the storage container?
    * Can you read from the load cell(s)?
    * Can you convert a load cell reading to weight?
    * Can you tare the load cell(s)?
    * Can you calibrate the load cell(s)?
1. Can you weigh the portion container?
    * Can you read from the load cell(s)?
    * Can you convert a load cell reading to weight?
    * Can you tare the load cell(s)?
    * Can you calibrate the load cell(s)?
1. Can you display text on the LCD touch display?
1. Can you react to a button press on the LCD touch display?
1. Can you read from and write to the SD card?
1. Upload the final program to the microcontroller via Over-the-Air (OTA).
1. Test the appliance by using the LCD touch display and the web service.
1. Test the appliance using the web service.

##### Dependencies

* User stories #66, #65, #64, #63

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested





##### Issue #68      


As a pet owner, I want to have a pets profile web page that allows me to provide and view information about my pet.

##### Acceptance Criteria

* [ ] Design a wireframe for the web page that allows users to provide information about their pet.
* Possible feature, make LCD display show the pets picture in some way.

 - User must see add, view and remove
    * [ ] Pets name
    * [ ] Pets image
    * [ ] Pets date of birth

##### Dependencies

* User stories #32, #69, #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #69       


As a product owner, I want the web service to be able to read html files from the web server and send them to the client.

##### Acceptance Criteria

* [ ] Web service must be able to read html files from the web server.
* [ ] Web service must be able to send html files to the client.

##### Dependencies

* User stories #32, #76

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #70       


As a owner, from a web page, I want to list all scheduled feedings, with view, edit and delete buttons.

##### Acceptance Criteria

 - It must be possible to -
 * Select a scheduled feeding from the welcome page.
 * A list of all scheduled feedings must be shown.
 * Each scheduled feeding must have a view, edit and delete button.

##### Dependencies

* User stories #33, #31, #69

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #71       


As an Appliance and a VoffCon device, it needs to connect and register the Network IP address of the appliance.

##### Acceptance Criteria

* After getting an IP address from the DHCP server, the appliance must send a request to the VoffCon server to register the IP address of the appliance.
* More tasks and duties
    * There are more tasks and duties, a VoffCon device must do but they are not part of this user story but need to be mentioned.
    * If a story is created for one of these tasks, make sure to add that story as a dependency to this story making all stories that depend on this story also depend on the new story.

##### Dependencies

* User stories TODO:

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #72       


As product owner, I want to have a VoffCon Control, that can be used to communicate with the appliance.

##### Acceptance Criteria

 - It must be possible to -
 - Get status from the appliance.
 - Device pinout and pin values.
 - Relay various commands from the VoffCon Card to the appliance and report back the result.


##### Dependencies

* User stories #71

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #73       


As product owner, I want to have a VoffCon Card that uses the VoffCon Control perform various tasks.

##### Acceptance Criteria

 - It must be possible to -
 - Account for various tasks to be performed by the owner.
 - Buttons and inputs will need to be created which can be hidden or showed depending on which task is selected.

##### Dependencies

* User stories #70, #71, #72

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #74       


As a VoffCon client I need to be able to get the device voffcon-status from the web service.

##### Acceptance Criteria

* [ ] Find the json object that should hold the voffcon-status.
* [ ] Create a function getVoffconStatus can gather the voffcon-status from the appliance
* [ ] Convert the voffcon-status to a json object.
* [ ] Must support a GET request from the client.
* [ ] Send the voffcon-status to the client.

##### Dependencies

* User stories #8, #73

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #75       


As a product owner, I want the web service to be able send a list of all scheduled feedings to the client.

##### Acceptance Criteria

 - It must be possible to -
 - receive a GET request from the client.
 - Read all scheduled feedings from the database.
 - Send a list of all scheduled feedings to the client.

##### Dependencies

* User stories #38

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #76       


As a product owner, I want the web service to be able to handle, GET, POST, PUT and DELETE requests.

##### Acceptance Criteria

 - The service must be able to
* Receive a GET request from the client, read from the SD card and send the requested data back to the client.
* Receive a POST request from the client, write data to the SD card and send a response back to the client.
* Receive a PUT request from the client, write data to the SD card and send a response back to the client.
* Receive a DELETE request from the client, delete data from the SD card and send a response back to the client.

##### Dependencies

* User stories #32

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested



##### Issue #77       


As a client, I want to be able to get the device status with a GET request.

##### Acceptance Criteria

* [ ] Design a json object that can hold all the status information.
* [ ] Create a function that gathers all the status information from the appliance.
* [ ] Create a function that can convert the status information to a json object. 
* [ ] Must support a GET request from the client.
* [ ] Call getStatus function described in story #78 
* [ ] Must send the status information to the client.

##### Dependencies

* User stories #78, #32, #76

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #78       


As a product owner, I want to have a function which total status of the appliance.

##### Acceptance Criteria

* [ ] Create a function called getStatus that gathers all the status information from the appliance.

 - It must gather the following information
    * [ ] Next feeding time
    * [ ] Next portion weight
    * [ ] Total food dispensed
    * [ ] Number of feedings
    * [ ] Last feeding time
    * [ ] Last portion size
    * [ ] Device running time
    * [ ] Device time and date
    * [ ] Device IP address
    * [ ] Device MAC address
    * [ ] Device name
    * [ ] Device version
    * [ ] Device status


##### Dependencies

* User stories #32

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested




##### Issue #79       



As a VoffCon client I need to be able to post a custom command route to the web service to perform various tasks.

##### Acceptance Criteria

* [ ] Find the json object that should hold the custom PUT command route.
* [ ] Find the json object that should hold the custom GET command route.
* [ ] Create a function postCustomVoffCommandRoute if needed
* [ ] Create a function getCustomVoffCommandRoute if needed

##### Dependencies

* User stories #77, #73

##### Definition of Done

* [ ] Documentation has been updated
* [ ] Documentation is not needed
* [ ] The solution has been tested



Back to <<Prioritization>> table.