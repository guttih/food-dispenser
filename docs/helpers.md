### Some helper snippets if you want to use them

#### Detailed User Story

```config
{
    "Detailed story": {

        "prefix": "userstory",
        "body": [
            "//      User Story ID\n//   ┌─────────────────┐\n====        US${1:00X}       \n//   └─────────────────┘",
            "[.lead]",
            "${2:As a actor, I want to do something, so I can achieve some goal.}",
            "",
            "===== Acceptance Criteria",
            "",
            "${3:.It must be possible to -\n . ${4:do something}\n . ${5:do something else\n}}$0\n",
            "${6:===== Dependencies\n\n* User stories <<US${7:0X1}>>${8:, <<US${9:0X2}>>}\n}",
            "===== Definition of Done",
            "",
            "* [ ] Documentation has been updated",
            "* [ ] Documentation is not needed",
            "* [ ] The solution has been tested +\n----\nThe test was conducted by...\n----",
            "'''",
        ],
        "description": "Write a detailed story"
    }
}
```